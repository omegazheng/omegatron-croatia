package com.omega.commons.rowMapper;

import com.omega.omegatron.service.UserWebSessionDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserWebSessionRowMapper implements RowMapper<UserWebSessionDto> {

    public UserWebSessionDto mapRow(ResultSet rs, int rowNum) throws SQLException {

        UserWebSessionDto session = new UserWebSessionDto();

        session.setPartyId(rs.getInt("PartyId"));
        session.setLastAccessTime(rs.getTimestamp("Last_Access_Time"));
        session.setParlayJsessionid(rs.getString("Parlay_JsessionID"));
        session.setIp(rs.getString("IP"));
        session.setMobile(rs.getBoolean("Is_Mobile"));
        session.setSessionKey(rs.getString("Session_Key"));

        return session;
    }


}
