package com.omega.commons.rowMapper;

import com.omega.commons.entity.RegistryHash;
import com.omega.commons.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RegistryHashMapper implements RowMapper<RegistryHash> {

    public RegistryHash mapRow(ResultSet rs, int rowNum) throws SQLException {

        RegistryHash registryHash = new RegistryHash();

        registryHash.setId(rs.getInt("Id"));
        registryHash.setMapKey(rs.getString("MapKey"));
        registryHash.setValue(rs.getString("Value"));

        return registryHash;
    }


}
