package com.omega.commons.rowMapper;

import com.omega.commons.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<User> {

    public User mapRow(ResultSet rs, int rowNum) throws SQLException {

        User user = new User();

        user.setPartyId(rs.getInt("PartyId"));
        user.setAccountId(rs.getInt("AccountId"));
        user.setBrandId(rs.getInt("BrandId"));
        user.setUserType(rs.getInt("UserType"));
        user.setUserId(rs.getString("UserId"));
        user.setCurrency(rs.getString("Currency"));
        user.setCity(rs.getString("City"));
        user.setFirstName(rs.getString("FirstName"));
        user.setLastName(rs.getString("LastName"));
        user.setGender(rs.getString("gender"));
        user.setLanguage(rs.getString("language"));
        user.setCountry(rs.getString("Country"));
        user.setBirthDate(rs.getTimestamp("birthDate"));
        user.setEmail(rs.getString("email"));
        user.setUuid(rs.getString("uuid"));


        return user;
    }


}
