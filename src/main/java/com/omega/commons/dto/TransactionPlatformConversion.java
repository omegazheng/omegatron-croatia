package com.omega.commons.dto;

import java.math.BigDecimal;

/**
 * Created by waimingmak on 2017-03-18.
 */
public class TransactionPlatformConversion {
    private String currency;
    private BigDecimal rate;
    private BigDecimal amount;

    public TransactionPlatformConversion(String currency, BigDecimal rate, BigDecimal amount){
        this.currency = currency;
        this.rate = rate;
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
