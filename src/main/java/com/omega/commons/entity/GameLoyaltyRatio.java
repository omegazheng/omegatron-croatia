package com.omega.commons.entity;

/**
 * Created by chenzheng on 2017-12-28.
 */
public class GameLoyaltyRatio {
    private Integer id;
    private Integer brandId;
    private Integer gameInfoId;
    private Long accrualRatio;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getGameInfoId() {
        return gameInfoId;
    }

    public void setGameInfoId(Integer gameInfoId) {
        this.gameInfoId = gameInfoId;
    }

    public Long getAccrualRatio() {
        return accrualRatio;
    }

    public void setAccrualRatio(Long accrualRatio) {
        this.accrualRatio = accrualRatio;
    }

    public String getKey() {
        return brandId + "-" + gameInfoId;
    }
}
