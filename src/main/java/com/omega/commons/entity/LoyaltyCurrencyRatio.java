package com.omega.commons.entity;

import java.math.BigDecimal;

/**
 * Created by chenzheng on 2017-12-28.
 */
public class LoyaltyCurrencyRatio {
    private Integer id;
    private Integer brandId;
    private String currencyCode;
    private BigDecimal ratio;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getRatio() {
        return ratio;
    }

    public void setRatio(BigDecimal ratio) {
        this.ratio = ratio;
    }

    public String getKey() {
        return brandId + "-" + currencyCode;
    }
}
