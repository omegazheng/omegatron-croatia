package com.omega.commons.entity;

import java.math.BigDecimal;

/**
 *
 */
public class CurrencyConversionRate {
    private String to_currency;
    private BigDecimal rate;

    public String getTo_currency() {
        return to_currency;
    }

    public void setTo_currency(String to_currency) {
        this.to_currency = to_currency;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
}
