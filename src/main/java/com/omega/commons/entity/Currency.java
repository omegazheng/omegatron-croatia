package com.omega.commons.entity;

/**
 *
 */
public class Currency {
    private String iso_code;
    private String name;

    public String getIso_code() {
        return iso_code;
    }

    public void setIso_code(String iso_code) {
        this.iso_code = iso_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
