package com.omega.commons.entity;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Waiming Mak
 */
public class Sportbook2BetDetail {
    private Integer id;
    private String currency;
    private BigDecimal amount;
    private String type;
    private String wager_id;
    private String platform_code;

    private String status;
    private Date created_timeStamp;
    private Date settled_timeStamp;
    private BigDecimal potential_win;
    private BigDecimal total_odds;
    private Boolean cash_out;
    private String msg;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPlatform_code() {
        return platform_code;
    }

    public void setPlatform_code(String platform_code) {
        this.platform_code = platform_code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWager_id() {
        return wager_id;
    }

    public void setWager_id(String wager_id) {
        this.wager_id = wager_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreated_timeStamp() {
        return created_timeStamp;
    }

    public void setCreated_timeStamp(Date created_timeStamp) {
        this.created_timeStamp = created_timeStamp;
    }

    public Date getSettled_timeStamp() {
        return settled_timeStamp;
    }

    public void setSettled_timeStamp(Date settled_timeStamp) {
        this.settled_timeStamp = settled_timeStamp;
    }

    public BigDecimal getPotential_win() {
        return potential_win;
    }

    public void setPotential_win(BigDecimal potential_win) {
        this.potential_win = potential_win;
    }

    public BigDecimal getTotal_odds() {
        return total_odds;
    }

    public void setTotal_odds(BigDecimal total_odds) {
        this.total_odds = total_odds;
    }

    public Boolean getCash_out() {
        return cash_out;
    }

    public void setCash_out(Boolean cash_out) {
        this.cash_out = cash_out;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
