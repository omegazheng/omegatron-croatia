package com.omega.commons.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Waiming Mak
 */
public class Sportbook2BetSelection {
    private Integer id;
    private Integer bet_details_id;
    private String selection;
    private BigDecimal odds;
    private String market;
    private String period;
    private String event;

    private String outcome_id;
    private String outcome_description;
    private String period_description;
    private Date event_start_timestamp;
    private String event_paths;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBet_details_id() {
        return bet_details_id;
    }

    public void setBet_details_id(Integer bet_details_id) {
        this.bet_details_id = bet_details_id;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public BigDecimal getOdds() {
        return odds;
    }

    public void setOdds(BigDecimal odds) {
        this.odds = odds;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }

    public String getOutcome_id() {
        return outcome_id;
    }

    public void setOutcome_id(String outcome_id) {
        this.outcome_id = outcome_id;
    }

    public String getOutcome_description() {
        return outcome_description;
    }

    public void setOutcome_description(String outcome_description) {
        this.outcome_description = outcome_description;
    }

    public String getPeriod_description() {
        return period_description;
    }

    public void setPeriod_description(String period_description) {
        this.period_description = period_description;
    }

    public Date getEvent_start_timestamp() {
        return event_start_timestamp;
    }

    public void setEvent_start_timestamp(Date event_start_timestamp) {
        this.event_start_timestamp = event_start_timestamp;
    }

    public String getEvent_paths() {
        return event_paths;
    }

    public void setEvent_paths(String event_paths) {
        this.event_paths = event_paths;
    }
}
