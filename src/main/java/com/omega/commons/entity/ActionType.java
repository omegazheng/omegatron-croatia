package com.omega.commons.entity;

/**
 * Created by songleo on 2016-04-27.
 */
public enum ActionType {
    EXPIRE_BONUS("EXPIRE_BONUS", "Expire Bonus"),
    CANCEL_BONUS("CANCEL_BONUS", "Cancel Bonus"),
    EXTERNAL_BONUS("EXTERNAL_BONUS", "External Bonus");

    private final String code;
    private final String description;

    ActionType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public static ActionType fromCode(String code) {
        for (ActionType element : ActionType.values()) {
            if (element.getCode().equals(code)) {
                return element;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String toString() {
        return description;
    }
}
