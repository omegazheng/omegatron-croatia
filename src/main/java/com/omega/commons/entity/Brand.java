package com.omega.commons.entity;

/**
 * Created by chenzheng on 2016-03-09.
 */
public class Brand {
    private Integer brandId;
    private String url;
    private String secondaryWalletPassword;
    private Integer secondaryWalletPlatformId;
    private boolean secondaryWalletEnabled;
    private boolean seamlessSecondaryWalletEnabled;
    private String sswPrimaryWalletUrl;
    private String sswPrimaryWalletCallerId;
    private String sswPrimaryWalletCallerPassword;
    private boolean hasAgency;

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSecondaryWalletPassword() {
        return secondaryWalletPassword;
    }

    public void setSecondaryWalletPassword(String secondaryWalletPassword) {
        this.secondaryWalletPassword = secondaryWalletPassword;
    }

    public Integer getSecondaryWalletPlatformId() {
        return secondaryWalletPlatformId;
    }

    public void setSecondaryWalletPlatformId(Integer secondaryWalletPlatformId) {
        this.secondaryWalletPlatformId = secondaryWalletPlatformId;
    }

    public boolean isSecondaryWalletEnabled() {
        return secondaryWalletEnabled;
    }

    public void setSecondaryWalletEnabled(boolean secondaryWalletEnabled) {
        this.secondaryWalletEnabled = secondaryWalletEnabled;
    }

    public boolean isSeamlessSecondaryWalletEnabled() {
        return seamlessSecondaryWalletEnabled;
    }

    public void setSeamlessSecondaryWalletEnabled(boolean seamlessSecondaryWalletEnabled) {
        this.seamlessSecondaryWalletEnabled = seamlessSecondaryWalletEnabled;
    }

    public String getSswPrimaryWalletUrl() {
        return sswPrimaryWalletUrl;
    }

    public void setSswPrimaryWalletUrl(String sswPrimaryWalletUrl) {
        this.sswPrimaryWalletUrl = sswPrimaryWalletUrl;
    }

    public String getSswPrimaryWalletCallerId() {
        return sswPrimaryWalletCallerId;
    }

    public void setSswPrimaryWalletCallerId(String sswPrimaryWalletCallerId) {
        this.sswPrimaryWalletCallerId = sswPrimaryWalletCallerId;
    }

    public String getSswPrimaryWalletCallerPassword() {
        return sswPrimaryWalletCallerPassword;
    }

    public void setSswPrimaryWalletCallerPassword(String sswPrimaryWalletCallerPassword) {
        this.sswPrimaryWalletCallerPassword = sswPrimaryWalletCallerPassword;
    }

    public boolean isHasAgency() {
        return hasAgency;
    }

    public void setHasAgency(boolean hasAgency) {
        this.hasAgency = hasAgency;
    }
}
