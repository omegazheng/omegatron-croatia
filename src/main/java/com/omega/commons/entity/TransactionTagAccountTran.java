package com.omega.commons.entity;

public class TransactionTagAccountTran {
    Integer id;
    Long accountTranId;
    Integer transactionTagId;

    public TransactionTagAccountTran() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getAccountTranId() {
        return accountTranId;
    }

    public void setAccountTranId(Long accountTranId) {
        this.accountTranId = accountTranId;
    }

    public Integer getTransactionTagId() {
        return transactionTagId;
    }

    public void setTransactionTagId(Integer transactionTagId) {
        this.transactionTagId = transactionTagId;
    }
}
