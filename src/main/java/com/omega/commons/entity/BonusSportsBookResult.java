package com.omega.commons.entity;

import java.math.BigDecimal;

public class BonusSportsBookResult {
    private String type;
    private String selection;
    private BigDecimal selection_condition;
    private BigDecimal odds;
    private BigDecimal odds_condition;
    private String market;
    private BigDecimal market_condition;
    private String period;
    private BigDecimal period_condition;
    private String event;
    private BigDecimal event_condition;

    public BonusSportsBookResult() {}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }

    public BigDecimal getSelection_condition() {
        return selection_condition;
    }

    public void setSelection_condition(BigDecimal selection_condition) {
        this.selection_condition = selection_condition;
    }

    public BigDecimal getOdds() {
        return odds;
    }

    public void setOdds(BigDecimal odds) {
        this.odds = odds;
    }

    public BigDecimal getOdds_condition() {
        return odds_condition;
    }

    public void setOdds_condition(BigDecimal odds_condition) {
        this.odds_condition = odds_condition;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public BigDecimal getMarket_condition() {
        return market_condition;
    }

    public void setMarket_condition(BigDecimal market_condition) {
        this.market_condition = market_condition;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public BigDecimal getPeriod_condition() {
        return period_condition;
    }

    public void setPeriod_condition(BigDecimal period_condition) {
        this.period_condition = period_condition;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public BigDecimal getEvent_condition() {
        return event_condition;
    }

    public void setEvent_condition(BigDecimal event_condition) {
        this.event_condition = event_condition;
    }
}
