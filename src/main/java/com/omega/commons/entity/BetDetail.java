package com.omega.commons.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Waiming Mak
 */
public class BetDetail {
    private Integer id;
    private String bet_status;
    private Date settlement_Date;
    private Integer settled_event_count;
    private String bet_mode;
    private BigDecimal bet_amount;
    private Integer platform_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBet_status() {
        return bet_status;
    }

    public void setBet_status(String bet_status) {
        this.bet_status = bet_status;
    }

    public Integer getSettled_event_count() {
        return settled_event_count;
    }

    public void setSettled_event_count(Integer settled_event_count) {
        this.settled_event_count = settled_event_count;
    }

    public Date getSettlement_Date() {
        return settlement_Date;
    }

    public void setSettlement_Date(Date settlement_Date) {
        this.settlement_Date = settlement_Date;
    }

    public BigDecimal getBet_amount() {
        return bet_amount;
    }

    public void setBet_amount(BigDecimal bet_amount) {
        this.bet_amount = bet_amount;
    }

    public String getBet_mode() {
        return bet_mode;
    }

    public void setBet_mode(String bet_mode) {
        this.bet_mode = bet_mode;
    }

    public Integer getPlatform_id() {
        return platform_id;
    }

    public void setPlatform_id(Integer platform_id) {
        this.platform_id = platform_id;
    }
}
