package com.omega.commons.entity;

/**
 * Created by chenzheng on 2017-12-18.
 */
public class GameInfoExcludedBrand {
    private Integer gameInfoId;
    private Integer brandId;

    public Integer getGameInfoId() {
        return gameInfoId;
    }

    public void setGameInfoId(Integer gameInfoId) {
        this.gameInfoId = gameInfoId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }
}
