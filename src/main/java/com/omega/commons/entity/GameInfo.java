package com.omega.commons.entity;

/**
 * Created by chenzheng on 2016-03-09.
 */
public class GameInfo {
    private Integer gameInfoId;
    private String gameId;
    private Integer platformId;
    private String platformCode;

    public GameInfo(){}

    public GameInfo(Integer id, String gameId, Integer platformId, String platformCode) {
        this.gameInfoId = id;
        this.gameId = gameId;
        this.platformId = platformId;
        this.platformCode = platformCode;
    }

    public Integer getGameInfoId() {
        return gameInfoId;
    }

    public void setGameInfoId(Integer gameInfoId) {
        this.gameInfoId = gameInfoId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Integer getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Integer platformId) {
        this.platformId = platformId;
    }

    public String getPlatformCode() {
        return platformCode;
    }

    public void setPlatformCode(String platformCode) {
        this.platformCode = platformCode;
    }
}
