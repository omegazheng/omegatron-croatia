package com.omega.commons.entity;

/**
 * Created by chenzheng on 2017-12-14.
 */
public class RegistryHash {
    private Integer id;
    private String mapKey;
    private String value;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMapKey() {
        return mapKey;
    }

    public void setMapKey(String mapKey) {
        this.mapKey = mapKey;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
