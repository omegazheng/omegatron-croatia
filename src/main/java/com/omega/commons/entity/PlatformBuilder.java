package com.omega.commons.entity;

public class PlatformBuilder {
    private Integer id;
    private String code;
    private String name;
    private Boolean isEnabled;
    private String username;
    private String password;
    private String walletType;
    private String platformType;
    private String sessionLimitWeb;
    private String realityCheckMobile;
    private String sessionLimitMobile;
    private String realityCheckWeb;
    private String excludedCountries;
    private Integer providerId;
    private Integer segmentId;
    private Integer convertJackpots;

    public PlatformBuilder(){
        this.id = null;
        this.code = null;
        this.name = null;
        this.isEnabled = true;
        this.username = null;
        this.password = null;
        this.walletType = "GAME_PLAY";
        this.platformType = "CASINO";
        this.convertJackpots = 1;
        this.sessionLimitWeb = "iframe";
        this.realityCheckMobile = "iframe";
        this.sessionLimitMobile = "iframe";
        this.realityCheckWeb = "iframe";
        this.excludedCountries = null;
        this.providerId = null;
        this.segmentId = 0;
    }

    public PlatformBuilder setId(Integer id) {
        this.id = id;
        return this;
    }

    public PlatformBuilder setCode(String code) {
        this.code = code;
        return this;
    }

    public PlatformBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public PlatformBuilder setIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
        return this;
    }

    public PlatformBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    public PlatformBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public PlatformBuilder setWalletType(String walletType) {
        this.walletType = walletType;
        return this;
    }

    public PlatformBuilder setPlatformType(String platformType) {
        this.platformType = platformType;
        return this;
    }

    public PlatformBuilder setSessionLimitWeb(String sessionLimitWeb) {
        this.sessionLimitWeb = sessionLimitWeb;
        return this;
    }

    public PlatformBuilder setRealityCheckMobile(String realityCheckMobile) {
        this.realityCheckMobile = realityCheckMobile;
        return this;
    }

    public PlatformBuilder setSessionLimitMobile(String sessionLimitMobile) {
        this.sessionLimitMobile = sessionLimitMobile;
        return this;
    }

    public PlatformBuilder setRealityCheckWeb(String realityCheckWeb) {
        this.realityCheckWeb = realityCheckWeb;
        return this;
    }

    public PlatformBuilder setExcludedCountries(String excludedCountries) {
        this.excludedCountries = excludedCountries;
        return this;
    }

    public PlatformBuilder setProviderId(Integer providerId) {
        this.providerId = providerId;
        return this;
    }

    public PlatformBuilder setSegmentId(Integer segmentId) {
        this.segmentId = segmentId;
        return this;
    }

    public Integer getConvertJackpots() {
        return convertJackpots;
    }

    public void setConvertJackpots(Integer convertJackpots) {
        this.convertJackpots = convertJackpots;
    }

    public Platform createPlatform() {
        return new Platform(id, code, name, isEnabled, username, password, walletType, platformType, sessionLimitWeb, realityCheckMobile, sessionLimitMobile, realityCheckWeb, excludedCountries, providerId, segmentId, convertJackpots);
    }
}