package com.omega.commons.entity;

/**
 * Created by chenzheng on 2017-12-14.
 */
public class Platform {
    private Integer id;
    private String code;
    private String name;
    private Boolean isEnabled;
    private String username;
    private String password;
    private String walletType;
    private String platformType;
    private String sessionLimitWeb;
    private String realityCheckMobile;
    private String sessionLimitMobile;
    private String realityCheckWeb;
    private String excludedCountries;
    private Integer providerId;
    private Integer segmentId;
    private Integer convertJackpots;

    public Platform(){}

    public Platform(Integer id, String code, String name, Boolean isEnabled, String username, String password, String walletType, String platformType, String sessionLimitWeb, String realityCheckMobile, String sessionLimitMobile, String realityCheckWeb, String excludedCountries, Integer providerId, Integer segmentId, Integer convertJackpots) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.isEnabled = isEnabled;
        this.username = username;
        this.password = password;
        this.walletType = walletType;
        this.platformType = platformType;
        this.sessionLimitWeb = sessionLimitWeb;
        this.realityCheckMobile = realityCheckMobile;
        this.sessionLimitMobile = sessionLimitMobile;
        this.realityCheckWeb = realityCheckWeb;
        this.excludedCountries = excludedCountries;
        this.providerId = providerId;
        this.segmentId = segmentId;
        this.convertJackpots = convertJackpots;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getWalletType() {
        return walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public String getPlatformType() {
        return platformType;
    }

    public void setPlatformType(String platformType) {
        this.platformType = platformType;
    }

    public String getSessionLimitWeb() {
        return sessionLimitWeb;
    }

    public void setSessionLimitWeb(String sessionLimitWeb) {
        this.sessionLimitWeb = sessionLimitWeb;
    }

    public String getRealityCheckMobile() {
        return realityCheckMobile;
    }

    public void setRealityCheckMobile(String realityCheckMobile) {
        this.realityCheckMobile = realityCheckMobile;
    }

    public String getSessionLimitMobile() {
        return sessionLimitMobile;
    }

    public void setSessionLimitMobile(String sessionLimitMobile) {
        this.sessionLimitMobile = sessionLimitMobile;
    }

    public String getRealityCheckWeb() {
        return realityCheckWeb;
    }

    public void setRealityCheckWeb(String realityCheckWeb) {
        this.realityCheckWeb = realityCheckWeb;
    }

    public String getExcludedCountries() {
        return excludedCountries;
    }

    public void setExcludedCountries(String excludedCountries) {
        this.excludedCountries = excludedCountries;
    }

    public Integer getProviderId() {
        return providerId;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEnabled() {
        return isEnabled;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    public Integer getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(Integer segmentId) {
        this.segmentId = segmentId;
    }

    public Integer getConvertJackpots() {
        return convertJackpots;
    }

    public void setConvertJackpots(Integer convertJackpots) {
        this.convertJackpots = convertJackpots;
    }
}
