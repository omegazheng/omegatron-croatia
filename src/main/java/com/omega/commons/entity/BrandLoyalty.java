package com.omega.commons.entity;

/**
 * Created by chenzheng on 2016-03-09.
 */
public class BrandLoyalty {
    private Integer id;
    private Integer brandId;
    private Integer conversionRatio;
    private Integer loyaltyToRaw;
    private Integer minimumConversion;
    private boolean isEnabled;
    private boolean isAccrualEnabled;
    private Integer bonusPlanId;
    private Integer accrualMultiplier;
    private boolean releasedBonusAccrualEnabled;
    private boolean playableBonusAccrualEnabled;
    private Integer monthsToExpirePoints;

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getConversionRatio() {
        return conversionRatio;
    }

    public void setConversionRatio(Integer conversionRatio) {
        this.conversionRatio = conversionRatio;
    }

    public Integer getLoyaltyToRaw() {
        return loyaltyToRaw;
    }

    public void setLoyaltyToRaw(Integer loyaltyToRaw) {
        this.loyaltyToRaw = loyaltyToRaw;
    }

    public Integer getMinimumConversion() {
        return minimumConversion;
    }

    public void setMinimumConversion(Integer minimumConversion) {
        this.minimumConversion = minimumConversion;
    }

    public boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public boolean getIsAccrualEnabled() {
        return isAccrualEnabled;
    }

    public void setIsAccrualEnabled(boolean isAccrualEnabled) {
        this.isAccrualEnabled = isAccrualEnabled;
    }

    public Integer getBonusPlanId() {
        return bonusPlanId;
    }

    public void setBonusPlanId(Integer bonusPlanId) {
        this.bonusPlanId = bonusPlanId;
    }

    public Integer getAccrualMultiplier() {
        return accrualMultiplier;
    }

    public void setAccrualMultiplier(Integer accrualMultiplier) {
        this.accrualMultiplier = accrualMultiplier;
    }

    public boolean getReleasedBonusAccrualEnabled() {
        return releasedBonusAccrualEnabled;
    }

    public void setReleasedBonusAccrualEnabled(boolean releasedBonusAccrualEnabled) {
        this.releasedBonusAccrualEnabled = releasedBonusAccrualEnabled;
    }

    public boolean getPlayableBonusAccrualEnabled() {
        return playableBonusAccrualEnabled;
    }

    public void setPlayableBonusAccrualEnabled(boolean playableBonusAccrualEnabled) {
        this.playableBonusAccrualEnabled = playableBonusAccrualEnabled;
    }

    public Integer getMonthsToExpirePoints() {
        return monthsToExpirePoints;
    }

    public void setMonthsToExpirePoints(Integer monthsToExpirePoints) {
        this.monthsToExpirePoints = monthsToExpirePoints;
    }
}
