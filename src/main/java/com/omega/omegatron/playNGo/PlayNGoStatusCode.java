package com.omega.omegatron.playNGo;

/**
 * Created by songleo on 2016-04-07.
 */
public enum PlayNGoStatusCode {
    // Return Code and Msg
    RETURN_SUCCESS("0", "Request successfully."),
    RETURN_NOUSER("1", "User was not logged in."),
    RETURN_INTERNAL("2", "Internal server error."),
    RETURN_INVALIDCURRENCY("3", "An unsupported currency was specified."),
    RETURN_WRONGUSERNAMEPASSWORD("4", "Wrong username or password."),
    RETURN_ACCOUNTLOCKED("5", "Account is locked."),
    RETURN_ACCOUNTDISABLED("6", "Account is disabled."),
    RETURN_NOTENOUGHMONEY("7", "The requested amount is too high or too low."),
    RETURN_MAXCONCURRENTCALLS("8", "The system is unavailable for this request. Try again later."),
    RETURN_SPENDINGBUDGETEXCEEDED("9", "Responsible gaming limit (money) exceeded."),
    RETURN_SESSIONEXPIRED("10", "The player session has expired."),
    RETURN_TIMEBUDGETEXCEEDED("11", "Responsible gaming limit (time) exceeded."),
    RETURN_SERVICEUNAVAILABLE("12", "Service is unavailable for any reason but able to respond, similar to MAXCONCCURENTCALLS which is a specialised condition of SERVICEUNAVAILABLE.");

    private final String code;
    private final String msg;

    PlayNGoStatusCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
