package com.omega.omegatron.playNGo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by songleo on 2016-04-06.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="balance")
public class BalanceResponse {
    public BalanceResponse() {}

    @XmlElement(name="real")
    private String real;

    @XmlElement(name="currency")
    private String currency;

    @XmlElement(name="statusCode")
    private String statusCode;

    public String getReal() {
        return real;
    }

    public void setReal(String real) {
        this.real = real;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
