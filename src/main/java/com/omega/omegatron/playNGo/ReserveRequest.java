package com.omega.omegatron.playNGo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by songleo on 2016-04-06.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="reserve")
public class ReserveRequest {
    public ReserveRequest() {}

    @XmlElement(name="externalId")
    private String externalId;

    @XmlElement(name="productId")
    private String productId;

    @XmlElement(name="transactionId")
    private String transactionId;

    @XmlElement(name="real")
    private String real;

    @XmlElement(name="currency")
    private String currency;

    @XmlElement(name="gameId")
    private String gameId;

    @XmlElement(name="gameSessionId")
    private String gameSessionId;

    @XmlElement(name="contextId")
    private Integer contextId;

    @XmlElement(name="accessToken")
    private String accessToken;

    @XmlElement(name="roundId")
    private Long roundId;

    @XmlElement(name="externalGameSessionId")
    private String externalGameSessionId;

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getReal() {
        return real;
    }

    public void setReal(String real) {
        this.real = real;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getGameSessionId() {
        return gameSessionId;
    }

    public void setGameSessionId(String gameSessionId) {
        this.gameSessionId = gameSessionId;
    }

    public Integer getContextId() {
        return contextId;
    }

    public void setContextId(Integer contextId) {
        this.contextId = contextId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Long getRoundId() {
        return roundId;
    }

    public void setRoundId(Long roundId) {
        this.roundId = roundId;
    }

    public String getExternalGameSessionId() {
        return externalGameSessionId;
    }

    public void setExternalGameSessionId(String externalGameSessionId) {
        this.externalGameSessionId = externalGameSessionId;
    }
}
