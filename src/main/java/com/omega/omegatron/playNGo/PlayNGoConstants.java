package com.omega.omegatron.playNGo;

/**
 * Created by songleo on 2016-04-06.
 */
public class PlayNGoConstants {

    // request type
    public static final String AUTHENTICATE_REQUEST         = "<authenticate>";
    public static final String RESERVE_REQUEST              = "<reserve>";
    public static final String RELEASE_REQUEST              = "<release>";
    public static final String BALANCE_REQUEST              = "<balance>";
    public static final String CANCELRESERVE_REQUEST        = "<cancelReserve>";

    // authenticate
    public static final String REQUEST_USERNAME             = "username";
    public static final String REQUEST_PRODUCTID            = "productId";
    public static final String REQUEST_CLIENT               = "client";
    public static final String REQUEST_ACCESSTOKEN          = "accessToken";
    public static final String REQUEST_GAMEID               = "gameId";

    // reserve
    public static final String REQUEST_EXTERNALID           = "externalId";
    public static final String REQUEST_TRANSACTIONID        = "transactionId";
    public static final String REQUEST_REAL                 = "real";
    public static final String REQUEST_CURRENCY             = "currency";
    public static final String REQUEST_GAMESESSIONID        = "gameSessionId";
    public static final String REQUEST_ROUNDID              = "roundId";

    // release
    public static final String REQUEST_STATE                = "state";
    public static final String REQUEST_TYPE                 = "type";
    public static final String REQUEST_JACKPOTGAIN          = "jackpotGain";
    public static final String REQUEST_JACKPOTLOSS          = "jackpotLoss";
    public static final String REQUEST_JACKPOTGAINSEED      = "jackpotGainSeed";
    public static final String REQUEST_JACKPOTGAINID        = "jackpotGainID";

    public static final String RETURN_ERROR_CODE            = "errorCode";
    public static final String RETURN_ERROR_MESSAGE         = "errorMessage";

    public static final String RETURN_OK_MESSAGE            = "ok";

// session states
    public static final Integer SESSION_STATE_OPEN           = 0;
    public static final Integer SESSION_STATE_CLOSED         = 1;

    // transaction type
    public static final Integer TRANSACTION_TYPE_REAL        = 0;
    public static final Integer TRANSACTION_TYPE_PROMOTIONAL = 1;
}
