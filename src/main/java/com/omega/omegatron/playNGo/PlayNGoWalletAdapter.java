package com.omega.omegatron.playNGo;

/**
 * Created by songleo on 2016-04-06.
 */
public interface PlayNGoWalletAdapter {
    /**
     * Process incoming requests, Takes json string as input and returns json as output.
     */
    public String processRequest(String jsonString);

}
