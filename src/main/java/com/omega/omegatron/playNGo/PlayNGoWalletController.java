package com.omega.omegatron.playNGo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;

/**
 * Created by songleo on 2016-04-06.
 */

@Controller
public class PlayNGoWalletController {
    static DecimalFormat amountFormat = new DecimalFormat("######0.00;-######0.00");
    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

    private PlayNGoWalletAdapter playNGoWalletAdapter;


    @RequestMapping("/playNGoWallet")
    public ModelAndView execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String method = request.getMethod();
        String ipAddress = request.getRemoteAddr();
        BufferedReader br = request.getReader();
        String line = null;
        StringBuffer requestString = new StringBuffer();
        while((line=br.readLine())!=null){
            requestString.append(line + "\n");
        }

        // for empty request, return a simple message (used to test if service is up)
        String respXml = "";
        if (requestString.toString().equals("")) {
            respXml = "Play'N Go is running. To use, please pass valid request string.";
        } else {
            String jsonString = requestString.toString();

            // send the request to the walletAdapter
            respXml = playNGoWalletAdapter.processRequest(jsonString);
            log.info("Response: " + respXml);
        }

        response.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();
        writer.write(respXml);
        return null;
    }

    public PlayNGoWalletAdapter getPlayNGoWalletAdapter() {
        return playNGoWalletAdapter;
    }

    @Autowired
    public void setPlayNGoWalletAdapter(PlayNGoWalletAdapter playNGoWalletAdapter) {
        this.playNGoWalletAdapter = playNGoWalletAdapter;
    }
}
