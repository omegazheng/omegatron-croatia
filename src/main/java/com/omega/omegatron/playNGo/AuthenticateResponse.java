package com.omega.omegatron.playNGo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by songleo on 2016-04-06.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="authenticate")
public class AuthenticateResponse {
    public AuthenticateResponse() {}

    @XmlElement(name="externalId")
    private String externalId;

    @XmlElement(name="statusCode")
    private String statusCode;

    @XmlElement(name="statusMessage")
    private String statusMessage;

    @XmlElement(name="userCurrency")
    private String userCurrency;

    @XmlElement(name="nickname")
    private String nickname;

    @XmlElement(name="country")
    private String country;

    @XmlElement(name="birthdate")
    private String birthdate;

    @XmlElement(name="registration")
    private String registration;

    @XmlElement(name="language")
    private String language;

    @XmlElement(name="affiliateId")
    private String affiliateId;

    @XmlElement(name="real")
    private String real;

    @XmlElement(name="gender")
    private String gender;

    @XmlElement(name="externalGameSessionId")
    private String externalGameSessionId;

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getUserCurrency() {
        return userCurrency;
    }

    public void setUserCurrency(String userCurrency) {
        this.userCurrency = userCurrency;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getAffiliateId() {
        return affiliateId;
    }

    public void setAffiliateId(String affiliateId) {
        this.affiliateId = affiliateId;
    }

    public String getReal() {
        return real;
    }

    public void setReal(String real) {
        this.real = real;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getExternalGameSessionId() {
        return externalGameSessionId;
    }

    public void setExternalGameSessionId(String externalGameSessionId) {
        this.externalGameSessionId = externalGameSessionId;
    }
}
