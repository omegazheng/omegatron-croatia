package com.omega.omegatron.playNGo;

/**
 * Created by songleo on 2016-04-07.
 */
public class PlayNGoErrorException extends Exception {
    private String errorCode;
    private String errorMessage;

    public PlayNGoErrorException(String errorCode, String errormessage) {
        this.errorCode = errorCode;
        this.errorMessage = errormessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
