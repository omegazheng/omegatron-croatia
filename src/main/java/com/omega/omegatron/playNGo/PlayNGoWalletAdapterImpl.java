package com.omega.omegatron.playNGo;

import com.omega.commons.service.PlatformService;
import com.omega.commons.service.TransactionTagService;
import com.omega.commons.service.UserService;
import com.omega.commonsUtil.BigDecimalUtil;
import com.omega.commonsUtil.DecimalFormatUtil;
import com.omega.commonsUtil.JAXBUtil;
import com.omega.omegatron.constants.OmegatronConstants;
import com.omega.omegatron.core.*;
import com.omega.omegatron.service.GameInstanceService;
import com.omega.omegatron.service.RegistryService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by songleo on 2016-04-06.
 */
public class PlayNGoWalletAdapterImpl implements PlayNGoWalletAdapter {
    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());
    private TransactionService transactionService;
    private RegistryService registryService;
    private JdbcTemplate jdbcTemplate;
    private GameInstanceService gameInstanceService;
    private PlatformService platformService;
    private UserService userService;
    private TransactionTagService transactionTagService;



    /** Main method for handling requests. Parse the jsonString and routes appropriately */
    public String processRequest(String jsonString) {
        log.info("Processing PlayNGoWalletAdapterImpl json request :: " + jsonString);

        String response = null;

        try {
            if(jsonString.contains(PlayNGoConstants.AUTHENTICATE_REQUEST)) {
                AuthenticateRequest authenticateRequest = JAXBUtil.toObject(jsonString, AuthenticateRequest.class);
                response = processAuthenticateRequest(authenticateRequest);
            } else if(jsonString.contains(PlayNGoConstants.RESERVE_REQUEST)) {
                ReserveRequest reserveRequest = JAXBUtil.toObject(jsonString, ReserveRequest.class);
                response = processReserveRequest(reserveRequest);
            } else if(jsonString.contains(PlayNGoConstants.RELEASE_REQUEST)) {
                ReleaseRequest releaseRequest = JAXBUtil.toObject(jsonString, ReleaseRequest.class);
                response = processReleaseRequest(releaseRequest);
            } else if(jsonString.contains(PlayNGoConstants.BALANCE_REQUEST)) {
                BalanceRequest balanceRequest = JAXBUtil.toObject(jsonString, BalanceRequest.class);
                response = processBalanceRequest(balanceRequest);
            } else if(jsonString.contains(PlayNGoConstants.CANCELRESERVE_REQUEST)) {
                CancelReserveRequest cancelReserveRequest = JAXBUtil.toObject(jsonString, CancelReserveRequest.class);
                response = processCancelReserveRequest(cancelReserveRequest);
            }
        }
        catch(Exception e) {
            log.error("Exception:" + e.getMessage());
            e.printStackTrace();
        }

        return response;
    }

    private String processAuthenticateRequest(AuthenticateRequest authenticateRequest) throws IOException {
        log.debug("Play'N Go process authentic request. username=" + authenticateRequest.getUsername() + " productId=" + authenticateRequest.getProductId() +
                " client=" + authenticateRequest.getClient() + " CID=" + authenticateRequest.getCID() + " clientIP=" + authenticateRequest.getClientIP() +
                " contextId=" + authenticateRequest.getContextId() + " accessToken=" + authenticateRequest.getAccessToken() +
                " language=" + authenticateRequest.getLanguage() + " gameId=" + authenticateRequest.getGameId());

        AuthenticateResponse authenticateResponse = new AuthenticateResponse();
        try {
            validateAuthenticRequestParamemters(authenticateRequest);

            Integer partyId = validateSessionByToken(authenticateRequest.getUsername());
            Integer brandId = userService.getBrandIdByPartyId(partyId);

            validateAccessToken(authenticateRequest.getAccessToken());

            // process
            com.omega.omegatron.core.BalanceResponse balResponse = transactionService.getBalance(partyId, null, OmegatronConstants.PLATFORM_PLAYNGO, authenticateRequest.getGameId());
            if (balResponse.getTranServiceError() != null) {
                setErrorException(balResponse.getTranServiceError());
                return null;
            }

            Map<String, Object> userResult = getUserDetail(partyId);
            authenticateResponse.setExternalId(partyId.toString());
            authenticateResponse.setStatusCode(PlayNGoStatusCode.RETURN_SUCCESS.getCode());
            authenticateResponse.setStatusMessage(PlayNGoConstants.RETURN_OK_MESSAGE);
            authenticateResponse.setUserCurrency((String) userResult.get("CURRENCY"));
            authenticateResponse.setCountry((String)userResult.get("COUNTRY"));

            Timestamp birthdate = (Timestamp) userResult.get("BIRTHDATE");
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            authenticateResponse.setBirthdate(fmt.format(birthdate));
            authenticateResponse.setNickname((String)userResult.get("NICKNAME"));

            Timestamp regdate = (Timestamp) userResult.get("REG_DATE");
            authenticateResponse.setRegistration(fmt.format(regdate));

            authenticateResponse.setLanguage(((String)userResult.get("LANGUAGE")).toUpperCase());
            authenticateResponse.setAffiliateId((String)userResult.get("CODE_KEY"));

            BigDecimal totalBalance = balResponse.getBalanceReal().add(balResponse.getBalanceReleasedBonus()).add((balResponse.getBalancePlayableBonus()));
            authenticateResponse.setReal(DecimalFormatUtil.formatAmount(totalBalance));

            String gender = (String)userResult.get("GENDER");
            gender = gender == null ? "m" : gender.toLowerCase();
            authenticateResponse.setGender(gender);
            authenticateResponse.setExternalGameSessionId(authenticateRequest.getUsername());
            authenticateResponse.setAffiliateId(brandId.toString());

            updateSession(partyId);
        } catch(PlayNGoErrorException pngExp) {
            authenticateResponse.setStatusCode(pngExp.getErrorCode());
            authenticateResponse.setStatusMessage(pngExp.getErrorMessage());
            log.error("PlayNGoErrorException " + pngExp.getErrorMessage());
        }
        catch(Exception e) {
            authenticateResponse.setStatusCode(PlayNGoStatusCode.RETURN_INTERNAL.getCode());
            authenticateResponse.setStatusMessage(PlayNGoStatusCode.RETURN_INTERNAL.getMsg() + " " + e.getMessage());
            log.error("Exception " + e.getMessage());
        }

        return setResponseToXml(authenticateResponse);
    }

    private String processReserveRequest(ReserveRequest reserveRequest) throws IOException {
        log.debug("Play'N Go process reserve request. externalId=" + reserveRequest.getExternalId() + " productId=" + reserveRequest.getProductId() +
                " transactionId=" + reserveRequest.getTransactionId() + " real=" + reserveRequest.getReal() + " currency=" + reserveRequest.getCurrency() +
                " gameId=" + reserveRequest.getGameId() + " gameSessionId=" + reserveRequest.getGameSessionId() +
                " contextId=" + reserveRequest.getContextId() + " accessToken=" + reserveRequest.getAccessToken() +
                " roundId=" + reserveRequest.getRoundId() + " externalGameSessionId=" + reserveRequest.getExternalGameSessionId());

        ReserveResponse reserveResponse = new ReserveResponse();
        try {
            validateReserveRequestParamemters(reserveRequest);

            Integer partyId = validateSessionByTokenAndPartyId(reserveRequest.getExternalGameSessionId(), Integer.parseInt(reserveRequest.getExternalId()));

            validateAccessToken(reserveRequest.getAccessToken());

            // process
            ProcessTransactionRequest ptRequest = new ProcessTransactionRequest();
            ptRequest.setAmount(DecimalFormatUtil.toBigDecimal(reserveRequest.getReal()).negate());
            ptRequest.setCurrency(reserveRequest.getCurrency());
            ptRequest.setGameId(reserveRequest.getGameId());
            ptRequest.setPartyId(partyId);
            ptRequest.setGameTranId(reserveRequest.getRoundId().toString());
            ptRequest.setProviderTranId(reserveRequest.getTransactionId());
            ptRequest.setTranType(OmegatronConstants.TRAN_TYPE_GAME_BET);
            ptRequest.setPlatformCode(OmegatronConstants.PLATFORM_PLAYNGO);

            ProcessTransactionResponse txnResponse = transactionService.processTransaction(ptRequest);
            if (txnResponse.getTranServiceError() != null) {
                setErrorException(txnResponse.getTranServiceError());
                return null;
            }

            Long omegaTransactionId = txnResponse.getTransactionId();
            reserveResponse.setExternalTransactionId(omegaTransactionId.toString());
            BigDecimal totalBalance = txnResponse.getBalanceReal().add(txnResponse.getBalanceReleasedBonus()).add(txnResponse.getBalancePlayableBonus());
            reserveResponse.setReal(DecimalFormatUtil.formatAmount(BigDecimalUtil.setScale(totalBalance)));
            reserveResponse.setCurrency(txnResponse.getCurrencyCode());
            reserveResponse.setStatusCode(PlayNGoStatusCode.RETURN_SUCCESS.getCode());
            reserveResponse.setStatusMessage(PlayNGoConstants.RETURN_OK_MESSAGE);

            updateSession(partyId);
        } catch(PlayNGoErrorException pngExp) {
            reserveResponse.setStatusCode(pngExp.getErrorCode());
            reserveResponse.setStatusMessage(pngExp.getErrorMessage());
            reserveResponse.setReal("0.00");
            reserveResponse.setCurrency(reserveRequest.getCurrency());
            log.error("PlayNGoErrorException " + pngExp.getErrorMessage());
        }
        catch(Exception e) {
            reserveResponse.setStatusCode(PlayNGoStatusCode.RETURN_INTERNAL.getCode());
            reserveResponse.setStatusMessage(PlayNGoStatusCode.RETURN_INTERNAL.getMsg() + " " + e.getMessage());
            reserveResponse.setReal("0.00");
            reserveResponse.setCurrency(reserveRequest.getCurrency());
            log.error("Exception " + e.getMessage());
        }

        return setResponseToXml(reserveResponse);
    }

    private String processReleaseRequest(ReleaseRequest releaseRequest) throws IOException {
        log.debug("Play'N Go process release request. externalId=" + releaseRequest.getExternalId() + " productId=" + releaseRequest.getProductId() +
                " transactionId=" + releaseRequest.getTransactionId() + " real=" + releaseRequest.getReal() + " currency=" + releaseRequest.getCurrency() +
                " gameSessionId=" + releaseRequest.getGameSessionId() + " contextId=" + releaseRequest.getContextId() +
                " state=" + releaseRequest.getState() + " totalLoss=" + releaseRequest.getTotalLoss() +
                " totalGain=" + releaseRequest.getTotalGain() + " numRounds=" + releaseRequest.getNumRounds() +
                " type=" + releaseRequest.getType() + " gameId=" + releaseRequest.getGameId() + " accessToken=" + releaseRequest.getAccessToken() +
                " roundId=" + releaseRequest.getRoundId() + " jackpotGain=" + releaseRequest.getJackpotGain() +
                " jackpotLoss=" + releaseRequest.getJackpotLoss() + " jackpotGainSeed=" + releaseRequest.getJackpotGainSeed() +
                " jackpotGainId=" + releaseRequest.getJackpotGainID() + " freegameExternalId=" + releaseRequest.getFreegameExternalId() +
                " turnover=" + releaseRequest.getTurnover() + " freeganeFinished=" + releaseRequest.getFreegameFinished() +
                " freegamegain=" + releaseRequest.getFreegameGain() + " freegameLoss=" + releaseRequest.getFreegameLoss() +
                " externalGameSessionId=" + releaseRequest.getExternalGameSessionId());

        ReleaseResponse releaseResponse = new ReleaseResponse();
        try {
            validateReleaseRequestParamemters(releaseRequest);

            // LSL-340 Allow PNG to update GAME_WIN without session validation
//            Integer partyId = validateSessionByTokenAndPartyId(releaseRequest.getExternalGameSessionId(), Integer.parseInt(releaseRequest.getExternalId()));
            Integer partyId = Integer.parseInt(releaseRequest.getExternalId());

            validateAccessToken(releaseRequest.getAccessToken());

            // process
            boolean isCloseRound = true;
            BigDecimal amount = DecimalFormatUtil.toBigDecimal(releaseRequest.getReal());
//            ProcessTransactionResponse  txnResponse = transactionService.checkTransaction(releaseRequest.getTransactionId(), OmegatronConstants.PLATFORM_PLAYNGO);
//            boolean transactionFound = txnResponse != null;
//            if (!transactionFound) {
//                // amount will be 0 when the player lost the round
//                if(amount.compareTo(BigDecimal.ZERO) == 0) {
//                    // normal win ok, but for free spin win will return error. So just return balance
//                    com.omega.omegatron.core.BalanceResponse balanceResponse = transactionService.getBalance(partyId, releaseRequest.getCurrency(), OmegatronConstants.PLATFORM_PLAYNGO, releaseRequest.getGameId());
//                    if (balanceResponse.getTranServiceError() != null) {
//                        setErrorException(balanceResponse.getTranServiceError());
//                        return null;
//                    }
//
//                    Long omegaTransactionId = txnResponse.getTransactionId();
//                    releaseResponse.setExternalTransactionId(omegaTransactionId.toString());
//                    BigDecimal totalBalance = balanceResponse.getBalanceReal().add(balanceResponse.getBalanceReleasedBonus()).add(balanceResponse.getBalancePlayableBonus());
//                    releaseResponse.setReal(DecimalFormatUtil.formatAmount(totalBalance));
//                    releaseResponse.setCurrency(balanceResponse.getCurrencyCode());
//                    releaseResponse.setStatusCode(PlayNGoStatusCode.RETURN_SUCCESS.getCode());
//                }
//            } else {


            // summary call. real and roundid = 0 ==> just return normal response
            if (releaseRequest.getState() == PlayNGoConstants.SESSION_STATE_CLOSED) {
                com.omega.omegatron.core.BalanceResponse balanceResponse = transactionService.getBalance(partyId, releaseRequest.getCurrency(), OmegatronConstants.PLATFORM_PLAYNGO, releaseRequest.getGameId());
                if (balanceResponse.getTranServiceError() != null) {
                    setErrorException(balanceResponse.getTranServiceError());
                    return null;
                }

                releaseResponse.setExternalTransactionId("");
                BigDecimal totalBalance = balanceResponse.getBalanceReal().add(balanceResponse.getBalanceReleasedBonus()).add(balanceResponse.getBalancePlayableBonus());
                releaseResponse.setReal(DecimalFormatUtil.formatAmount(BigDecimalUtil.setScale(totalBalance)));
                releaseResponse.setCurrency(balanceResponse.getCurrencyCode());
                releaseResponse.setStatusCode(PlayNGoStatusCode.RETURN_SUCCESS.getCode());
            } else {
                ProcessTransactionRequest ptRequest = new ProcessTransactionRequest();
                ptRequest.setAmount(amount);
                ptRequest.setCurrency(releaseRequest.getCurrency());
                ptRequest.setGameId(releaseRequest.getGameId());
                ptRequest.setPartyId(partyId);
                ptRequest.setGameTranId(releaseRequest.getRoundId().toString());
                ptRequest.setProviderTranId(releaseRequest.getTransactionId());
                ptRequest.setPlatformCode(OmegatronConstants.PLATFORM_PLAYNGO);
                ptRequest.setTranType(OmegatronConstants.TRAN_TYPE_GAME_WIN);
//                ptRequest.setIsFinal(isCloseRound); // TODO
//                if (isFreeRound) { // TODO, ask Play'nGo so many times, no confirm, maybe they do not seperate this
//                    ptRequest.setTranType(OmegatronConstants.TRAN_TYPE_PLATFORM_BONUS);
//                }

                if(!StringUtils.isEmpty(releaseRequest.getFreegameExternalId())) {
//                    log.info("***** releaseRequest.getFreegameExternalId()=" + releaseRequest.getFreegameExternalId() + "****");
                    ptRequest.setTranType(OmegatronConstants.TRAN_TYPE_PLATFORM_BONUS); // free game
                }

                if(!StringUtils.isEmpty(releaseRequest.getFreegameExternalId()) && amount.compareTo(BigDecimal.ZERO) == 0) {
                    // normal win ok, but for free spin win & amount==0 will return error. So just return balance
                    com.omega.omegatron.core.BalanceResponse balanceResponse = transactionService.getBalance(partyId, releaseRequest.getCurrency(), OmegatronConstants.PLATFORM_PLAYNGO, releaseRequest.getGameId());
                    if (balanceResponse.getTranServiceError() != null) {
                        setErrorException(balanceResponse.getTranServiceError());
                        return null;
                    }

                    releaseResponse.setExternalTransactionId("");
                    BigDecimal totalBalance = balanceResponse.getBalanceReal().add(balanceResponse.getBalanceReleasedBonus()).add(balanceResponse.getBalancePlayableBonus());
                    releaseResponse.setReal(DecimalFormatUtil.formatAmount(BigDecimalUtil.setScale(totalBalance)));
                    releaseResponse.setCurrency(balanceResponse.getCurrencyCode());
                    releaseResponse.setStatusCode(PlayNGoStatusCode.RETURN_SUCCESS.getCode());
                } else {

                    ProcessTransactionResponse txnResponse = transactionService.processTransaction(ptRequest);

                    if (txnResponse.getTranServiceError() != null) {
                        setErrorException(txnResponse.getTranServiceError());
                        return null;
                    }

                    if (isCloseRound) { // also isFreeRound?
                        Integer platformId = platformService.getPlatformIdFromCode(OmegatronConstants.PLATFORM_PLAYNGO);
                        gameInstanceService.endGame(partyId, releaseRequest.getRoundId().toString(), platformId);
                    }

                    // create JackpotTag
                    // win amount=0 will not insert into DB, sometimes jackpotGain=0
                    if(txnResponse.getTransactionId() > 0 && !StringUtils.isEmpty(releaseRequest.getJackpotGain())) {
                        BigDecimal jackpotGainAmount = new BigDecimal(releaseRequest.getJackpotGain());
                        if(jackpotGainAmount.compareTo(BigDecimal.ZERO) > 0) {
                            Integer brandId = userService.getBrandIdByPartyId(partyId);
                            transactionTagService.createTransactionTagAccountTran(brandId, "jackpot", txnResponse.getTransactionId());
                        }
                    }

                    Long omegaTransactionId = txnResponse.getTransactionId();
                    releaseResponse.setExternalTransactionId(omegaTransactionId.toString());
                    BigDecimal totalBalance = txnResponse.getBalanceReal().add(txnResponse.getBalanceReleasedBonus()).add(txnResponse.getBalancePlayableBonus());
                    releaseResponse.setReal(DecimalFormatUtil.formatAmount(BigDecimalUtil.setScale(totalBalance)));
                    releaseResponse.setCurrency(txnResponse.getCurrencyCode());
                    releaseResponse.setStatusCode(PlayNGoStatusCode.RETURN_SUCCESS.getCode());
                }
            }

            updateSession(partyId);
        } catch(PlayNGoErrorException pngExp) {
            releaseResponse.setStatusCode(pngExp.getErrorCode());
            releaseResponse.setCurrency(releaseRequest.getCurrency());
            releaseResponse.setReal("0.00");
            log.error("PlayNGoErrorException " + pngExp.getErrorMessage());
        }
        catch(Exception e) {
            releaseResponse.setStatusCode(PlayNGoStatusCode.RETURN_INTERNAL.getCode());
            releaseResponse.setCurrency(releaseRequest.getCurrency());
            releaseResponse.setReal("0.00");
            log.error("Exception " + e.getMessage());
        }

        return setResponseToXml(releaseResponse);
    }

    private String processBalanceRequest(BalanceRequest balanceRequest) throws IOException {
        log.debug("Play'N Go process balance request. externalId=" + balanceRequest.getExternalId() + " productId=" + balanceRequest.getProductId() +
                " currency=" + balanceRequest.getCurrency() + " gameId=" + balanceRequest.getGameId() +
                " accessToken=" + balanceRequest.getAccessToken() + " externalGameSessionId=" + balanceRequest.getExternalGameSessionId());

        BalanceResponse balanceResponse = new BalanceResponse();
        try {
            validateBalanceRequestParamemters(balanceRequest);

            Integer partyId = validateSessionByTokenAndPartyId(balanceRequest.getExternalGameSessionId(), Integer.parseInt(balanceRequest.getExternalId()));

            validateAccessToken(balanceRequest.getAccessToken());

            // process
            com.omega.omegatron.core.BalanceResponse balResponse = transactionService.getBalance(partyId, balanceRequest.getCurrency(), OmegatronConstants.PLATFORM_PLAYNGO, balanceRequest.getGameId());
            if (balResponse.getTranServiceError() != null) {
                setErrorException(balResponse.getTranServiceError());
                return null;
            }

            BigDecimal totalBalance = balResponse.getBalanceReal().add(balResponse.getBalanceReleasedBonus()).add(balResponse.getBalancePlayableBonus());
            balanceResponse.setReal(DecimalFormatUtil.formatAmount(BigDecimalUtil.setScale(totalBalance)));
            balanceResponse.setCurrency(balResponse.getCurrencyCode());
            balanceResponse.setStatusCode(PlayNGoStatusCode.RETURN_SUCCESS.getCode());

            updateSession(partyId);
        } catch(PlayNGoErrorException pngExp) {
            balanceResponse.setStatusCode(pngExp.getErrorCode());
            balanceResponse.setCurrency("");
            balanceResponse.setReal("0.00");
            log.error("PlayNGoErrorException " + pngExp.getErrorMessage());
        }
        catch(Exception e) {
            balanceResponse.setStatusCode(PlayNGoStatusCode.RETURN_INTERNAL.getCode());
            balanceResponse.setCurrency("");
            balanceResponse.setReal("0.00");
            log.error("Exception " + e.getMessage());
        }

        return setResponseToXml(balanceResponse);
    }

    private String processCancelReserveRequest(CancelReserveRequest cancelReserveRequest) throws IOException {
        log.debug("Play'N Go process CancelReserve request. externalId=" + cancelReserveRequest.getExternalId() + " productId=" + cancelReserveRequest.getProductId() +
                " transactionId=" + cancelReserveRequest.getTransactionId() + " real=" + cancelReserveRequest.getReal() + " currency=" + cancelReserveRequest.getCurrency() +
                " gameSessionId=" + cancelReserveRequest.getGameSessionId() + " accessToken=" + cancelReserveRequest.getAccessToken() +
                " roundId=" + cancelReserveRequest.getRoundId() + " gameId=" + cancelReserveRequest.getGameId() +
                " externalGameSessionId=" + cancelReserveRequest.getExternalGameSessionId());

        CancelReserveResponse cancelReserveResponse = new CancelReserveResponse();
        cancelReserveResponse.setTransactionId(cancelReserveRequest.getTransactionId());
        try {
            validateCancelReserveRequestParamemters(cancelReserveRequest);

            // Check wager exists => no exist + no need to check user -> also return ok
            ProcessTransactionResponse  acctTxnResponse = transactionService.checkTransaction(cancelReserveRequest.getTransactionId(), OmegatronConstants.PLATFORM_PLAYNGO);
            if (acctTxnResponse == null) {
                cancelReserveResponse.setStatusCode(PlayNGoStatusCode.RETURN_SUCCESS.getCode());
            } else {
                // LSL-174. Play'NGo free spin wanted
//                Integer partyId = validateSessionByTokenAndPartyId(cancelReserveRequest.getExternalGameSessionId(), Integer.parseInt(cancelReserveRequest.getExternalId()));
                Integer partyId = Integer.parseInt(cancelReserveRequest.getExternalId());

                validateAccessToken(cancelReserveRequest.getAccessToken());

                // process
                RollbackTransactionResponse txnResponse =
                        transactionService.rollbackTransactionV1(cancelReserveRequest.getTransactionId(), OmegatronConstants.PLATFORM_PLAYNGO);
                if (txnResponse.getTranServiceError() != null) {
                    setErrorException(txnResponse.getTranServiceError());
                    return null;
                }

                Long omegaTransactionId = txnResponse.getRollbackTranId();
                cancelReserveResponse.setExternalTransactionId(omegaTransactionId.toString());
                cancelReserveResponse.setStatusCode(PlayNGoStatusCode.RETURN_SUCCESS.getCode());

                updateSession(partyId);
            }

        } catch(PlayNGoErrorException pngExp) {
            cancelReserveResponse.setStatusCode(pngExp.getErrorCode());
            log.error("PlayNGoErrorException " + pngExp.getErrorMessage());
        }
        catch(Exception e) {
            cancelReserveResponse.setStatusCode(PlayNGoStatusCode.RETURN_INTERNAL.getCode());
            log.error("Exception " + e.getMessage());
        }

        return setResponseToXml(cancelReserveResponse);
    }

    private Map<String, Object> getUserDetail(Integer partyId)throws PlayNGoErrorException {
        log.debug("getUserDetail for partyId=" + partyId);

        String query =
                "select" +
                        "  users.partyid, users.CURRENCY, users.NICKNAME, users.COUNTRY, users.BIRTHDATE, users.REG_DATE, users.LANGUAGE, users.GENDER, userTracking.CODE_KEY" +
                        " from" +
                        "  external_mpt.user_conf users with (nolock) " +
                        " left join USER_TRACKING_CODE userTracking on users.PARTYID = userTracking.PARTYID" +
                        " where" +
                        "  users.PARTYID = ?";

        List<Map<String, Object>> result = jdbcTemplate.queryForList(query, partyId);

        if(result.size() == 0){
            log.error("Result is empty");
            throw new PlayNGoErrorException(PlayNGoStatusCode.RETURN_NOUSER.getCode(), "Player not found. partyId=" + partyId);
        }

        return result.get(0);
    }

    private void setErrorException(TranServiceError tranServiceError) throws PlayNGoErrorException {
        Map<String, String> errorReturn = translateError(tranServiceError);
        String errorCode = errorReturn.get(PlayNGoConstants.RETURN_ERROR_CODE);
        String errorMessage = errorReturn.get(PlayNGoConstants.RETURN_ERROR_MESSAGE);
        throw new PlayNGoErrorException(errorCode, errorMessage);
    }

    private Map<String, String> translateError(TranServiceError tranServiceError) {
        Map<String, String> returnResult = new HashMap<String, String>();

        if (tranServiceError == null) {
            returnResult.put(PlayNGoConstants.RETURN_ERROR_CODE, PlayNGoStatusCode.RETURN_SUCCESS.getCode());
            returnResult.put(PlayNGoConstants.RETURN_ERROR_MESSAGE, "Success!");
        } else {
            switch (tranServiceError) {
                case PLAYER_NOT_FOUND:
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_CODE, PlayNGoStatusCode.RETURN_NOUSER.getCode());
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_MESSAGE, "Player not found by server.");
                    break;
                case INSUFFICIENT_FUNDS:
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_CODE, PlayNGoStatusCode.RETURN_NOTENOUGHMONEY.getCode());
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_MESSAGE, "Insufficient Funds.");
                    break;
                case TRAN_NOT_FOUND:
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_CODE, PlayNGoStatusCode.RETURN_SERVICEUNAVAILABLE.getCode());
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_MESSAGE, "An error occurred during cancelRelease request. The wager been cancelled was not present in the integrator's system.");
                    break;
                case GAME_NOT_CONFIGURED:
                case BONUS_PLAN_NOT_FOUND:
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_CODE, PlayNGoStatusCode.RETURN_INTERNAL.getCode());
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_MESSAGE, "The game is not configured for the operator.");
                    break;
                case WAGER_LIMIT_HIT:
                case LOSS_LIMIT_HIT:
                case SESSION_LIMIT:
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_CODE, PlayNGoStatusCode.RETURN_SPENDINGBUDGETEXCEEDED.getCode());
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_MESSAGE, "The player is not allowed to perform the bet due to gaming limits in the wallet platform.");
                    break;
                case MISSING_REQ_FIELDS:
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_CODE, PlayNGoStatusCode.RETURN_INTERNAL.getCode());
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_MESSAGE, "Missing required parameter(s).");
                    break;
                case INVALID_AMOUNT:
                case POSITIVE_BET:
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_CODE, PlayNGoStatusCode.RETURN_INTERNAL.getCode());
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_MESSAGE, "The request contains an unexpected parameter value.");
                    break;
                case PLAYER_LOCKED:
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_CODE, PlayNGoStatusCode.RETURN_ACCOUNTLOCKED.getCode());
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_MESSAGE, "The account associated with a given session ID is blocked and no bets can be performed.");
                    break;
                default:
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_CODE, PlayNGoStatusCode.RETURN_INTERNAL.getCode());
                    returnResult.put(PlayNGoConstants.RETURN_ERROR_MESSAGE, "A technical error occurred while processing the request.");
                    log.error("TranServiceError other error=" + tranServiceError.toString());
                    break;
            }
        }

        return returnResult;
    }

    private Integer validateSessionByToken(String sessionKey) throws PlayNGoErrorException{
//        return 91429638;  // for testing

        // check session
        Integer returnPartyId = transactionService.getPartyId(sessionKey);
        if (returnPartyId == null) {
            throw new PlayNGoErrorException(PlayNGoStatusCode.RETURN_NOUSER.getCode(), "Player not found. username=" + sessionKey);
        }

        return returnPartyId;
    }

    private Integer validateSessionByTokenAndPartyId(String sessionKey, Integer partyId) throws PlayNGoErrorException{
        Integer returnPartyId = validateSessionByToken(sessionKey);
        if (returnPartyId.compareTo(partyId) != 0) {
            throw new PlayNGoErrorException(PlayNGoStatusCode.RETURN_NOUSER.getCode(), "Player not found. username=" + sessionKey);
        }

        return returnPartyId;
    }

    private boolean validateAccessToken(String accessToken) throws PlayNGoErrorException {
        boolean isValid = false;

        String secretToken = registryService.getValue(OmegatronConstants.PLAYNGO_ACCESS_TOKEN_KEY);
        if(secretToken == null) {
            throw new RuntimeException("Omega server do not set Play'N Go accessToken yet!");
        }

        // check the session is empty
        if(!secretToken.equals(accessToken)) {
            throw new PlayNGoErrorException(PlayNGoStatusCode.RETURN_WRONGUSERNAMEPASSWORD.getCode(), "Wrong accessToken. accessToken=" + accessToken);
        }

        isValid = true;
        return isValid;
    }

    private boolean validateAuthenticRequestParamemters(AuthenticateRequest request) throws PlayNGoErrorException {
        boolean isValid = false;
        List missingParameters = new ArrayList();

        if(StringUtils.isEmpty(request.getUsername())) {
            missingParameters.add(PlayNGoConstants.REQUEST_USERNAME);
        }
        if(StringUtils.isEmpty(request.getProductId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_PRODUCTID);
        }
//        if(StringUtils.isEmpty(request.getClient())) { // document has, but demo no
//            missingParameters.add(PlayNGoConstants.REQUEST_CLIENT);
//        }
        if(StringUtils.isEmpty(request.getAccessToken())) {
            missingParameters.add(PlayNGoConstants.REQUEST_ACCESSTOKEN);
        }
        if(StringUtils.isEmpty(request.getGameId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_GAMEID);
        }

        if(missingParameters.size() > 0) {
            throw new PlayNGoErrorException(PlayNGoStatusCode.RETURN_INTERNAL.getCode(), PlayNGoStatusCode.RETURN_INTERNAL.getMsg() + "Missing paramemters: " + missingParameters);
        }

        isValid = true;
        return isValid;
    }

    private boolean validateReserveRequestParamemters(ReserveRequest request) throws PlayNGoErrorException {
        boolean isValid = false;
        List missingParameters = new ArrayList();

        if(StringUtils.isEmpty(request.getExternalId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_EXTERNALID);
        }
        if(StringUtils.isEmpty(request.getProductId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_PRODUCTID);
        }
        if(StringUtils.isEmpty(request.getTransactionId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_TRANSACTIONID);
        }
        if(StringUtils.isEmpty(request.getReal())) {
            missingParameters.add(PlayNGoConstants.REQUEST_REAL);
        }
        if(StringUtils.isEmpty(request.getCurrency())) {
            missingParameters.add(PlayNGoConstants.REQUEST_CURRENCY);
        }
        if(StringUtils.isEmpty(request.getGameId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_GAMEID);
        }
        if(StringUtils.isEmpty(request.getGameSessionId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_GAMESESSIONID);
        }
        if(StringUtils.isEmpty(request.getAccessToken())) {
            missingParameters.add(PlayNGoConstants.REQUEST_ACCESSTOKEN);
        }
        if(request.getRoundId() == null) {
            missingParameters.add(PlayNGoConstants.REQUEST_ROUNDID);
        }

        if(missingParameters.size() > 0) {
            throw new PlayNGoErrorException(PlayNGoStatusCode.RETURN_INTERNAL.getCode(), PlayNGoStatusCode.RETURN_INTERNAL.getMsg() + "Missing paramemters: " + missingParameters);
        }

        isValid = true;
        return isValid;
    }

    private boolean validateReleaseRequestParamemters(ReleaseRequest request) throws PlayNGoErrorException {
        boolean isValid = false;
        List missingParameters = new ArrayList();

        if(StringUtils.isEmpty(request.getExternalId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_EXTERNALID);
        }
        if(StringUtils.isEmpty(request.getProductId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_PRODUCTID);
        }
        if(StringUtils.isEmpty(request.getTransactionId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_TRANSACTIONID);
        }
        if(StringUtils.isEmpty(request.getReal())) {
            missingParameters.add(PlayNGoConstants.REQUEST_REAL);
        }
        if(StringUtils.isEmpty(request.getCurrency())) {
            missingParameters.add(PlayNGoConstants.REQUEST_CURRENCY);
        }
        if(StringUtils.isEmpty(request.getGameSessionId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_GAMESESSIONID);
        }
        if(request.getState() == null) {
            missingParameters.add(PlayNGoConstants.REQUEST_STATE);
        }
        if(request.getType() == null) {
            missingParameters.add(PlayNGoConstants.REQUEST_TYPE);
        }
        if(StringUtils.isEmpty(request.getGameId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_GAMEID);
        }
        if(StringUtils.isEmpty(request.getAccessToken())) {
            missingParameters.add(PlayNGoConstants.REQUEST_ACCESSTOKEN);
        }
        if(request.getRoundId() == null) {
            missingParameters.add(PlayNGoConstants.REQUEST_ROUNDID);
        }
        if(StringUtils.isEmpty(request.getJackpotGain())) {
            missingParameters.add(PlayNGoConstants.REQUEST_JACKPOTGAIN);
        }
        if(StringUtils.isEmpty(request.getJackpotLoss())) {
            missingParameters.add(PlayNGoConstants.REQUEST_JACKPOTLOSS);
        }
        if(StringUtils.isEmpty(request.getJackpotGainSeed())) {  // for testing
            missingParameters.add(PlayNGoConstants.REQUEST_JACKPOTGAINSEED);
        }
//        if(request.getJackpotGainID() == null) { // the Play'nGo test stage do not has this
//            missingParameters.add(PlayNGoConstants.REQUEST_JACKPOTGAINID);
//        }

        if(missingParameters.size() > 0) {
            throw new PlayNGoErrorException(PlayNGoStatusCode.RETURN_INTERNAL.getCode(), PlayNGoStatusCode.RETURN_INTERNAL.getMsg() + "Missing paramemters: " + missingParameters);
        }

        isValid = true;
        return isValid;
    }

    private boolean validateBalanceRequestParamemters(BalanceRequest request) throws PlayNGoErrorException {
        boolean isValid = false;
        List missingParameters = new ArrayList();

        if(StringUtils.isEmpty(request.getExternalId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_EXTERNALID);
        }
        if(StringUtils.isEmpty(request.getProductId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_PRODUCTID);
        }
        if(StringUtils.isEmpty(request.getCurrency())) {
            missingParameters.add(PlayNGoConstants.REQUEST_CURRENCY);
        }
        if(StringUtils.isEmpty(request.getGameId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_GAMEID);
        }
        if(StringUtils.isEmpty(request.getAccessToken())) {
            missingParameters.add(PlayNGoConstants.REQUEST_ACCESSTOKEN);
        }

        if(missingParameters.size() > 0) {
            throw new PlayNGoErrorException(PlayNGoStatusCode.RETURN_INTERNAL.getCode(), PlayNGoStatusCode.RETURN_INTERNAL.getMsg() + "Missing paramemters: " + missingParameters);
        }

        isValid = true;
        return isValid;
    }

    private boolean validateCancelReserveRequestParamemters(CancelReserveRequest request) throws PlayNGoErrorException {
        boolean isValid = false;
        List missingParameters = new ArrayList();

        if(StringUtils.isEmpty(request.getExternalId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_EXTERNALID);
        }
        if(StringUtils.isEmpty(request.getProductId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_PRODUCTID);
        }
        if(StringUtils.isEmpty(request.getTransactionId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_TRANSACTIONID);
        }
        if(StringUtils.isEmpty(request.getReal())) {
            missingParameters.add(PlayNGoConstants.REQUEST_REAL);
        }
        if(StringUtils.isEmpty(request.getCurrency())) {
            missingParameters.add(PlayNGoConstants.REQUEST_CURRENCY);
        }
//        if(StringUtils.isEmpty(request.getGameSessionId())) {
//            missingParameters.add(PlayNGoConstants.REQUEST_GAMESESSIONID);
//        }
        if(StringUtils.isEmpty(request.getAccessToken())) {
            missingParameters.add(PlayNGoConstants.REQUEST_ACCESSTOKEN);
        }
        if(request.getRoundId() == null) {
            missingParameters.add(PlayNGoConstants.REQUEST_ROUNDID);
        }
        if(StringUtils.isEmpty(request.getGameId())) {
            missingParameters.add(PlayNGoConstants.REQUEST_GAMEID);
        }

        if(missingParameters.size() > 0) {
            throw new PlayNGoErrorException(PlayNGoStatusCode.RETURN_INTERNAL.getCode(), PlayNGoStatusCode.RETURN_INTERNAL.getMsg() + "Missing paramemters: " + missingParameters);
        }

        isValid = true;
        return isValid;
    }

    // TODO: need specific ???????
    private String setResponseToXml(Object playNGoResponse) throws IOException {
        String reply= JAXBUtil.toFormatString(playNGoResponse);
        return reply;
    }

    private void updateSession(Integer partyId) {
        transactionService.updateSession(partyId);
    }

    public TransactionService getTransactionService() {
        return transactionService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public RegistryService getRegistryService() {
        return registryService;
    }

    public void setRegistryService(RegistryService registryService) {
        this.registryService = registryService;
    }
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public GameInstanceService getGameInstanceService() {
        return gameInstanceService;
    }

    public void setGameInstanceService(GameInstanceService gameInstanceService) {
        this.gameInstanceService = gameInstanceService;
    }

    public PlatformService getPlatformService() {
        return platformService;
    }

    public void setPlatformService(PlatformService platformService) {
        this.platformService = platformService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setTransactionTagService(TransactionTagService transactionTagService) {
        this.transactionTagService = transactionTagService;
    }
}
