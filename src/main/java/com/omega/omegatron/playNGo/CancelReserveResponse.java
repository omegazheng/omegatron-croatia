package com.omega.omegatron.playNGo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by songleo on 2016-04-07.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="cancelReserve")
public class CancelReserveResponse {
    public CancelReserveResponse() {}

    @XmlElement(name="externalTransactionId")
    private String externalTransactionId;

    @XmlElement(name="statusCode")
    private String statusCode;

    @XmlElement(name="transactionId")
    private String transactionId;

    public String getExternalTransactionId() {
        return externalTransactionId;
    }

    public void setExternalTransactionId(String externalTransactionId) {
        this.externalTransactionId = externalTransactionId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
