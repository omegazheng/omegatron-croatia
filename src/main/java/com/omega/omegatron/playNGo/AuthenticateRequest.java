package com.omega.omegatron.playNGo;

import javax.xml.bind.annotation.*;

/**
 * Created by songleo on 2016-04-06.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="authenticate")
public class AuthenticateRequest {
    public AuthenticateRequest() {}

    @XmlElement(name="username")
    private String username;

    @XmlElement(name="productId")
    private String productId;

    @XmlElement(name="client")
    private String client;

    @XmlElement(name="CID")
    private String CID;

    @XmlElement(name="clientIP")
    private String clientIP;

    @XmlElement(name="contextId")
    private Integer contextId;

    @XmlElement(name="accessToken")
    private String accessToken;

    @XmlElement(name="language")
    private String language;

    @XmlElement(name="gameId")
    private String gameId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getClientIP() {
        return clientIP;
    }

    public void setClientIP(String clientIP) {
        this.clientIP = clientIP;
    }

    public Integer getContextId() {
        return contextId;
    }

    public void setContextId(Integer contextId) {
        this.contextId = contextId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}
