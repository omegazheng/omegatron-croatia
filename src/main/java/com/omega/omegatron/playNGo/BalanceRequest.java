package com.omega.omegatron.playNGo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by songleo on 2016-04-06.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="balance")
public class BalanceRequest {
    public BalanceRequest() {}

    @XmlElement(name="externalId")
    private String externalId;

    @XmlElement(name="productId")
    private String productId;

    @XmlElement(name="currency")
    private String currency;

    @XmlElement(name="gameId")
    private String gameId;

    @XmlElement(name="accessToken")
    private String accessToken;

    @XmlElement(name="externalGameSessionId")
    private String externalGameSessionId;

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getExternalGameSessionId() {
        return externalGameSessionId;
    }

    public void setExternalGameSessionId(String externalGameSessionId) {
        this.externalGameSessionId = externalGameSessionId;
    }
}
