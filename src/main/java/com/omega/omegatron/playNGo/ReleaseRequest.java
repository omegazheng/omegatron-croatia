package com.omega.omegatron.playNGo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by songleo on 2016-04-06.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="release")
public class ReleaseRequest {
    public ReleaseRequest() {}

    @XmlElement(name="externalId")
    private String externalId;

    @XmlElement(name="productId")
    private String productId;

    @XmlElement(name="transactionId")
    private String transactionId;

    @XmlElement(name="real")
    private String real;

    @XmlElement(name="currency")
    private String currency;

    @XmlElement(name="gameSessionId")
    private String gameSessionId;

    @XmlElement(name="contextId")
    private Integer contextId;

    @XmlElement(name="state")
    private Integer state;

    @XmlElement(name="totalLoss")
    private String totalLoss;

    @XmlElement(name="totalGain")
    private String totalGain;

    @XmlElement(name="numRounds")
    private Integer numRounds;

    @XmlElement(name="type")
    private Integer type;

    @XmlElement(name="gameId")
    private String gameId;

    @XmlElement(name="accessToken")
    private String accessToken;

    @XmlElement(name="roundId")
    private Long roundId;

    @XmlElement(name="jackpotGain")
    private String jackpotGain;

    @XmlElement(name="jackpotLoss")
    private String jackpotLoss;

    @XmlElement(name="jackpotGainSeed")
    private String jackpotGainSeed;

    @XmlElement(name="jackpotGainID")
    private Integer jackpotGainID;

    @XmlElement(name="freegameExternalId")
    private String freegameExternalId;

    @XmlElement(name="turnover")
    private Integer turnover;

    @XmlElement(name="freegameFinished")
    private Boolean freegameFinished;

    @XmlElement(name="freegameGain")
    private String freegameGain;

    @XmlElement(name="freegameLoss")
    private String freegameLoss;

    @XmlElement(name="externalGameSessionId")
    private String externalGameSessionId;

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getReal() {
        return real;
    }

    public void setReal(String real) {
        this.real = real;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getGameSessionId() {
        return gameSessionId;
    }

    public void setGameSessionId(String gameSessionId) {
        this.gameSessionId = gameSessionId;
    }

    public Integer getContextId() {
        return contextId;
    }

    public void setContextId(Integer contextId) {
        this.contextId = contextId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getTotalLoss() {
        return totalLoss;
    }

    public void setTotalLoss(String totalLoss) {
        this.totalLoss = totalLoss;
    }

    public String getTotalGain() {
        return totalGain;
    }

    public void setTotalGain(String totalGain) {
        this.totalGain = totalGain;
    }

    public Integer getNumRounds() {
        return numRounds;
    }

    public void setNumRounds(Integer numRounds) {
        this.numRounds = numRounds;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Long getRoundId() {
        return roundId;
    }

    public void setRoundId(Long roundId) {
        this.roundId = roundId;
    }

    public String getJackpotGain() {
        return jackpotGain;
    }

    public void setJackpotGain(String jackpotGain) {
        this.jackpotGain = jackpotGain;
    }

    public String getJackpotLoss() {
        return jackpotLoss;
    }

    public void setJackpotLoss(String jackpotLoss) {
        this.jackpotLoss = jackpotLoss;
    }

    public String getJackpotGainSeed() {
        return jackpotGainSeed;
    }

    public void setJackpotGainSeed(String jackpotGainSeed) {
        this.jackpotGainSeed = jackpotGainSeed;
    }

    public Integer getJackpotGainID() {
        return jackpotGainID;
    }

    public void setJackpotGainID(Integer jackpotGainID) {
        this.jackpotGainID = jackpotGainID;
    }

    public String getFreegameExternalId() {
        return freegameExternalId;
    }

    public void setFreegameExternalId(String freegameExternalId) {
        this.freegameExternalId = freegameExternalId;
    }

    public Integer getTurnover() {
        return turnover;
    }

    public void setTurnover(Integer turnover) {
        this.turnover = turnover;
    }

    public Boolean getFreegameFinished() {
        return freegameFinished;
    }

    public void setFreegameFinished(Boolean freegameFinished) {
        this.freegameFinished = freegameFinished;
    }

    public String getFreegameGain() {
        return freegameGain;
    }

    public void setFreegameGain(String freegameGain) {
        this.freegameGain = freegameGain;
    }

    public String getFreegameLoss() {
        return freegameLoss;
    }

    public void setFreegameLoss(String freegameLoss) {
        this.freegameLoss = freegameLoss;
    }

    public String getExternalGameSessionId() {
        return externalGameSessionId;
    }

    public void setExternalGameSessionId(String externalGameSessionId) {
        this.externalGameSessionId = externalGameSessionId;
    }
}
