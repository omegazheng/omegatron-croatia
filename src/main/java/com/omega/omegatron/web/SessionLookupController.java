package com.omega.omegatron.web;

import com.omega.commonsUtil.SessionKeyUtil;
import com.omega.omegatron.constants.ConfigKeys;
import com.omega.omegatron.service.RegistryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * A client will pass in a session key and this will look up the party id based on the session. It will
 * return -1 if not found.
 */
@Controller
public class SessionLookupController {



    private RegistryService registryService;

    private JdbcTemplate jdbcTemplate;

    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());


    @RequestMapping(value = "/SessionLookup")
    @ResponseBody
    public String sessionLookup(String sessionKey)  {

        log.debug("Looking up user for sesionKey=" + sessionKey);

        if (!SessionKeyUtil.isSessionKeyHasValidFormat(sessionKey)) {
            log.error("SessionKey " + sessionKey + " is invalid.");
            return "-1";
        }

        // for null session key return -1
        if (sessionKey == null) {
            log.error("session key is null returning -1");
            return "-1";
        }

        // get the session expiry time, to make sure we don't look up an expired session
        String strVal = registryService.getValue(ConfigKeys.PS_WEB_SESSION_TIMEOUT);
        int timeout = 30;
        if (strVal != null) {
            timeout = Integer.parseInt(strVal);
        }
        DateTime minAccessTime = new DateTime().minusMinutes(timeout);
        log.debug("minAccesstime=" + minAccessTime);

        // look up the partyId and return if found
        String query = "select partyid from user_web_session where session_key = ? and last_access_time > ?";
        try {
            int partyId = jdbcTemplate.queryForObject(query, Integer.class, sessionKey, minAccessTime.toDate());
            log.debug("found partyid =" + partyId);
            return Integer.toString(partyId);
        } catch (EmptyResultDataAccessException erdaExcp) {
            log.error("user not found for sessionKey=" + sessionKey);
            return "-1";
        }

   }

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public RegistryService getRegistryService() {
        return registryService;
    }
    @Autowired
    public void setRegistryService(RegistryService registryService) {
        this.registryService = registryService;
    }
}
