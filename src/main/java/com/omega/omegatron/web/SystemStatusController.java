package com.omega.omegatron.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.omega.commons.entity.GameLoyaltyRatio;
import com.omega.commons.entity.Platform;
import com.omega.commons.entity.User;
import com.omega.commons.service.*;
import com.omega.omegatron.chartwell.ChartwellWalletAdapter;
import com.omega.omegatron.core.TransactionService;
import com.omega.omegatron.service.*;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.text.DecimalFormat;

/**
 * Gives an overview of the current system status. Displays information
 * such as current memory usage.
 */
@Controller
public class SystemStatusController {

    ObjectMapper mapper = new ObjectMapper();

    private TransactionService transactionService;

    private ChartwellWalletAdapter chartwellWalletAdapter;

    private CacheService cacheService;

    private ComboPooledDataSource dataSource;

    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

    private StatsdLoggingService statsdLoggingService;

    private RegistryService registryService;
    private UserService userService;
    private UserWebSessionService userWebSessionService;
    private UserWebSessionManager userWebSessionManager;
    private GameLoyaltyRatioService gameLoyaltyRatioService;
    private GameExcludeService gameExcludeService;
    private BrandService brandService;


    @RequestMapping("/SystemStatus")
    public ModelAndView systemStatus(HttpServletRequest request,  HttpServletResponse response) throws IOException, SQLException {
        String ttl = request.getParameter("ttl");
        Integer brandId = 0;
        try {
            if ("All".equalsIgnoreCase(ttl)) {
                brandId = null;
            } else {
                brandId = new Integer(ttl);
            }
        } catch (Exception e) {
            brandId = 0;
        }
        if (!new Integer(0).equals(brandId)) {
            brandService.expireLicense(brandId);
        }

        ModelAndView mav = new ModelAndView();
        mav.setViewName("systemStatus.jsp");

        log.info("Executing system status controller.");


        statsdLoggingService.log(StatsdLoggingConstant.SYSTEM_OMEGA_TRON, 123, 100);
        log.info("Just testing statsdLoggingService");

        // ModelMap model = new ModelMap();
        // model.put("test", "testtis");

        log.info("Getting runtime.");
        Runtime rt = Runtime.getRuntime();
        log.info("Runtime received.");

        long oneMeg = 1024 * 1024;

        DecimalFormat df = new DecimalFormat();
        df.applyPattern("###,###,###,##0");

        long usedMem = rt.totalMemory() - rt.freeMemory();
        long maxHeapRemaining = rt.maxMemory() - usedMem;

        mav.addObject("usedMemory", df.format(usedMem));
        mav.addObject("maxHeapRemaining", df.format(maxHeapRemaining));
        mav.addObject("heapSize", df.format(rt.totalMemory()));
        mav.addObject("maxHeapSize", df.format(rt.maxMemory()));
        mav.addObject("freeMemory", df.format(rt.freeMemory()));


        mav.addObject("usedMemoryMegs", df.format(usedMem/oneMeg));
        mav.addObject("maxHeapRemainingMegs", df.format(maxHeapRemaining/oneMeg));
        mav.addObject("heapSizeMegs", df.format(rt.totalMemory()/oneMeg));
        mav.addObject("maxHeapSizeMegs", df.format(rt.maxMemory()/oneMeg));
        mav.addObject("freeMemoryMegs", df.format(rt.freeMemory()/oneMeg));

        // uptime
        log.info("Getting runtime MX Bean.");
        RuntimeMXBean mx = ManagementFactory.getRuntimeMXBean();
        log.info("Runtime MX Bean retrieved.");
        Period period = new Period(mx.getUptime());
        PeriodFormatter daysHoursMinutes = new PeriodFormatterBuilder()
            .appendDays()
            .appendSuffix(" day ", " days ")
            .appendHours()
            .appendSuffix(" hour ", " hours ")
            .appendMinutes()
            .appendSuffix(" min ", " mins ")
            .appendSeconds()
            .appendSuffix(" sec", " secs")
            .toFormatter();
        mav.addObject("uptime", daysHoursMinutes.print(period.normalizedStandard()));
        mav.addObject("uptimeRaw", mx.getUptime());

        DateTime startDate = new DateTime(mx.getStartTime());
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MMM dd, yyyy HH:mm:ss z");
        mav.addObject("startTime", fmt.print(startDate));
        mav.addObject("serverTime", fmt.print(new DateTime()));

        mav.addObject("dbHost", getDBHostName(dataSource.getJdbcUrl()));
        mav.addObject("dbName", getDbName(dataSource.getJdbcUrl()));
        mav.addObject("hostName", getHostName());

        mav.addObject("softwareVersion", getSoftwareVersion());

        String clearCache = request.getParameter("clearCache");
        if(StringUtils.isNotBlank(clearCache)) {
            cacheService.reload();
            mav.addObject("cacheReloaded", fmt.print(new DateTime()));
        }

        // PrintWriter writer = response.getWriter();
        // writer.write("HEllo world");
        // return null;
        return mav;
    }

    private PlatformService platformService;
    /**
     * This will test various cache function.
     *  type = registry, platform (case insensitive)
     * registry
     * http://localhost:8080/omegatron/spr/CacheStatus?type=registry&brandId=1&key=caching.expire
     * http://localhost:8080/omegatron/spr/CacheStatus?type=registry&key=caching.expire
     *
     * platform
     * http://localhost:8080/omegatron/spr/CacheStatus?type=platform&code=OMEGA_CAS
     * http://localhost:8080/omegatron/spr/CacheStatus?type=platform&id=1
     *
     * user
     * http://localhost:8080/omegatron/spr/CacheStatus?type=user&code=OMEGA_CAS
     * http://localhost:8080/omegatron/spr/CacheStatus?type=user&id=1
     *
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws SQLException
     */
    @RequestMapping("/CacheStatus")
    public ModelAndView cacheTest(HttpServletRequest request,  HttpServletResponse response) throws IOException, SQLException {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("cacheStatus.jsp");
        String type = request.getParameter("type");
        try {
            if (StringUtils.isEmpty(type)|| "registry".equalsIgnoreCase(type)) {
                Integer brandId = Integer.parseInt(request.getParameter("brandId"));

                if (brandId != null) {
                    mav.addObject("method", "registryService.getValue() with brandId = "+ brandId);
                    String key = request.getParameter("key");
                    String value = registryService.getValue(brandId, key, "Not Found");
                    mav.addObject("key", key);
                    mav.addObject("value", value);
                } else {
                    mav.addObject("method", "registryService.getValue()");
                    String key = request.getParameter("key");
                    String value = registryService.getValue(key, "Not Found");
                    mav.addObject("key", key);
                    mav.addObject("value", value);
                }
            } else if ("platform".equalsIgnoreCase(type)) {
                Platform platform = new Platform();
                String code = request.getParameter("code");
                if (!StringUtils.isEmpty(code)) {
                    mav.addObject("method", "platformService.getPlatformByCode()");
                    platform = platformService.getPlatformByCode(code);
                } else {
                    try {
                        Integer id = Integer.parseInt(request.getParameter("id"));
                        platform = platformService.getPlatformById(id);
                        mav.addObject("method", "platformService.getPlatformById()");
                    } catch (NumberFormatException e) {
                        platform = platformService.getRandom();
                        mav.addObject("method", "platformService.getRandom()");
                    }
                }
                mav.addObject("key", "platform ");
                mav.addObject("value", mapper.writeValueAsString(platform));

            } else if ("user".equalsIgnoreCase(type)) {

                User user = new User();
                String partyid = request.getParameter("partyid");
                if (!StringUtils.isEmpty(partyid)) {
                    mav.addObject("method", "userService.getByPartyId()");
                    user = userService.getByPartyId(Integer.parseInt(partyid));
                } else {
                    Integer accountId = Integer.parseInt(request.getParameter("accountid"));
                    user = userService.getByAccountId(accountId);
                    mav.addObject("method", "userService.getByAccountId()");
                }
                mav.addObject("key", "user ");
                mav.addObject("value", mapper.writeValueAsString(user));

            } else if ("uws".equalsIgnoreCase(type)) {

                UserWebSession userWebSession = new UserWebSession();
                String partyid = request.getParameter("partyid");
                if (!StringUtils.isEmpty(partyid)) {
                    String create = request.getParameter("create");
                    if (create!=null) {
                        mav.addObject("method", "userWebSessionService.createNewSession("+partyid+")");
                        userWebSession = userWebSessionService.createNewSession(Integer.parseInt(partyid));
                    } else {
                        userWebSessionManager.updateUserWebSession(partyid);
                        userWebSession = userWebSessionService.getUserWebSession(
                                        userWebSessionService.getSessionKey(Integer.parseInt(partyid)));
                        mav.addObject("method", "userWebSessionManager.updateUserWebSession("+partyid+")");
                    }
                } else {
                    String sessionKey = request.getParameter("sessionkey");
                    userWebSessionManager.updateUserWebSession(sessionKey);
                    userWebSession = userWebSessionService.getUserWebSession(sessionKey);
                    mav.addObject("method", "userWebSessionManager.updateUserWebSession("+sessionKey+")");
                }
                mav.addObject("key", "UserWebSession ");
                mav.addObject("value", mapper.writeValueAsString(userWebSession));

            } else if ("glr".equalsIgnoreCase(type)) {

                Integer partyId = Integer.parseInt(request.getParameter("partyid"));
                Integer platformId = Integer.parseInt(request.getParameter("platformid"));
                String gameId = request.getParameter("gameid");

                mav.addObject("method", "gameLoyaltyRatioService.getByPartyIdPlatformIdAndGameId("+partyId+","+platformId+","+gameId+")");
                GameLoyaltyRatio gameLoyaltyRatio = gameLoyaltyRatioService.getByPartyIdPlatformIdAndGameId(partyId, platformId, gameId);

                mav.addObject("key", "GameLoyaltyRatioService ");
                mav.addObject("value", mapper.writeValueAsString(gameLoyaltyRatio));
            } else if ("ges".equalsIgnoreCase(type)) {

                Integer partyId = Integer.parseInt(request.getParameter("partyid"));
                String gameId = request.getParameter("gameid");
                String platformCode = request.getParameter("platformcode");

                mav.addObject("method", "gameExcludeService.isGameAllowForPlayer("+partyId+","+gameId+","+platformCode+")");
                Boolean result = gameExcludeService.isGameAllowForPlayer(partyId, gameId, platformCode);

                mav.addObject("key", "gameExcludeService ");
                mav.addObject("value", mapper.writeValueAsString(result));
            }

        } catch (Exception ee) {
            mav.addObject("key", "Help");
            mav.addObject("value",  "Refer to usage below");

        }

        return mav;
    }



    private String getHostName(){
        String hostName;
        try {
            hostName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException uhe){
            hostName = "Unknown Host Exception";
        }
        return hostName;
    }

    public String getDBHostName(String url) {
        int startIndex = url.lastIndexOf("//") + 2;
        int endIndex = url.indexOf(":", startIndex);
        if (endIndex == -1) {
            endIndex = url.indexOf(("/"), startIndex);
        }
        String dbHost = url.substring(startIndex, endIndex);
        return dbHost;
    }

    public String getDbName(String url) {
        int startIndex = url.lastIndexOf("/") + 1;
        return url.substring(startIndex, url.length());
    }

    private String getSoftwareVersion() {

        String version = "Unknown";
        InputStream in = this.getClass().getResourceAsStream("/version.txt");
        if (in != null) {
            java.util.Scanner s = new java.util.Scanner(in).useDelimiter("\\A");
            if (s.hasNext()) {
                version = s.next();
            }
            try {
              in.close();
            } catch (IOException excp) {
                //
            }
        }

        return version;
    }

    public ComboPooledDataSource getDataSource() {
        return dataSource;
    }

    @Autowired
    public void setDataSource(ComboPooledDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Autowired
    public void setStatsdLoggingService(StatsdLoggingService statsdLoggingService) {
        this.statsdLoggingService = statsdLoggingService;
    }
    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Autowired
    public void setRegistryService(RegistryService registryService) {
        this.registryService = registryService;
    }

    @Autowired
    public void setPlatformService(PlatformService platformService) {
        this.platformService = platformService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setUserWebSessionManager(UserWebSessionManager userWebSessionManager) {
        this.userWebSessionManager = userWebSessionManager;
    }

    @Autowired
    public void setUserWebSessionService(UserWebSessionService userWebSessionService) {
        this.userWebSessionService = userWebSessionService;
    }

    @Autowired
    public void setGameLoyaltyRatioService(GameLoyaltyRatioService gameLoyaltyRatioService) {
        this.gameLoyaltyRatioService = gameLoyaltyRatioService;
    }

    @Autowired
    public void setGameExcludeService(GameExcludeService gameExcludeService) {
        this.gameExcludeService = gameExcludeService;
    }

    @Autowired
    public void setBrandService(BrandService brandService) {
        this.brandService = brandService;
    }
}
