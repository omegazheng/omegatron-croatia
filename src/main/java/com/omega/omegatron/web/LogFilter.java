package com.omega.omegatron.web;

import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Simple filter to log requests before and after execution.
 */
public class LogFilter implements Filter {
    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());


    public void destroy() {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        try {

            HttpServletRequest httpReq = (HttpServletRequest) request;

            String ipAddress = httpReq.getRemoteAddr();

            // get the paramsString
            String paramsString = getParamString(httpReq);
            String msg = "ipAddress=" + ipAddress + ", uri=" + request.getServerName() + httpReq.getRequestURI() +
                    ", params=" + paramsString;

            // String msg = "ipAddress=" + ipAddress + ", uri=" + request.getServerName() + httpReq.getRequestURI();


            log.info("Start request :: " + msg);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            chain.doFilter(request, response);
            stopWatch.stop();

            log.info("End request (" + stopWatch.getTime() + "ms) :: "+httpReq.getRequestURI()+":"+stopWatch.getTime() +" :: " + msg);
            //log.info("End request (" + stopWatch.getTime() + "ms) :: " + msg);
        } catch (RuntimeException exception) {
            // log exception and rethrow it
            log.error("Runtime exception occurred and caught. Log and rethrow", exception);
            throw exception;
        }


    }


    public void init(FilterConfig filterConfig) throws ServletException {

    }


    /**
     * Gets the set of parameters from the request and returns
     * as a comma separated string. If maskParams is set then it will try
     * to determine whether it might hold sensitive card data and if so
     * it will add a mask.
     * <p/>
     *
     * @param request
     * @return
     */
    public static String getParamString(HttpServletRequest request) {

        if (request.getParameterMap().size() == 0) {
            return "empty";
        }
        StringBuilder sb = new StringBuilder("[");
        ArrayList<String> list = new ArrayList<String>();
        Enumeration enumeration = request.getParameterNames();
        while (enumeration.hasMoreElements()) {
            String paramName = (String) enumeration.nextElement();
            String values[] = request.getParameterValues(paramName);
            for (String paramVal : values) {
                sb.append(paramName);
                sb.append("=");
//                if (paramName.toLowerCase().contains("password")) {
//                    paramVal = "xxxxxx";
//                }
                sb.append(paramVal);
                sb.append(",");
            }
        }
        sb.delete(sb.length() - 1, sb.length());
        sb.append("]");

        return sb.toString();
    }



}

