package com.omega.omegatron.omegawallet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;

/**
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TriggerBonusExRequest", namespace = "http://omegawallet.omegatron.omega.com/",
    propOrder = {
        "platformCode",
        "platformPassword",
        "platformTransactionId",
        "partyId",
        "amount",
        "currency",
        "transactionType"
    })
public class TriggerBonusExRequest {

    @XmlElement(required = true)
    private String platformCode;

    @XmlElement(required = true)
    private String platformPassword;

    @XmlElement(required = true)
    private String platformTransactionId;

    @XmlElement(required = true)
    private Integer partyId;

    @XmlElement(required = true)
    private BigDecimal amount;

    @XmlElement(required = true)
    private String currency;

    @XmlElement(required = true)
    private String transactionType;

    public String getPlatformCode() {
        return platformCode;
    }

    public void setPlatformCode(String platformCode) {
        this.platformCode = platformCode;
    }

    public String getPlatformPassword() {
        return platformPassword;
    }

    public void setPlatformPassword(String platformPassword) {
        this.platformPassword = platformPassword;
    }

    public String getPlatformTransactionId() {
        return platformTransactionId;
    }

    public void setPlatformTransactionId(String platformTransactionId) {
        this.platformTransactionId = platformTransactionId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getPartyId() {
        return partyId;
    }

    public void setPartyId(Integer partyId) {
        this.partyId = partyId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
}
