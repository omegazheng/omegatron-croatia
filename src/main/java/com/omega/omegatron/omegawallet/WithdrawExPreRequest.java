package com.omega.omegatron.omegawallet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;

/**
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WithdrawExPreRequest", namespace = "http://omegawallet.omegatron.omega.com/",
    propOrder = {
        "platformCode",
        "platformPassword",
        "partyId",
        "currency",
        "amount",
        "platformTransactionId",
        "gameRoundId",
        "gameId",
        "isGamePlayFinal"
    })
public class WithdrawExPreRequest {

    @XmlElement(required = true)
    private String platformCode;

    @XmlElement(required = true)
    private String platformPassword;

    @XmlElement(required = true)
    private Integer partyId;

    @XmlElement(required = true)
    protected String currency;

    @XmlElement(required = true)
    protected BigDecimal amount;

    @XmlElement(required = true)
    protected Long platformTransactionId;

    protected String gameRoundId;

    protected String gameId;

    protected Boolean isGamePlayFinal;

    public String getPlatformCode() {
        return platformCode;
    }

    public void setPlatformCode(String platformCode) {
        this.platformCode = platformCode;
    }

    public String getPlatformPassword() {
        return platformPassword;
    }

    public void setPlatformPassword(String platformPassword) {
        this.platformPassword = platformPassword;
    }

    public Integer getPartyId() {
        return partyId;
    }

    public void setPartyId(Integer partyId) {
        this.partyId = partyId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getPlatformTransactionId() {
        return platformTransactionId;
    }

    public void setPlatformTransactionId(Long platformTransactionId) {
        this.platformTransactionId = platformTransactionId;
    }

    public String getGameRoundId() {
        return gameRoundId;
    }

    public void setGameRoundId(String gameRoundId) {
        this.gameRoundId = gameRoundId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Boolean getIsGamePlayFinal() {
        return isGamePlayFinal;
    }

    public void setIsGamePlayFinal(Boolean isGamePlayFinal) {
        this.isGamePlayFinal = isGamePlayFinal;
    }
}


