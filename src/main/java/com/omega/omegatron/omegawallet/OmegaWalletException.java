package com.omega.omegatron.omegawallet;

import javax.jws.soap.SOAPBinding;
import javax.xml.ws.WebFault;
import java.math.BigDecimal;

/**
 */
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@WebFault
public class OmegaWalletException extends Exception  {
    private Integer code;
    private String message;
    private BigDecimal balance;

    public OmegaWalletException() {

    }

    public OmegaWalletException(Integer code, String message) {
        this(code, message, null);
    }

    public OmegaWalletException(Integer code, String message, BigDecimal balance) {
        this.code = code;
        this.message = message;
        this.balance = balance;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
