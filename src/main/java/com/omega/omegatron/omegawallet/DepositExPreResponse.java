package com.omega.omegatron.omegawallet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DepositExPreResponse", propOrder = {
    "balance",
    "cashBalance",
    "bonusBalance",
    "cashAmount",
    "bonusAmount",
    "transactionId",
    "sendWinNotification"
})
public class DepositExPreResponse {

    protected BigDecimal balance;
    protected BigDecimal cashBalance;
    protected BigDecimal bonusBalance;
    protected BigDecimal cashAmount;
    protected BigDecimal bonusAmount;
    protected long transactionId;
    protected boolean sendWinNotification;

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getCashBalance() {
        return cashBalance;
    }

    public void setCashBalance(BigDecimal cashBalance) {
        this.cashBalance = cashBalance;
    }

    public BigDecimal getBonusBalance() {
        return bonusBalance;
    }

    public void setBonusBalance(BigDecimal bonusBalance) {
        this.bonusBalance = bonusBalance;
    }

    public BigDecimal getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(BigDecimal cashAmount) {
        this.cashAmount = cashAmount;
    }

    public BigDecimal getBonusAmount() {
        return bonusAmount;
    }

    public void setBonusAmount(BigDecimal bonusAmount) {
        this.bonusAmount = bonusAmount;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public boolean getSendWinNotification() {
        return sendWinNotification;
    }

    public void setSendWinNotification(boolean sendWinNotification) {
        this.sendWinNotification = sendWinNotification;
    }
}
