package com.omega.omegatron.omegawallet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DepositResponse", propOrder = {
    "balance",
    "transactionId",
    "sendWinNotification"
})
public class DepositResponse {

    protected BigDecimal balance;
    protected long transactionId;
    protected boolean sendWinNotification;

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public boolean getSendWinNotification() {
        return sendWinNotification;
    }

    public void setSendWinNotification(boolean sendWinNotification) {
        this.sendWinNotification = sendWinNotification;
    }
}
