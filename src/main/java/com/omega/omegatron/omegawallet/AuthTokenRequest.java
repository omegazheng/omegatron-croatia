package com.omega.omegatron.omegawallet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "authTokenRequest", namespace = "http://omegawallet.omegatron.omega.com/",
    propOrder = {
        "platformCode",
        "platformPassword",
        "sessionKey"
    })
public class AuthTokenRequest {


    @XmlElement(required = true)
    private String platformCode;

    @XmlElement(required = true)
    private String platformPassword;

    @XmlElement(required = true)
    private String sessionKey;

    public String getPlatformCode() {
        return platformCode;
    }

    public void setPlatformCode(String platformCode) {
        this.platformCode = platformCode;
    }

    public String getPlatformPassword() {
        return platformPassword;
    }

    public void setPlatformPassword(String platformPassword) {
        this.platformPassword = platformPassword;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }
}
