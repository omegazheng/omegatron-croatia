package com.omega.omegatron.omegawallet;

/**
 */
public class OmegaWalletConstants {

    public static final int ERR_PLAYER_NOT_FOUND = 9001;
    public static final int ERR_INSUFF_FUNDS = 9002;
    public static final int ERR_INVALID_CURRENCY = 9003;
    public static final int ERR_INVALID_AMOUNT = 9004;
    public static final int ERR_NEGATIVE_AMOUNT = 9005;
    public static final int ERR_AUTH_FAILED = 9006;
    public static final int ERR_TRAN_NOT_FOUND = 9007;

    public static final int ERR_BONUS_PLAN_NOT_FOUND = 9008;

    public static final int ERR_WAGER_LIMIT_EXCEEDED  = 9009;
    public static final int ERR_LOSS_LIMIT_EXCEEDED   = 9010;
    public static final int ERR_SESSION_LIMIT_EXCEEDED= 9011;
    public static final int ERR_INVALID_GAME_ROUND_ID = 9012;
    public static final int ERR_INVALID_TXN_ON_HOLD_STATUS = 9013;
    public static final int ERR_TXN_ON_HOLD_NOT_FOUND = 9014;


    public static final int ERR_EGAME_INSUFFICIENT = 9100;
    public static final int ERR_EGAME_NOT_CHECKED_OUT = 9101;

    public static final int ERR_INVALID_BONUS_TRAN_TYPE = 9200;

    public static final int ERR_UNEXPECTED = 9999;


}
