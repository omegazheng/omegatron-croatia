package com.omega.omegatron.omegawallet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RollbackRequestV2", namespace = "http://omegawallet.omegatron.omega.com/",
        propOrder = {
                "platformCode",
                "platformPassword",
                "platformTransactionId"
        })
public class RollbackRequestV2 {

    @XmlElement(required = true)
    private String platformCode;

    @XmlElement(required = true)
    private String platformPassword;

    @XmlElement(required = true)
    private String platformTransactionId;

    public String getPlatformCode() {
        return platformCode;
    }

    public void setPlatformCode(String platformCode) {
        this.platformCode = platformCode;
    }

    public String getPlatformPassword() {
        return platformPassword;
    }

    public void setPlatformPassword(String platformPassword) {
        this.platformPassword = platformPassword;
    }

    public String getPlatformTransactionId() {
        return platformTransactionId;
    }

    public void setPlatformTransactionId(String platformTransactionId) {
        this.platformTransactionId = platformTransactionId;
    }
}
