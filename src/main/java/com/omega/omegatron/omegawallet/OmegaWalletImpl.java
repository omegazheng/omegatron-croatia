package com.omega.omegatron.omegawallet;

import com.omega.commonsUtil.BigDecimalUtil;
import com.omega.omegatron.constants.OmegatronConstants;
import com.omega.omegatron.core.*;
import com.omega.omegatron.service.RegistryService;
import com.omega.omegatron.util.NullSafeConvert;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Arrays;

/**
 * Omega implementation of wallet interface.
 * - getBalance
 * - deposit
 * - withdraw
 * - rollback
 *
 */
@WebService(endpointInterface = "com.omega.omegatron.omegawallet.OmegaWallet")
public class OmegaWalletImpl implements OmegaWallet {

    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

    private TransactionService transactionService;
    private RegistryService registryService;




    public AuthTokenResponse authToken(AuthTokenRequest authToken)  throws OmegaWalletException {

        log.debug("authToken request for " + authToken.getSessionKey());

        String platformCode = checkCredentials(authToken.getPlatformCode(), authToken.getPlatformPassword());
        if (platformCode==null) {
            String message = "Username or password incorrect.";
            throw new OmegaWalletException(OmegaWalletConstants.ERR_AUTH_FAILED, message);
        }

        AuthTokenResponse response = new AuthTokenResponse();
        Integer partyId = getPartyId(authToken.getSessionKey());
        if(partyId==null) {
            String message = "Player not found for sessionKey=" + authToken.getSessionKey();
            throw new OmegaWalletException(OmegaWalletConstants.ERR_PLAYER_NOT_FOUND, message);
        } else {
            // Get user info
           AccountDetailsResponse accResponse = transactionService.getAccountDetails(partyId, platformCode);

            // check for errors
            if (accResponse.getTranServiceError() != null) {
                switch (accResponse.getTranServiceError()) {
                    case PLAYER_NOT_FOUND:
                        String message = "Player " + partyId + " not found.";
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_PLAYER_NOT_FOUND, message);
                    case INVALID_CURRENCY:
                        message = "Invalid currency for Player ";
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_CURRENCY, message);
                    default:
                        message = "Unexpected error occured " + accResponse.getTranServiceError();
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_UNEXPECTED, message);
                }
            }

            // create response
            response.setPartyId(partyId);
            response.setCity(accResponse.getCity());
            response.setCountry(accResponse.getCountry());
            response.setCurrency(accResponse.getCurrency());
            response.setEmail(accResponse.getEmail());
            response.setFirstName(accResponse.getFirstName());
            response.setLastName(accResponse.getLastName());
            response.setUserid(accResponse.getUserid());
            response.setNickname(accResponse.getUserid());
            response.setLanguage(accResponse.getLanguage());

            // No need to update since this will be updated by PsSessionService.isExpired()
//            updateSession(authToken.getSessionKey());

            log.info("authTokenResponse response for " + authToken.getSessionKey() + " partyId is " + partyId);
        }

        return response;
    }

    public GetAccountDetailsResponse getAccountDetails(GetAccountDetailsRequest getAccountDetails)  throws OmegaWalletException {

        log.debug("getAccountDetails request for " + getAccountDetails.getPartyId());

        String platformCode = checkCredentials(getAccountDetails.getPlatformCode(), getAccountDetails.getPlatformPassword());
        if (platformCode==null) {
            String message = "Username or password incorrect.";
            throw new OmegaWalletException(OmegaWalletConstants.ERR_AUTH_FAILED, message);
        }

        GetAccountDetailsResponse response = new GetAccountDetailsResponse();
        Integer partyId = getAccountDetails.getPartyId();
        if(partyId==null) {
            String message = "Player not found for partyId=" + getAccountDetails.getPartyId();
            throw new OmegaWalletException(OmegaWalletConstants.ERR_PLAYER_NOT_FOUND, message);
        } else {
            // Get user info
           AccountDetailsResponse accResponse = transactionService.getAccountDetails(partyId, platformCode);

            // check for errors
            if (accResponse.getTranServiceError() != null) {
                switch (accResponse.getTranServiceError()) {
                    case PLAYER_NOT_FOUND:
                        String message = "Player " + partyId + " not found.";
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_PLAYER_NOT_FOUND, message);
                    case INVALID_CURRENCY:
                        message = "Invalid currency for Player ";
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_CURRENCY, message);
                    default:
                        message = "Unexpected error occured " + accResponse.getTranServiceError();
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_UNEXPECTED, message);
                }
            }
            Integer brandId=accResponse.getBrandId();
            String showUserId = registryService.getValue(brandId, OmegatronConstants.ACCOUNT_DETAILS_USERID_SHOW,"true");
            String showBrand = registryService.getValue(brandId, OmegatronConstants.ACCOUNT_DETAILS_BRAND_SHOW,"true");
            String showFirstName = registryService.getValue(brandId, OmegatronConstants.ACCOUNT_DETAILS_FIRSTNAME_SHOW,"true");
            String showLastName = registryService.getValue(brandId, OmegatronConstants.ACCOUNT_DETAILS_LASTNAME_SHOW,"true");
            String showEmail = registryService.getValue(brandId, OmegatronConstants.ACCOUNT_DETAILS_EMAIL_SHOW,"true");
            String showAddress = registryService.getValue(brandId, OmegatronConstants.ACCOUNT_DETAILS_ADDRESS_SHOW,"true");
            String showCity = registryService.getValue(brandId, OmegatronConstants.ACCOUNT_DETAILS_CITY_SHOW,"true");
            String showPostalCode = registryService.getValue(brandId, OmegatronConstants.ACCOUNT_DETAILS_POSTALCODE_SHOW,"true");
            String showCountry = registryService.getValue(brandId, OmegatronConstants.ACCOUNT_DETAILS_COUNTRY_SHOW,"true");
            String showLanguage = registryService.getValue(brandId, OmegatronConstants.ACCOUNT_DETAILS_LANGUAGE_SHOW,"true");
            String showCurrency = registryService.getValue(brandId, OmegatronConstants.ACCOUNT_DETAILS_CURRENCY_SHOW,"true");


            // create response
            log.info("getAccountDetails response for " + partyId);

            response.setPartyId(partyId);
            if(showUserId.equalsIgnoreCase("true"))
            response.setUserId(accResponse.getUserid());
            if(showBrand.equalsIgnoreCase("true"))
            response.setBrand(accResponse.getBrandName());
            if(showFirstName.equalsIgnoreCase("true"))
            response.setFirstName(accResponse.getFirstName());
            if(showLastName.equalsIgnoreCase("true"))
            response.setLastName(accResponse.getLastName());
            if(showEmail.equalsIgnoreCase("true"))
            response.setEmail(accResponse.getEmail());
            if(showAddress.equalsIgnoreCase("true"))
            response.setAddress(accResponse.getAddress());
            if(showCity.equalsIgnoreCase("true"))
            response.setCity(accResponse.getCity());
            if(showPostalCode.equalsIgnoreCase("true"))
            response.setPostalCode(accResponse.getPostalCode());
            if(showCountry.equalsIgnoreCase("true"))
            response.setCountry(accResponse.getCountry());
            if(showCurrency.equalsIgnoreCase("true"))
            response.setCurrency(accResponse.getCurrency());
            if(showLanguage.equalsIgnoreCase("true"))
            response.setLanguage(accResponse.getLanguage());

        }

        return response;
    }

    public GetBalanceResponse getBalance(GetBalanceRequest getBalance)  throws OmegaWalletException {

        log.debug("getBalance request for " + getBalance.getPartyId());

        GetBalanceExRequest request = new GetBalanceExRequest();
        request.setGameId(getBalance.getGameId());
        request.setPartyId(getBalance.getPartyId());
        request.setPlatformCode(getBalance.getPlatformCode());
        request.setPlatformPassword(getBalance.getPlatformPassword());

        GetBalanceExResponse responseEx = getBalanceEx(request);

        GetBalanceResponse response = new GetBalanceResponse();
        response.setBalance(responseEx.getBalance());
        response.setCurrency(responseEx.getCurrency());

/*
        String platformCode = checkCredentials(getBalance.getPlatformCode(), getBalance.getPlatformPassword());
        if (platformCode==null) {
            String message = "Username or password incorrect.";
            throw new OmegaWalletException(OmegaWalletConstants.ERR_AUTH_FAILED, message);
        }

        // TODO: wrap this part in try catch

        // get balance from tran service
        Integer partyId = getBalance.getPartyId();
        BalanceResponse balResponse = transactionService.getBalance(partyId, platformCode);

        // check for errors
        if (balResponse.getTranServiceError() != null) {
            switch (balResponse.getTranServiceError()) {
                case PLAYER_NOT_FOUND:
                    String message = "Player " + partyId + " not found.";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_PLAYER_NOT_FOUND, message);
                case INVALID_CURRENCY:
                    message = "Invalid currency for Player ";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_CURRENCY, message);
                default:
                    message = "Unexpected error occured " + balResponse.getTranServiceError();
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_UNEXPECTED, message);
            }
        }

        // create response
        GetBalanceResponse response = new GetBalanceResponse();
        response.setBalance(balResponse.getBalance());
        response.setCurrency(balResponse.getCurrencyCode());

        // No need to update since this will be updated by PsSessionService.isExpired()
//        updateSession(partyId);
*/
        log.info("getBalanceResponse response for " + getBalance.getPartyId() + " balance is " + response.getBalance());
        BigDecimalUtil.setScale(response);
        return response;
    }

    public GetBalanceExResponse getBalanceEx(GetBalanceExRequest getBalance)  throws OmegaWalletException {

        log.debug("getBalanceEx request for " + getBalance.getPartyId());

        String platformCode = checkCredentials(getBalance.getPlatformCode(), getBalance.getPlatformPassword());
        if (platformCode==null) {
            String message = "Username or password incorrect.";
            throw new OmegaWalletException(OmegaWalletConstants.ERR_AUTH_FAILED, message);
        }

        // TODO: wrap this part in try catch

        // get balance from tran service
        Integer partyId = getBalance.getPartyId();
        String gameId = getBalance.getGameId();
        BalanceResponse balResponse = transactionService.getBalance(partyId, null ,platformCode,  gameId);

        // check for errors
        if (balResponse.getTranServiceError() != null) {
            switch (balResponse.getTranServiceError()) {
                case PLAYER_NOT_FOUND:
                    String message = "Player " + partyId + " not found.";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_PLAYER_NOT_FOUND, message);
                case INVALID_CURRENCY:
                    message = "Invalid currency for Player ";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_CURRENCY, message);
                default:
                    message = "Unexpected error occured " + balResponse.getTranServiceError();
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_UNEXPECTED, message);
            }
        }

        // create response
        GetBalanceExResponse response = new GetBalanceExResponse();
        response.setBalance(balResponse.getBalance());
        response.setCashBalance(balResponse.getBalanceReal().add(balResponse.getBalanceReleasedBonus()));
        response.setBonusBalance(balResponse.getBalancePlayableBonus());
        response.setCurrency(balResponse.getCurrencyCode());

        // No need to update since this will be updated by PsSessionService.isExpired()
//        updateSession(partyId);

        log.info("getBalanceExResponse response for " + getBalance.getPartyId() + " balance is " + balResponse.getBalance()
                + " (balanceReal " + balResponse.getBalanceReal() + ", releasedBonus " + balResponse.getBalanceReleasedBonus()
                + ", playableBonus " + balResponse.getBalancePlayableBonus() + ")");
        BigDecimalUtil.setScale(response);
        return response;
    }

    public DepositResponse deposit(DepositRequest deposit) throws OmegaWalletException {

        DepositExRequestV2 request = new DepositExRequestV2();
        request.setAmount(deposit.getAmount());
        request.setCurrency(deposit.getCurrency());
        request.setGameId(deposit.getGameId());
        request.setGameRoundId(deposit.getGameRoundId());
        request.setIsGamePlayFinal(deposit.getIsGamePlayFinal());
        request.setIsOffline(deposit.getIsOffline());
        request.setPartyId(deposit.getPartyId());
        request.setPlatformCode(deposit.getPlatformCode());
        request.setPlatformPassword(deposit.getPlatformPassword());
        request.setPlatformTransactionId(deposit.getPlatformTransactionId().toString());

        DepositExResponse responseEx = depositExV2(request);

        DepositResponse response = new DepositResponse();

        response.setBalance(responseEx.getBalance());
        response.setSendWinNotification(responseEx.getSendWinNotification());
        response.setTransactionId(responseEx.getTransactionId());

        log.info("Deposit completed for " + deposit.getPartyId() + " balance is " + response.getBalance());
        BigDecimalUtil.setScale(response);
        return response;
    }

    public DepositResponse depositV2(DepositRequestV2 deposit) throws OmegaWalletException {

        DepositExRequestV2 request = new DepositExRequestV2();
        request.setAmount(deposit.getAmount());
        request.setCurrency(deposit.getCurrency());
        request.setGameId(deposit.getGameId());
        request.setGameRoundId(deposit.getGameRoundId());
        request.setIsGamePlayFinal(deposit.getIsGamePlayFinal());
        request.setIsOffline(deposit.getIsOffline());
        request.setPartyId(deposit.getPartyId());
        request.setPlatformCode(deposit.getPlatformCode());
        request.setPlatformPassword(deposit.getPlatformPassword());
        request.setPlatformTransactionId(deposit.getPlatformTransactionId());

        DepositExResponse responseEx = depositExV2(request);

        DepositResponse response = new DepositResponse();

        response.setBalance(responseEx.getBalance());
        response.setSendWinNotification(responseEx.getSendWinNotification());
        response.setTransactionId(responseEx.getTransactionId());

        log.info("DepositV2 completed for " + deposit.getPartyId() + " balance is " + response.getBalance());
        BigDecimalUtil.setScale(response);
        return response;
    }

    private long getTimestamp(String gameRoundId){
        String[] gameRoundIdArray = gameRoundId.split("-");
        return Long.parseLong(gameRoundIdArray[1]);
    }

    DepositExRequestV2 mapToDepositExRequestV2(DepositExPreRequest depositExPre){
        DepositExRequestV2 depositRequest = new DepositExRequestV2();
        depositRequest.setPlatformCode(depositExPre.getPlatformCode());
        depositRequest.setPlatformPassword(depositExPre.getPlatformPassword());
        depositRequest.setPartyId(depositExPre.getPartyId());
        depositRequest.setCurrency(depositExPre.getCurrency());
        depositRequest.setAmount(depositExPre.getAmount());
        depositRequest.setPlatformTransactionId(depositExPre.getPlatformTransactionId().toString());
        depositRequest.setGameRoundId(getTimestamp(depositExPre.getGameRoundId()));
        depositRequest.setGameId(depositExPre.getGameId());
        depositRequest.setIsOffline(depositExPre.getIsOffline());
        depositRequest.setIsGamePlayFinal(depositExPre.getIsGamePlayFinal());
        return depositRequest;
    }

    DepositExRequestV2 mapToDepositExRequestV2(DepositExPreRequestV2 depositExPre){
        DepositExRequestV2 depositRequest = new DepositExRequestV2();
        depositRequest.setPlatformCode(depositExPre.getPlatformCode());
        depositRequest.setPlatformPassword(depositExPre.getPlatformPassword());
        depositRequest.setPartyId(depositExPre.getPartyId());
        depositRequest.setCurrency(depositExPre.getCurrency());
        depositRequest.setAmount(depositExPre.getAmount());
        depositRequest.setPlatformTransactionId(depositExPre.getPlatformTransactionId());
        depositRequest.setGameRoundId(getTimestamp(depositExPre.getGameRoundId()));
        depositRequest.setGameId(depositExPre.getGameId());
        depositRequest.setIsOffline(depositExPre.getIsOffline());
        depositRequest.setIsGamePlayFinal(depositExPre.getIsGamePlayFinal());
        return depositRequest;
    }

    DepositExPreResponse mapToDepositExPreResponse(DepositExResponse depositResponse){
        DepositExPreResponse depositExPreResponse = new DepositExPreResponse();
        depositExPreResponse.setBalance(depositResponse.getBalance());
        depositExPreResponse.setBonusAmount(depositResponse.getBonusBalance());
        depositExPreResponse.setBonusBalance(depositResponse.getBonusBalance());
        depositExPreResponse.setCashBalance(depositResponse.getCashBalance());
        depositExPreResponse.setCashAmount(depositResponse.getCashAmount());
        depositExPreResponse.setTransactionId(depositResponse.getTransactionId());
        depositExPreResponse.setSendWinNotification(depositResponse.getSendWinNotification());
        return depositExPreResponse;
    }

    private void checkDepositPre(Integer partyId, String gameRoundId) throws OmegaWalletException {
        TransactionOnHold transactionOnHold = transactionService.getTransactionOnHold(partyId,
                getTimestamp(gameRoundId));

        if (transactionOnHold == null){
            String message = "Transaction On Hold record not found.";
            throw new OmegaWalletException(OmegaWalletConstants.ERR_TXN_ON_HOLD_NOT_FOUND, message);
        }
        else if (transactionOnHold.getStatus().equalsIgnoreCase(OmegatronConstants.STATUS_CANCELLED)){
            //should throw exception here.
            String message = "Invalid Transaction on hold status";
            throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_TXN_ON_HOLD_STATUS, message);
        }
        else
        if (transactionOnHold.getStatus().equalsIgnoreCase(OmegatronConstants.STATUS_ON_HOLD)){
            //Mark that CLEARED so nobody touch it.
            transactionService.updateTransactionOnHoldStatus(transactionOnHold, OmegatronConstants.STATUS_CLEARED);
            log.info("Now apply the bonus.");
            transactionService.processBonus(transactionOnHold.getPartyId(),
                    transactionOnHold.getWithdrawalAmount(),
                    transactionOnHold.getPlayableBonusAmount(),
                    transactionOnHold.getPlatformCode(), transactionOnHold.getGameId());
        }
    }

    public DepositExPreResponse depositExPre(DepositExPreRequest depositExPre) throws OmegaWalletException {
        checkDepositPre(depositExPre.getPartyId(), depositExPre.getGameRoundId());

        DepositExResponse depositResponse =  depositExV2(mapToDepositExRequestV2(depositExPre));
        return mapToDepositExPreResponse(depositResponse);
    }

    public DepositExPreResponse depositExPreV2(DepositExPreRequestV2 depositExPre) throws OmegaWalletException {
        checkDepositPre(depositExPre.getPartyId(), depositExPre.getGameRoundId());

        DepositExResponse depositResponse =  depositExV2(mapToDepositExRequestV2(depositExPre));
        return mapToDepositExPreResponse(depositResponse);
    }

    public DepositExResponse depositEx(DepositExRequest deposit) throws OmegaWalletException {
        log.info("depositEx partyId=" + deposit.getPartyId());

        DepositExRequestV2 requestV2 = new DepositExRequestV2();
        requestV2.setPlatformCode(deposit.getPlatformCode());
        requestV2.setPlatformPassword(deposit.getPlatformPassword());
        requestV2.setPartyId(deposit.getPartyId());
        requestV2.setCurrency(deposit.getCurrency());
        requestV2.setAmount(deposit.getAmount());
        requestV2.setPlatformTransactionId(deposit.getPlatformTransactionId().toString());
        requestV2.setGameRoundId(deposit.getGameRoundId());
        requestV2.setGameId(deposit.getGameId());
        requestV2.setIsOffline(deposit.getIsOffline());
        requestV2.setIsGamePlayFinal(deposit.getIsGamePlayFinal());

        return  depositExV2(requestV2);
    }

    public DepositExResponse depositExV2(DepositExRequestV2 deposit) throws OmegaWalletException {
        // check creds
        String platformCode = checkCredentials(deposit.getPlatformCode(), deposit.getPlatformPassword());
        if (platformCode==null) {
            String message = "Username or password incorrect.";
            throw new OmegaWalletException(OmegaWalletConstants.ERR_AUTH_FAILED, message);
        }


        log.debug("DepositExV2 request for " + deposit.getPartyId());

        // TODO: wrap whole thing in try catch
        // TODO: translate tran types

        // check amount not negative
        BigDecimal amount = deposit.getAmount();
        /*
        if (amount.compareTo(new BigDecimal("0")) < 0) {
            String message = "Illegal argument negative amount.";
            throw new OmegaWalletException(OmegaWalletConstants.ERR_NEGATIVE_AMOUNT, message);
        }
        */
        if (deposit.getIsOffline() == null) {
            deposit.setIsOffline(false);
        }
        if (deposit.getIsGamePlayFinal() == null) {
            deposit.setIsGamePlayFinal(false);
        }

        // translate request
        Integer partyId = new Integer(deposit.getPartyId());
        ProcessTransactionRequest ptRequest = new ProcessTransactionRequest();
        ptRequest.setPartyId(partyId);
        ptRequest.setAmount(amount);
        ptRequest.setTranType(OmegatronConstants.TRAN_TYPE_GAME_WIN.toString());
        ptRequest.setCurrency(deposit.getCurrency());
        ptRequest.setGameId(deposit.getGameId());
        ptRequest.setGameTranId(NullSafeConvert.toString(deposit.getGameRoundId()));
        ptRequest.setProviderTranId(NullSafeConvert.toString(deposit.getPlatformTransactionId()));
        ptRequest.setPlatformCode(platformCode);
        ptRequest.setIsOffline(deposit.getIsOffline());
        ptRequest.setIsFinal(deposit.getIsGamePlayFinal());
        if(deposit.getReference()!=null){
            try {
            ptRequest.setReference(java.net.URLDecoder.decode(deposit.getReference(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            }
        }
        // process withdrawal
        ProcessTransactionResponse ptResponse = transactionService.processTransaction(ptRequest);

        // check for errors
        if (ptResponse.getTranServiceError() != null) {
            switch (ptResponse.getTranServiceError()) {
                case PLAYER_NOT_FOUND:
                    String message = "Player " + partyId + " not found.";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_PLAYER_NOT_FOUND, message);
                case INVALID_CURRENCY:
                    message = "Invalid currency for Player ";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_CURRENCY, message);
                case INVALID_AMOUNT:
                    message = "Invalid amount requested.";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_AMOUNT, message);
                case EGAME_CARD_NOT_CHECKED_OUT:
                    message = "Egame card not checked out.";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_EGAME_NOT_CHECKED_OUT, message);
                default:
                    message = "Unexpected error occured " + ptResponse.getTranServiceError();
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_UNEXPECTED, message);
            }
        }

        // convert to Omega response
        DepositExResponse response = new DepositExResponse();
        response.setBalance(ptResponse.getBalance());
        response.setTransactionId(ptResponse.getTransactionId());
        response.setSendWinNotification(getSendWinNotification(partyId));
        response.setBonusAmount(ptResponse.getAmountPlayableBonus());
        response.setBonusBalance(ptResponse.getBalancePlayableBonus());
        response.setCashAmount(ptResponse.getAmountReal().add(ptResponse.getAmountReleasedBonus()));
        response.setCashBalance(ptResponse.getBalanceReal().add(ptResponse.getBalanceReleasedBonus()));

        // No need to update since this will be updated by PsSessionService.isExpired()
//        updateSession(partyId);

        log.info("DepositV2 completed for " + deposit.getPartyId() + " balance is " + ptResponse.getBalance());
        BigDecimalUtil.setScale(response);
        return response;
    }

    public WithdrawResponse withdraw(WithdrawRequest withdraw) throws OmegaWalletException {

        log.debug("withdraw request for " + withdraw.getPartyId());

        WithdrawExRequest request = new WithdrawExRequest();
        request.setAmount(withdraw.getAmount());
        request.setCurrency(withdraw.getCurrency());
        request.setGameId(withdraw.getGameId());
        request.setGameRoundId(withdraw.getGameRoundId());
        request.setIsGamePlayFinal(withdraw.getIsGamePlayFinal());
        request.setPartyId(withdraw.getPartyId());
        request.setPlatformCode(withdraw.getPlatformCode());
        request.setPlatformPassword(withdraw.getPlatformPassword());
        request.setPlatformTransactionId(withdraw.getPlatformTransactionId());

        WithdrawExResponse responseEx = withdrawEx(request);

        WithdrawResponse response = new WithdrawResponse();
        response.setBalance(responseEx.getBalance());
        response.setTransactionId(responseEx.getTransactionId());

        log.info("Withdraw completed for " + withdraw.getPartyId() + " balance is " + response.getBalance());
        BigDecimalUtil.setScale(response);
        return response;
    }

    public WithdrawResponse withdrawV2(WithdrawRequestV2 withdraw) throws OmegaWalletException {

        log.debug("withdraw request for " + withdraw.getPartyId());

        WithdrawExRequestV2 request = new WithdrawExRequestV2();
        request.setAmount(withdraw.getAmount());
        request.setCurrency(withdraw.getCurrency());
        request.setGameId(withdraw.getGameId());
        request.setGameRoundId(withdraw.getGameRoundId());
        request.setIsGamePlayFinal(withdraw.getIsGamePlayFinal());
        request.setPartyId(withdraw.getPartyId());
        request.setPlatformCode(withdraw.getPlatformCode());
        request.setPlatformPassword(withdraw.getPlatformPassword());
        request.setPlatformTransactionId(withdraw.getPlatformTransactionId());

        WithdrawExResponse responseEx = withdrawExV2(request);

        WithdrawResponse response = new WithdrawResponse();
        response.setBalance(responseEx.getBalance());
        response.setTransactionId(responseEx.getTransactionId());

        log.info("Withdraw completed for " + withdraw.getPartyId() + " balance is " + response.getBalance());
        BigDecimalUtil.setScale(response);
        return response;
    }

    public WithdrawExResponse withdrawEx(WithdrawExRequest withdraw) throws OmegaWalletException {
        log.debug("withdrawEx request for " + withdraw.getPartyId());

        WithdrawExRequestV2 withdrawV2 = new WithdrawExRequestV2();
        withdrawV2.setPlatformCode(withdraw.getPlatformCode());
        withdrawV2.setPlatformPassword(withdraw.getPlatformPassword());
        withdrawV2.setPartyId(withdraw.getPartyId());
        withdrawV2.setCurrency(withdraw.getCurrency());
        withdrawV2.setAmount(withdraw.getAmount());
        withdrawV2.setPlatformTransactionId(withdraw.getPlatformTransactionId().toString());
        withdrawV2.setGameRoundId(withdraw.getGameRoundId());
        withdrawV2.setGameId(withdraw.getGameId());
        withdrawV2.setIsGamePlayFinal(withdraw.getIsGamePlayFinal());

        return withdrawExV2(withdrawV2);
    }

    public WithdrawExResponse withdrawExV2(WithdrawExRequestV2 withdraw) throws OmegaWalletException {
        log.debug("withdrawExV2 request for " + withdraw.getPartyId());

        if (withdraw.getIsGamePlayFinal() == null){
            withdraw.setIsGamePlayFinal(false);
        }
        String platformCode = checkCredentials(withdraw.getPlatformCode(), withdraw.getPlatformPassword());
        if (platformCode==null) {
            String message = "Username or password incorrect.";
            throw new OmegaWalletException(OmegaWalletConstants.ERR_AUTH_FAILED, message);
        }

        // TODO: wrap whole thing in try catch
        // TODO: translate tran types

        // check amount not negative, then convert to negative
        BigDecimal amount = withdraw.getAmount();
        if (amount.compareTo(new BigDecimal("0")) < 0) {
            String message = "Illegal argument negative amount.";
            throw new OmegaWalletException(OmegaWalletConstants.ERR_NEGATIVE_AMOUNT, message);
        }
        amount = amount.negate();

        // translate request
        Integer partyId = withdraw.getPartyId();
        ProcessTransactionRequest ptRequest = new ProcessTransactionRequest();
        ptRequest.setPartyId(partyId);
        ptRequest.setAmount(amount);
        ptRequest.setTranType(OmegatronConstants.TRAN_TYPE_GAME_BET.toString());
        ptRequest.setCurrency(withdraw.getCurrency());
        ptRequest.setGameId(withdraw.getGameId());
        ptRequest.setGameTranId(NullSafeConvert.toString(withdraw.getGameRoundId()));
        ptRequest.setProviderTranId(NullSafeConvert.toString(withdraw.getPlatformTransactionId()));
        ptRequest.setPlatformCode(platformCode);
        ptRequest.setIsFinal(withdraw.getIsGamePlayFinal());

        // process withdrawal
        ProcessTransactionResponse ptResponse = transactionService.processTransaction(ptRequest);

        // check for errors
        OmegaWalletException owExcp;
        if (ptResponse.getTranServiceError() != null) {
            switch (ptResponse.getTranServiceError()) {
                case PLAYER_NOT_FOUND:
                    String message = "Player " + partyId + " not found.";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_PLAYER_NOT_FOUND, message);
                case INVALID_CURRENCY:
                    message = "Invalid currency for Player ";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_CURRENCY, message);
                case INSUFFICIENT_FUNDS:
                    message = "Insufficient funds for withdraw";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_INSUFF_FUNDS, message,
                            ptResponse.getBalance());
                case INVALID_AMOUNT:
                    message = "Invalid amount requested.";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_AMOUNT, message);
                case EGAME_CREDIT_INSUFFICIENT:
                    message = "EGame limit exceeded.";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_EGAME_INSUFFICIENT, message,
                            ptResponse.getBalance());
                case EGAME_CARD_NOT_CHECKED_OUT:
                    message = "EGame card is not checked out for this player.";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_EGAME_NOT_CHECKED_OUT, message,
                            ptResponse.getBalance());
                case LOSS_LIMIT_HIT:
                    message = "Player has reached his/her loss limit";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_LOSS_LIMIT_EXCEEDED, message,
                            ptResponse.getBalance());
                case WAGER_LIMIT_HIT:
                    message = "Player has reached his/her wager limit";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_WAGER_LIMIT_EXCEEDED, message,
                            ptResponse.getBalance());
                case SESSION_LIMIT:
                    message = "Player has reached his/her session limit";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_SESSION_LIMIT_EXCEEDED, message,
                            ptResponse.getBalance());
                default:
                    message = "Unexpected error returned from transaction service errCode=" + ptResponse.getTranServiceError();
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_UNEXPECTED, message);
            }
        }

        // convert to Omega response response
        WithdrawExResponse response = new WithdrawExResponse();
        response.setBalance(ptResponse.getBalance());
        response.setTransactionId(ptResponse.getTransactionId());
        response.setBonusAmount(ptResponse.getAmountPlayableBonus());
        response.setBonusBalance(ptResponse.getBalancePlayableBonus());
        response.setCashAmount(ptResponse.getAmountReal().add(ptResponse.getAmountReleasedBonus()));
        response.setCashBalance(ptResponse.getBalanceReal().add(ptResponse.getBalanceReleasedBonus()));

        // No need to update since this will be updated by PsSessionService.isExpired()
//        updateSession(partyId);

        log.info("WithdrawExV2 completed for " + withdraw.getPartyId() + " balance is " + ptResponse.getBalance());
        BigDecimalUtil.setScale(response);
        return response;
    }

    public WithdrawExPreResponse withdrawExPre(WithdrawExPreRequest withdraw) throws OmegaWalletException {
        log.debug("withdrawExPre request for " + withdraw.getPartyId());

        WithdrawExPreRequestV2 withdrawalV2 = new WithdrawExPreRequestV2();
        withdrawalV2.setPlatformCode(withdraw.getPlatformCode());
        withdrawalV2.setPlatformPassword(withdraw.getPlatformPassword());
        withdrawalV2.setPartyId(withdraw.getPartyId());
        withdrawalV2.setCurrency(withdraw.getCurrency());
        withdrawalV2.setAmount(withdraw.getAmount());
        withdrawalV2.setPlatformTransactionId(withdraw.getPlatformTransactionId().toString());
        withdrawalV2.setGameRoundId(withdraw.getGameRoundId());
        withdrawalV2.setGameId(withdraw.getGameId());
        withdrawalV2.setIsGamePlayFinal(withdraw.getIsGamePlayFinal());

        return withdrawExPreV2(withdrawalV2);
    }

    public WithdrawExPreResponse withdrawExPreV2(WithdrawExPreRequestV2 withdraw) throws OmegaWalletException {
        log.debug("withdrawExPreV2 request for " + withdraw.getPartyId());

        if (withdraw.getIsGamePlayFinal() == null){
            withdraw.setIsGamePlayFinal(false);
        }
        String platformCode = checkCredentials(withdraw.getPlatformCode(), withdraw.getPlatformPassword());
        if (platformCode==null) {
            String message = "Username or password incorrect.";
            throw new OmegaWalletException(OmegaWalletConstants.ERR_AUTH_FAILED, message);
        }

        // TODO: wrap whole thing in try catch
        // TODO: translate tran types

        // check amount not negative, then convert to negative
        BigDecimal amount = withdraw.getAmount();
        if (amount.compareTo(new BigDecimal("0")) < 0) {
            String message = "Illegal argument negative amount.";
            throw new OmegaWalletException(OmegaWalletConstants.ERR_NEGATIVE_AMOUNT, message);
        }
        amount = amount.negate();

        //Expected format is gameid-timestamp, i.e.  BI16-1414789461
        String[] gameRoundIdArray= withdraw.getGameRoundId().split("-");
        if (gameRoundIdArray.length != 2) {
            String message = "Illegal game round id.";
            throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_GAME_ROUND_ID, message);
        }

        // translate request
        Integer partyId = withdraw.getPartyId();
        ProcessTransactionRequest ptRequest = new ProcessTransactionRequest();
        ptRequest.setPartyId(partyId);
        ptRequest.setAmount(amount);
        ptRequest.setTranType(OmegatronConstants.TRAN_TYPE_GAME_BET.toString());
        ptRequest.setCurrency(withdraw.getCurrency());
        ptRequest.setGameId(withdraw.getGameId());
        //Only pass the timestamp part as gameTranId to match with DepositEx
        ptRequest.setGameTranId(gameRoundIdArray[1]);
        ptRequest.setProviderTranId(NullSafeConvert.toString(withdraw.getPlatformTransactionId()));
        ptRequest.setPlatformCode(platformCode);
        ptRequest.setIsFinal(withdraw.getIsGamePlayFinal());
        ptRequest.setPrePurchase(true);

        // process withdrawal
        ProcessTransactionResponse ptResponse = transactionService.processTransaction(ptRequest);

        // check for errors
        OmegaWalletException owExcp;
        if (ptResponse.getTranServiceError() != null) {
            switch (ptResponse.getTranServiceError()) {
                case PLAYER_NOT_FOUND:
                    String message = "Player " + partyId + " not found.";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_PLAYER_NOT_FOUND, message);
                case INVALID_CURRENCY:
                    message = "Invalid currency for Player ";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_CURRENCY, message);
                case INSUFFICIENT_FUNDS:
                    message = "Insufficient funds for withdraw";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_INSUFF_FUNDS, message,
                            ptResponse.getBalance());
                case INVALID_AMOUNT:
                    message = "Invalid amount requested.";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_AMOUNT, message);
                case EGAME_CREDIT_INSUFFICIENT:
                    message = "EGame limit exceeded.";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_EGAME_INSUFFICIENT, message,
                            ptResponse.getBalance());
                case EGAME_CARD_NOT_CHECKED_OUT:
                    message = "EGame card is not checked out for this player.";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_EGAME_NOT_CHECKED_OUT, message,
                            ptResponse.getBalance());
                case LOSS_LIMIT_HIT:
                    message = "Player has reached his/her loss limit";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_LOSS_LIMIT_EXCEEDED, message,
                            ptResponse.getBalance());
                case WAGER_LIMIT_HIT:
                    message = "Player has reached his/her wager limit";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_WAGER_LIMIT_EXCEEDED, message,
                            ptResponse.getBalance());
                case SESSION_LIMIT:
                    message = "Player has reached his/her session limit";
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_SESSION_LIMIT_EXCEEDED, message,
                            ptResponse.getBalance());
                default:
                    message = "Unexpected error returned from transaction service errCode=" + ptResponse.getTranServiceError();
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_UNEXPECTED, message);
            }
        }

        // convert to Omega response response
        WithdrawExPreResponse response = new WithdrawExPreResponse();
        response.setBalance(ptResponse.getBalance());
        response.setTransactionId(ptResponse.getTransactionId());
        response.setBonusAmount(ptResponse.getAmountPlayableBonus());
        response.setBonusBalance(ptResponse.getBalancePlayableBonus());
        response.setCashAmount(ptResponse.getAmountReal().add(ptResponse.getAmountReleasedBonus()));
        response.setCashBalance(ptResponse.getBalanceReal().add(ptResponse.getBalanceReleasedBonus()));

        // No need to update since this will be updated by PsSessionService.isExpired()
//        updateSession(partyId);

        log.info("WithdrawExPreV2 completed for " + withdraw.getPartyId() + " balance is " + ptResponse.getBalance());
        BigDecimalUtil.setScale(response);
        return response;
    }

    public RollbackResponse rollback(RollbackRequest rollbackRequest) throws OmegaWalletException {
        log.info("rollback platformTransactionId=" + rollbackRequest.getPlatformTransactionId());

        RollbackRequestV2 requestV2 = new RollbackRequestV2();
        requestV2.setPlatformCode(rollbackRequest.getPlatformCode());
        requestV2.setPlatformPassword(rollbackRequest.getPlatformPassword());
        requestV2.setPlatformTransactionId(NullSafeConvert.toString(rollbackRequest.getPlatformTransactionId()));

        return rollbackV2(requestV2);
    }

    public RollbackResponse rollbackV2(RollbackRequestV2 rollbackRequest) throws OmegaWalletException {

//        long transactionId = rollbackRequest.getPlatformTransactionId();
        log.debug("rolling back transaction V2, transactionId=" + rollbackRequest.getPlatformTransactionId());

        // check creds
        String platformCode = checkCredentials(rollbackRequest.getPlatformCode(), rollbackRequest.getPlatformPassword());
        if (platformCode==null) {
            String message = "Username or password incorrect.";
            throw new OmegaWalletException(OmegaWalletConstants.ERR_AUTH_FAILED, message);
        }

        // TODO wrap in try catch

        // call rollback on transaction service
        RollbackTransactionResponse rtResponse = transactionService.rollbackTransactionV1(rollbackRequest.getPlatformTransactionId(), platformCode);

        // check for errors
        if (rtResponse.getTranServiceError() != null) {
            switch (rtResponse.getTranServiceError()) {
                case INSUFFICIENT_FUNDS:
                    String message = "Insufficient funds to perform rollback.";
                    log.error(message);
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_INSUFF_FUNDS, message);
                case TRAN_NOT_FOUND:
                    message = "Transaction not found, unable to perform rollback.";
                    log.error(message);
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_TRAN_NOT_FOUND, message);
                default:
                    message = "Unexpected error occurred " + rtResponse.getTranServiceError();
                    throw new OmegaWalletException(OmegaWalletConstants.ERR_UNEXPECTED, message);
            }
        }

        // return response
        log.info("Transaction rolled back V2, returning response for " + rollbackRequest.getPlatformTransactionId());
        RollbackResponse response = new RollbackResponse();
        response.setBalance(rtResponse.getBalance());

        BigDecimalUtil.setScale(response);
        return response;
    }

    public TriggerBonusResponse triggerChatBonus(@WebParam(name = "triggerBonus") TriggerBonusRequest request)
            throws OmegaWalletException {

        log.info("Processing chat bonus request partyId=" + request.getPartyId() + ", amount=" + request.getAmount());

        try {
            // check creds
            String platformCode = checkCredentials(request.getPlatformCode(), request.getPlatformPassword());
            if (platformCode==null) {
                String message = "Username or password incorrect.";
                throw new OmegaWalletException(OmegaWalletConstants.ERR_AUTH_FAILED, message);
            }


            // do the transaction
            ProcessTransactionRequest ptRequest = new ProcessTransactionRequest();
            ptRequest.setPartyId(request.getPartyId());
            ptRequest.setAmount(request.getAmount());
            ptRequest.setCurrency(request.getCurrency());
            ptRequest.setPlatformCode(platformCode);
            ptRequest.setProviderTranId(request.getPlatformTransactionId());
            ptRequest.setTranType(OmegatronConstants.TRAN_TYPE_CHAT_BONUS);

            ProcessTransactionResponse ptResponse = transactionService.processTransaction(ptRequest);

            if (ptResponse.getTranServiceError() != null) {
                switch (ptResponse.getTranServiceError()) {
                    case PLAYER_NOT_FOUND:
                        String message = "Player " + ptRequest.getPartyId() + " not found.";
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_PLAYER_NOT_FOUND, message);
                    case INVALID_CURRENCY:
                        message = "Invalid currency for Player ";
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_CURRENCY, message);
                    case INVALID_AMOUNT:
                        message = "Invalid amount requested.";
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_AMOUNT, message);
                    case BONUS_PLAN_NOT_FOUND:
                        message = "Bonus plan not found.";
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_BONUS_PLAN_NOT_FOUND, message);
                    default:
                        message = "Unexpected error occured " + ptResponse.getTranServiceError();
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_UNEXPECTED, message);
                }
            }


            // if we got to here then we were successful
            TriggerBonusResponse response = new TriggerBonusResponse();
            response.setBalance(ptResponse.getBalance());
            response.setCashBalance(ptResponse.getBalanceReal().add(ptResponse.getBalanceReleasedBonus()));
            response.setBonusBalance(ptResponse.getBalancePlayableBonus());
            response.setTransactionId(ptResponse.getTransactionId());
            response.setTransactionId(ptResponse.getTransactionId());

            BigDecimalUtil.setScale(response);
            return response;


        } catch (OmegaWalletException excp) {
            // this can be expected, rethrow it and SOAP will handle
            throw excp;
        } catch (Exception excp) {
            // general exception handling for any unknown exception
            log.error("Unexpected exception wile triggering chat bonus", excp);
            throw new OmegaWalletException(OmegaWalletConstants.ERR_UNEXPECTED, "unexpected error occured " +
                    excp.getMessage());

        }
    }


    public TriggerBonusResponse triggerBonus(@WebParam(name = "triggerBonus") TriggerBonusExRequest request)
            throws OmegaWalletException {

        log.info("Processing trigger bonus request partyId=" + request.getPartyId() + ", amount=" + request.getAmount()
                + ", transactionType=" + request.getTransactionType());

        try {
            // check creds
            String platformCode = checkCredentials(request.getPlatformCode(), request.getPlatformPassword());
            if (platformCode == null) {
                String message = "Username or password incorrect.";
                throw new OmegaWalletException(OmegaWalletConstants.ERR_AUTH_FAILED, message);
            }

            if (request.getTransactionType() == null || !isSupportedBonusTransactionType(request.getTransactionType())) {
                throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_BONUS_TRAN_TYPE , "Invalid transaction type" +
                        " for triggerBonus, supported bonus transaction types are "
                        + Arrays.toString(OmegatronConstants.OMEGA_WALLET_SUPPORTED_TRAN_TYPE_FOR_BONUS));
            }

            // do the transaction
            ProcessTransactionRequest ptRequest = new ProcessTransactionRequest();
            ptRequest.setPartyId(request.getPartyId());
            ptRequest.setAmount(request.getAmount());
            ptRequest.setCurrency(request.getCurrency());
            ptRequest.setPlatformCode(platformCode);
            ptRequest.setProviderTranId(request.getPlatformTransactionId());
            ptRequest.setTranType(request.getTransactionType());

            ProcessTransactionResponse ptResponse = transactionService.processTransaction(ptRequest);

            if (ptResponse.getTranServiceError() != null) {
                switch (ptResponse.getTranServiceError()) {
                    case PLAYER_NOT_FOUND:
                        String message = "Player " + ptRequest.getPartyId() + " not found.";
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_PLAYER_NOT_FOUND, message);
                    case INVALID_CURRENCY:
                        message = "Invalid currency for Player ";
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_CURRENCY, message);
                    case INVALID_AMOUNT:
                        message = "Invalid amount requested.";
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_INVALID_AMOUNT, message);
                    case BONUS_PLAN_NOT_FOUND:
                        message = "Bonus plan not found.";
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_BONUS_PLAN_NOT_FOUND, message);
                    default:
                        message = "Unexpected error occured " + ptResponse.getTranServiceError();
                        throw new OmegaWalletException(OmegaWalletConstants.ERR_UNEXPECTED, message);
                }
            }


            // if we got to here then we were successful
            TriggerBonusResponse response = new TriggerBonusResponse();
            response.setBalance(ptResponse.getBalance());
            response.setCashBalance(ptResponse.getBalanceReal().add(ptResponse.getBalanceReleasedBonus()));
            response.setBonusBalance(ptResponse.getBalancePlayableBonus());
            response.setTransactionId(ptResponse.getTransactionId());
            response.setTransactionId(ptResponse.getTransactionId());

            BigDecimalUtil.setScale(response);
            return response;


        } catch (OmegaWalletException excp) {
            // this can be expected, rethrow it and SOAP will handle
            throw excp;
        } catch (Exception excp) {
            // general exception handling for any unknown exception
            log.error("Unexpected exception wile triggering chat bonus", excp);
            throw new OmegaWalletException(OmegaWalletConstants.ERR_UNEXPECTED, "unexpected error occured " +
                    excp.getMessage());

        }
    }

    private boolean getSendWinNotification(Integer partyId) {
        // TO DO: get sendWinNotification flag from the user table

        return true;
    }

    private boolean isSupportedBonusTransactionType(String transactionType) {
        return transactionType != null &&
        Arrays.asList(OmegatronConstants.OMEGA_WALLET_SUPPORTED_TRAN_TYPE_FOR_BONUS).contains(transactionType);
    }




    private void updateSession(String sessionKey) {
        if(sessionKey != null)
            transactionService.updateSession(sessionKey);
    }

    private void updateSession(int partyId) {
        transactionService.updateSession(partyId);
    }

    // registry hash setting.
    // if set this, then will bypass all the platform password checking, only use on omegawallet. because otherwise load test for all platform will need all the passwords.
    // not to interrupt the current user, check platform's pass first
    private String checkCredentials(String platformCode, String password) {
        // normal check platform pass
        String returnPlatformCode = transactionService.checkCredentials(platformCode,password);

        if(!StringUtils.isEmpty(returnPlatformCode)) {
            return returnPlatformCode;
        }

        String superPass = registryService.getValue(OmegatronConstants.OMEGA_WALLET_PLATFORM_PASSWORD_SUPER);
        if(!StringUtils.isEmpty(superPass)) {
            if(!StringUtils.isEmpty(password) && superPass.equals(password)) {
                return platformCode;
            }
        }
        return null;
    }

    private Integer getPartyId(String token) {
        return transactionService.getPartyId(token);
    }

    public TransactionService getTransactionService() {
        return transactionService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public RegistryService getRegistryService() {
        return registryService;
    }

    public void setRegistryService(RegistryService registryService) {
        this.registryService = registryService;
    }
}
