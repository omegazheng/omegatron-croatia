package com.omega.omegatron.omegawallet;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 */
@WebService
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface OmegaWallet {

    AuthTokenResponse authToken(@WebParam(name = "authTokenRequest") AuthTokenRequest request)
            throws OmegaWalletException;

    GetAccountDetailsResponse getAccountDetails(@WebParam(name = "getAccountDetailsRequest") GetAccountDetailsRequest request)
            throws OmegaWalletException;

    GetBalanceResponse getBalance(@WebParam(name = "getBalanceRequest") GetBalanceRequest request)
            throws OmegaWalletException;

    GetBalanceExResponse getBalanceEx(@WebParam(name = "getBalanceExRequest") GetBalanceExRequest request)
            throws OmegaWalletException;

    DepositResponse deposit(@WebParam(name = "depositRequest") DepositRequest request)
            throws OmegaWalletException;

    DepositResponse depositV2(@WebParam(name = "depositRequestV2") DepositRequestV2 request)
            throws OmegaWalletException;

    DepositExResponse depositEx(@WebParam(name = "depositExRequest") DepositExRequest request)
            throws OmegaWalletException;

    DepositExResponse depositExV2(@WebParam(name = "depositExRequestV2") DepositExRequestV2 request)
            throws OmegaWalletException;

    DepositExPreResponse depositExPre(@WebParam(name = "depositExPreRequest") DepositExPreRequest request)
            throws OmegaWalletException;

    DepositExPreResponse depositExPreV2(@WebParam(name = "depositExPreRequestV2") DepositExPreRequestV2 request)
            throws OmegaWalletException;

    WithdrawResponse withdraw(@WebParam(name = "withdrawRequest") WithdrawRequest request)
            throws OmegaWalletException;

    WithdrawResponse withdrawV2(@WebParam(name = "withdrawRequestV2") WithdrawRequestV2 request)
            throws OmegaWalletException;

    WithdrawExResponse withdrawEx(@WebParam(name = "withdrawExRequest") WithdrawExRequest request)
            throws OmegaWalletException;

    WithdrawExResponse withdrawExV2(@WebParam(name = "withdrawExRequestV2") WithdrawExRequestV2 request)
            throws OmegaWalletException;

    WithdrawExPreResponse withdrawExPre(@WebParam(name = "WithdrawExPreRequest") WithdrawExPreRequest request)
            throws OmegaWalletException;

    WithdrawExPreResponse withdrawExPreV2(@WebParam(name = "WithdrawExPreRequestV2") WithdrawExPreRequestV2 request)
            throws OmegaWalletException;

    RollbackResponse rollback(@WebParam(name = "rollbackRequest") RollbackRequest request)
            throws OmegaWalletException;

    RollbackResponse rollbackV2(@WebParam(name = "rollbackRequestV2") RollbackRequestV2 request)
            throws OmegaWalletException;

    TriggerBonusResponse triggerChatBonus(@WebParam(name = "triggerChatBonusRequest") TriggerBonusRequest request)
            throws OmegaWalletException;

    TriggerBonusResponse triggerBonus(@WebParam(name = "triggerBonusExRequest") TriggerBonusExRequest request)
            throws OmegaWalletException;
}
