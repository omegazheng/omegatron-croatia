package com.omega.omegatron.constant;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created by songleo on 2016-05-09.
 */
abstract public class GenericCommonResponse {
    public GenericCommonResponse(){
        request = "";
        rc = GenericReturnCode.RETURN_SUCCESS;
    }

    @XmlAttribute
    private GenericReturnCode rc;

    @XmlAttribute
    protected String request;

    public String getRc() {
        return rc.getCode();
    }

    public void setRc(GenericReturnCode rc) {
        this.rc = rc;
    }


    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }
}
