package com.omega.omegatron.constant;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by songleo on 2016-05-09.
 */
@XmlRootElement(name="RSP")
@XmlAccessorType(XmlAccessType.FIELD)
public class GenericErrorResponse extends  GenericCommonResponse {
    public GenericErrorResponse(){}

    @XmlAttribute
    private String msg;


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
