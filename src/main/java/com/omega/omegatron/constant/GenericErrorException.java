package com.omega.omegatron.constant;

/**
 * Created by songleo on 2016-05-09.
 */
public class GenericErrorException extends Exception {
    GenericReturnCode rc;
    String message;
    GenericErrorResponse errorResponse = new GenericErrorResponse();

    public GenericErrorException(String request, GenericReturnCode rc) {
        this.rc = rc;
        errorResponse.setRequest(request);
        errorResponse.setRc(rc);
    }

    public GenericErrorException(String request, GenericReturnCode rc, String message) {
        this.rc = rc;
        this.message = message;
        errorResponse.setRequest(request);
        errorResponse.setRc(rc);
        errorResponse.setMsg(message);
    }

    public GenericErrorResponse getErrorResponse() {
        return errorResponse;
    }
}
