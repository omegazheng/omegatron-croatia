package com.omega.omegatron.constant;

/**
 * Created by songleo on 2016-05-09.
 */

import javax.xml.bind.annotation.*;

@XmlType(name = "rc")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlEnum
public enum GenericReturnCode {

    // Return Code and Msg
    @XmlEnumValue("0")
    RETURN_SUCCESS("0", "The request was successfully executed."),
    @XmlEnumValue("1")
    RETURN_TECHNICAL_ERROR("1", "A technical error occurred while processing the request."),
    @XmlEnumValue("3")
    RETURN_CONFIG_ERROR("3", "The game is not configured for the operator."),
    @XmlEnumValue("12")
    RETURN_BAD_ARGUMENT("12","The request contains an unexpected parameter value."),
    @XmlEnumValue("102")
    RETURN_WAGER_NOT_FOUND ( "102", "An error occurred during cancelwager request. The wager been cancelled was not present in the integrator's system."),
    @XmlEnumValue("108")
    RETURN_ROLLBACK_FAILURE("108", "An error occurred during cancelwager request. Probable causes: Wager was already cancelled, cancelwager was attempted for a result call or round is already closed."),
    @XmlEnumValue("1000")
    RETURN_NOT_LOGGED_ON("1000", "The session ID is invalid."),
    @XmlEnumValue("1003")
    RETURN_AUTHENTICATION_FAILED("1003", "Unable to authenticate user identity."),
    @XmlEnumValue("1006")
    RETURN_OUT_OF_MONEY("1006", "The player doesn't have sufficient funds for the bet."),
    @XmlEnumValue("1008")
    RETURN_PARAMETER_REQUIRED("1008", "Missing required parameter(s) "),
    @XmlEnumValue("1019")
    RETURN_GAMING_LIMITS ("1019", "The player is not allowed to perform the bet due to gaming limits in the wallet platform."),
    @XmlEnumValue("1035")
    RETURN_ACCOUNT_BLOCKED("1035", "The account associated with a given session ID is blocked and no bets can be performed.");

    private final String code;
    private final String msg;

    GenericReturnCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public static GenericReturnCode fromCode(String code) {
        for (GenericReturnCode element : GenericReturnCode.values()) {
            if (element.getCode().equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

    public String toString() {
        return getCode();
    }

}
