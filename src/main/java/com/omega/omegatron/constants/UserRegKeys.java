package com.omega.omegatron.constants;

/**
 * User registry keys (key value mapping for users).
 */
public class UserRegKeys {
    public final static String MG_POKER_ALIAS = "microgaming.pokerAlias";

    public static final String NETENTBOUNCEBACK_EGAMEERROR = "netEntBounceback.error";
}
