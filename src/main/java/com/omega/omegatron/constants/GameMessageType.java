package com.omega.omegatron.constants;

/**
 *
 */
public enum GameMessageType {

    HIT_LOSS_LIMIT("HIT_LOSS_LIMIT"),
    HIT_WAGER_LIMIT("HIT_WAGER_LIMIT"),
    HIT_SESSION_LIMIT("HIT_SESSION_LIMIT"),
    START_USE_PLAYABLE_BONUS("START_USE_PLAYABLE_BONUS");

    private final String code;

    GameMessageType(String code) {
        this.code = code;
    }

    public static GameMessageType fromCode(String code) {
        for (GameMessageType element : GameMessageType.values()) {
            if (element.getCode().equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String toString() {
        return code;
    }

}

