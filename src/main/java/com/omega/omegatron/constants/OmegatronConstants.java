package com.omega.omegatron.constants;

import java.util.Arrays;
import java.util.List;

/**
 */
public class OmegatronConstants {
    // public static final String TRAN_TYPE_GAME_PLAY = "GAME_PLAY";
    public static final String TRAN_TYPE_GAME_BET = "GAME_BET";
    public static final String TRAN_TYPE_GAME_WIN = "GAME_WIN";
    public static final String TRAN_TYPE_GAME_ADJUSTMENT = "GAME_ADJ";
    public static final String TRAN_TYPE_ROLLBACK = "ROLLBACK";
    public static final String TRAN_TYPE_BONUS_RELEASE = "BONUS_REL";
    public static final String TRAN_TYPE_BONUS_EXPIRE = "EXP_BONUS";
    public static final String TRAN_TYPE_TRANSFER_IN = "TRANSF_IN";
    public static final String TRAN_TYPE_TRANSFER_OUT = "TRANSF_OUT";
    public static final String TRAN_TYPE_TOURN_WIN = "TOURN_WIN";
    public static final String TRAN_TYPE_PLATFORM_BONUS = "PLTFRM_BON";
    public static final String TRAN_TYPE_CHAT_BONUS = "CHAT_BONUS";
    public static final String TRAN_TYPE_TIPS = "TIPS";
    public static final String TRAN_TYPE_END_GAME = "END_GAME";
    public static final String TRAN_TYPE_CASH_OUT = "CASH_OUT";
    public static final String TRAN_TYPE_STAKE_DECREASE = "STAKE_DEC";
    public static final String TRAN_TYPE_REFUND = "REFUND";
    public static final String TRAN_TYPE_AUTO_CHARGE = "AUTO_CHAR";
    public static final String TRAN_TYPE_SYSTEM_EFT = "SYSTEM_EFT";
    public static final String TRAN_TYPE_MACHINE_EFT = "MACHIN_EFT";

    public static final String ROLLBACK_PLATFORM_TRAN_ID_PREFIX = "RB-";

    public static final String[] OMEGA_WALLET_SUPPORTED_TRAN_TYPE_FOR_BONUS = {TRAN_TYPE_PLATFORM_BONUS, TRAN_TYPE_CHAT_BONUS};

    public static final String PLATFORM_CHARTWELL_CAS = "CW_CAS";
    public static final String PLATFORM_NETENT_CAS = "NETENT_CAS";
    public static final String PLATFORM_QUICKFIRE_CAS = "QFIRE_CAS";
    public static final String PLATFORM_BGS_SPORTS = "BGS_SPORTS"; //Obsolete
    public static final String PLATFORM_MG_POKER = "MG_POKER";
    public static final String PLATFORM_OMEGA_CAS = "OMEGA_CAS";
    public static final String PLATFORM_MG_CIP = "MG_CIP";
    public static final String PLATFORM_NYX_CAS = "NYX_CAS";
    public static final String PLATFORM_EVO_CAS = "EVO_CAS";
    public static final String PLATFORM_COMTRADE_CAS = "CT_CAS";
    public static final String PLATFORM_PARLAY_BINGO = "PALY_BINGO";
    public static final String PLATFORM_YGGDRASIL = "YGGDRASIL";
    public static final String PLATFORM_OFFSIDE = "OFFSIDE";
    public static final String PLATFORM_BETGAMES = "BETGAMES";
    public static final String PLATFORM_EZUGI = "EZUGI";
    public static final String PLATFORM_TOPGAME = "TOPGAME";
    public static final String PLATFORM_PRAGMATIC_BINGO = "PRAGMATIC_BINGO";
    public static final String PLATFORM_BETCONSTRUCT = "BETCONST";
    public static final String PLATFORM_SOCCERDATABET = "SDB_CAS";
    public static final String PLATFORM_GENII="GENII";
    public static final String PLATFORM_NOHALFTIME="NOHALFTIME";
    public static final String PLATFORM_OMEGASSW = "OMEGASSW";
    public static final String PLATFORM_ISOFTBET = "ISOFTBET";
    public static final String PLATFORM_PLAYNGO  = "PLAYNGO";
    public static final String PLATFORM_PLAYSON  = "PLAYSON";
    public static final String PLATFORM_QUICKSPIN  = "QUICKSPIN";
    public static final String PLATFORM_DIGITAIN_SPORTBOOK = "DIGITAIN_SPORTBOOK";
    public static final String PLATFORM_RHINO = "SLOTCO";
    public static final String PLATFORM_BETSOFT = "BETSOFT";
    public static final String PLATFORM_TOMHORN = "TOMHORN";
    public static final String PLATFORM_DASHUR = "DASHUR";
    public static final String PLATFORM_VNET = "VNET";
    public static final String PLATFORM_IGT_CAS = "IGT_CAS";
    public static final String PLATFORM_VIG_GAMEFEED = "VIG_GAMEFEED";
    public static final String PLATFORM_LEANDER = "LEANDER";
    public static final String PLATFORM_GREENTUBE = "GREENTUBE";
    public static final String PLATFORM_BCCASINO = "BCCASINO";
    public static final String PLATFORM_BCSPORTSBOOK = "BCSPORTSBOOK";
    public static final String PLATFORM_BONUS = "BONUS";
    public static final String PLATFORM_RIMINI = "RIMINI";
    public static final String PLATFORM_LOTTOLAND = "LOTTOLAND";

    public static final String PLATFORM_BIA_SPORTBOOK_PRELIVE = "BIA_PRELIVE";
    public static final String PLATFORM_BIA_SPORTBOOK_BETLIST = "BIA_BETLIST";
    public static final String PLATFORM_BIA_SPORTBOOK_LIVE = "BIA_LIVE";
    public static final String PLATFORM_BIA_SPORTBOOK_TOTO = "BIA_TOTO";
    public static final String PLATFORM_BIA_SPORTBOOK_VBL = "BIA_VBL";
    public static final String PLATFORM_BIA_SPORTBOOK_VDR = "BIA_VDR";
    public static final String PLATFORM_BIA_SPORTBOOK_VFEC = "BIA_VFEC";
    public static final String PLATFORM_BIA_SPORTBOOK_VFL = "BIA_VFL";
    public static final String PLATFORM_BIA_SPORTBOOK_VFLM = "BIA_VFLM";
    public static final String PLATFORM_BIA_SPORTBOOK_VHC = "BIA_VHC";
    public static final String PLATFORM_BIA_SPORTBOOK_VTO = "BIA_VTO";
    public static final String PLATFORM_BIA_SPORTBOOK_MIXED = "BIA_Mixed";
    public static final String PLATFORM_BIA_SPORTBOOK = "BIA_SPORTBOOK";

    public static final String PLATFORM_NOLIMIT_CITY = "NOLIMIT_CITY";
    public static final String NOLIMIT_CITY_OPERATOR = "nolimit.city.operator";
    public static final String NOLIMIT_CITY_KEY      = "nolimit.city.key";

    public static final String PLATFORM_ELK             = "ELK_CAS";
    public static final String ELK_PARTNER_UID          = "elk.partner.uid";
    public static final String ELK_PARTNER_KEY          = "elk.partner.key";
    public static final String ELK_OPERATOR_ID          = "elk.operator.id";
    public static final String ELK_PARTNER_UID_DEFAULT  = "elkstage";
    public static final String ELK_PARTNER_KEY_DEFAULT  = "passwordstage";
    public static final String ELK_OPERATOR_ID_DEFAULT  = "7770127";

    public static final String PRODUCT_EGT              = "EGT_CAS";
    public static final String EGT_PARTNER_UID          = "egt.partner.uid";
    public static final String EGT_PARTNER_KEY          = "egt.partner.key";
    public static final String EGT_PARTNER_UID_DEFAULT  = "devuid";
    public static final String EGT_PARTNER_KEY_DEFAULT  = "devkey";

    public static final String PLATFORM_BLUEPRINT             = "BLUEPRINT";
    public static final String BLUEPRINT_PARTNER_UID          = "blueprint.partner.uid";
    public static final String BLUEPRINT_PARTNER_KEY          = "blueprint.partner.key";
    public static final String BLUEPRINT_PARTNER_UID_DEFAULT  = "";
    public static final String BLUEPRINT_PARTNER_KEY_DEFAULT = "";
    public static final String PLAYABLE_BONUS_ALERT_PRODUCTS = "playableBonusAlert.products";

    public static List<String> BIA_PLATFORM_LIST = Arrays.asList(
            "BIA_PRELIVE",
            "BIA_BETLIST",
            "BIA_OVERVIEW",
            "BIA_LIVE",
            "BIA_TOTO",
            "BIA_VBL",
            "BIA_VDR",
            "BIA_VFEC",
            "BIA_VFL",
            "BIA_VFLM",
            "BIA_VHC",
            "BIA_VTO",
            "BIA_Mixed",
            "BIA_SPORTBOOK"
    );

    public static final String ISOFTBET_IP_WHITELIST = "iSoftBet.ipWhiteList";
    public static final String ISOFTBET_SECRET_KEY   = "iSoftBet.secretKey";

    public static final String PLAYNGO_ACCESS_TOKEN_KEY   = "playNGo.accessToken";

    public static final String BETGAMES_SECRETKEY = "betgames.secretKey";

    public static final String EXPIRY_MINUTES_KEY = "omegatron.session.expiryMinutes";

    public static final int PS_WEB_SESSION_TIMEOUT_DEFAULT = 30;

    public static final String EGAME_VERSION1 = "egame1";
    public static final String EGAME_VERSION2 = "egame2";

    public static final String NETENTBOUNCEBACK_INSUFFICIENTFUND = "INSUFFICIENT_FUND";
    public static final String NETENTBOUNCEBACK_NOTCHECKOUT = "NOT_CHECK_OUT";

    public static final String TEST_ENVIRONMENT = "omegaTron.isTestEnvironment";

    public static final Integer CHARTWELL_Baccarat_GameInfoId = 774;
    public static final Integer CHARTWELL_Blackjack_GameInfoId = 775;
    public static final Integer CHARTWELL_FruitParty_GameInfoId = 756;
    public static final Integer NETENT_Baccarat_GameInfoId = 840;

    public static final String CHARTWELL_Blackjack_GameId = "5";
    public static final String CHARTWELL_Baccarat_GameId = "13";
    public static final String NETENT_Baccarat_GameId = "13";

    public static final String STATUS_ON_HOLD   = "ON_HOLD";
    public static final String STATUS_CANCELLED = "CANCELLED";
    public static final String STATUS_CLEARED   = "CLEARED";

    public static final String STATUS_NOT_LOCKED = "NOT_LOCKED";
    public static final String STATUS_PERM_LOCK = "PERM_LOCK";

    public static final String SESSION_LIMIT_MESSAGE = "sessionLimit.sessionLimitReached";

    public static final String SSW_USER_PLATFORM_SESSION_LIMIT = "_SSW_";

    public static final String DIGITAIN_SPORTBOOK_SECRET_KEY   = "digitain.sportbook.secretKey";
    public final static String DIGITAIN_SPORTBOOK_PARTNER_ID = "digitain.sportbook.partnerId";

    public static final String PLATFORM_CURRENCY_CONVERSION_IS_ENABLED = "platformCurrencyConv.isEnabled";
    public static final String PLATFORM_CURRENCY_CONVERSION_CACHE_TIMETOLIVE = "platformCurrencyConv.conversionRate.cache.timeToLive";
    public static final int    PLATFORM_CURRENCY_CONVERSION_CACHE_TIMETOLIVE_DEFAULT = 60;
    public static final String PLATFORM_CURRENCY_CONVERSION_CACHE_TIMETOIDLE = "platformCurrencyConv.conversionRate.cache.timeToIdle";
    public static final int    PLATFORM_CURRENCY_CONVERSION_CACHE_TIMETOIDLE_DEFAULT = 60;

    public final static String BIA_SPORTBOOK_BASE_URL               = "bia.baseUrl";
    public final static String BIA_SPORTBOOK_BASE_MOBILE_URL        = "bia.mobile.baseUrl";
    public final static String BIA_SPORTBOOK_WALLET_CODE            = "bia.walletCode";
    public final static String BIA_SPORTBOOK_WEB_SERVICE_URL        = "bia.webServiceUrl";
    public static final String BIA_SKIP_UPDATE_BET_DETAIL           = "bia.skipUpdateBetDetail";
    public static final String AUTH_SITEID_COUNTRY                  = "bia.country"; // betrebels do not want send player's country (except itly)
    public static final String AUTH_SITEID_COUNTRY_DEFAULT          = "AT";
    public static final String BIA_DELAY_IN_CALLING_GETBETEXTRA     = "bia.delayInCallingGetBetExtra";


    public final static String VNET_IP_WHITE_LIST                   = "vnet.ipWhiteList";
    public final static String VNET_SERVICE_USERNAME                = "vnet.cis.userName";
    public final static String VNET_SERVICE_USERNAME_DEFAULT        = "cis_gamingtec";
    public final static String VNET_SERVICE_PASSWORD                = "vnet.cis.password";
    public final static String VNET_SERVICE_PASSWORD_DEFAULT        = "3tNsAvXmtYMDTna63TXj";
    public final static String VNET_AUTH_CHECK                      = "vnet.isAuthEnabled";
    public final static String ACCOUNT_DETAILS_USERID_SHOW          = "getAccountDetails.showUserId";
    public final static String ACCOUNT_DETAILS_BRAND_SHOW           = "getAccountDetails.showBrand";
    public final static String ACCOUNT_DETAILS_FIRSTNAME_SHOW       = "getAccountDetails.showFirstName";
    public final static String ACCOUNT_DETAILS_LASTNAME_SHOW        = "getAccountDetails.showLastName";
    public final static String ACCOUNT_DETAILS_EMAIL_SHOW           = "getAccountDetails.showEmail";
    public final static String ACCOUNT_DETAILS_ADDRESS_SHOW         = "getAccountDetails.showAddress";
    public final static String ACCOUNT_DETAILS_COUNTRY_SHOW         = "getAccountDetails.showCountry";
    public final static String ACCOUNT_DETAILS_CURRENCY_SHOW        = "getAccountDetails.showCurrency";
    public final static String ACCOUNT_DETAILS_LANGUAGE_SHOW        = "getAccountDetails.showLanguage";
    public final static String ACCOUNT_DETAILS_POSTALCODE_SHOW      = "getAccountDetails.showPostalCode";
    public final static String ACCOUNT_DETAILS_CITY_SHOW            = "getAccountDetails.showCity";
    public final static String VNET_AUTH_CHECK_DEFAULT              = "true";

    public final static int USER_TYPE_PLAYER = 0;
    public final static int USER_TYPE_AGENT = 20;
    public final static int USER_TYPE_BROKER = 1;
    public final static int USER_TYPE_PROMOTER = 2;


    public final static String ICS_DOMAIN = "ics.domain";


    public final static String VIG_SECRET       = "vig.secret";
    public final static String VIG_USER         = "vig.user";
    public final static String VIG_SITE_ID      = "vig.siteId";

    // registry hash setting. if set this, then will bypass all the platform password checking, only use on omegawallet. because otherwise load test will not pass.
    public final static String OMEGA_WALLET_PLATFORM_PASSWORD_SUPER    = "omegaWallet.platform.superPassword";

    public static final String GREENTUBE_SECRET_KEY   = "greentube.secretKey";

    public static final String EZUGI_SALT   = "ezugi.secretKey";
    public static final String EZUGI_ENABLE_HASH   = "ezugi.enabledHash";
    public static final Integer EZUGI_ENABLE_HASH_DEFAULT   = 1;

    public static final String MICROGAMING_REGISTER_WITH_USERID = "microgaming.register.userId";

    public static final String RIMINI_WEB_SERVICE_URL_KEY = "rimini.webService.url";
    public static final String RIMINI_WEB_SERVICE_URL_DEFAULT = "https://aesoffice.eu.com:14444/EFTService.asmx?wsdl";

    public static final String RIMINI_GAME_DEFAULT   = "RIMINI";

    public static final String EMPTY = "";
}
