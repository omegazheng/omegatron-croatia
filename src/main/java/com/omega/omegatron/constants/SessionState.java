package com.omega.omegatron.constants;

public enum SessionState {
    PENDING ("PENDING","Pending"),
    OPEN ("OPEN","Open"),
    CLOSED ("CLOSED","Closed"),
    TIMEOUT("TIMEOUT","Timeout");

    private final String code;
    private final String description;

    SessionState(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static SessionState fromCode(String code) {
        for (SessionState element : SessionState.values()) {
            if (element.getCode().equals(code)) {
                return element;
            }
        }
        return null;
    }

    public String toString() {
        return description;
    }
}
