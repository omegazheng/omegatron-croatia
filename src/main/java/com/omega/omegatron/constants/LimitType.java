package com.omega.omegatron.constants;

/**
 * Created with IntelliJ IDEA.
 * User: daryl
 * Date: 18/07/13
 * Time: 2:41 PM
 * To change this template use File | Settings | File Templates.
 */
public enum LimitType {

    LOSS_LIMIT("LOSS_LIMIT", "Loss Limit"),
    WAGER_LIMIT("WAGER_LIMIT", "Wager Limit");


    private final String code;
    private final String description;

    LimitType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static LimitType fromCode(String code) {
        for (LimitType element : LimitType.values()) {
            if (element.getCode().equalsIgnoreCase(code)) {
                return element;
            }
        }
        return null;
    }

    public String toString() {
        return description;
    }

}

