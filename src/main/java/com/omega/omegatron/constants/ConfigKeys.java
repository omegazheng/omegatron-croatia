package com.omega.omegatron.constants;

/**
 */
public class ConfigKeys {

    public final static String EGAME_IS_ENABLED = "egame.isEnabled";
    public final static String EGAME_VERSION = "egame.version";
    public final static String USER_LIMITS_ENABLED = "userLimits.enabled";

    public final static String PS_WEB_SESSION_TIMEOUT = "psWebSessionTimeout";

    public final static String SHUTDOWN_TIME_RANGE = "shutdownMode.timeRange";

    public final static String TXS_CACHE_USER_WEB_SESSION_UPDATE = "txs.cache.UserWebSession.update";

    public final static String YGGDRASIL_ORG = "yggdrasil.org";

    public final static String  STATSD_PREFIX ="statsd.prefix";
    public final static String  STATSD_DOMAIN ="statsd.domain";
    public final static String  STATSD_PORT   ="statsd.port";

}
