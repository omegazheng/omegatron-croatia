package com.omega.omegatron.commons;

public class GenericCommonConstants {
    public static final String RETURN_ERROR_CODE                    = "errorCode";
    public static final String RETURN_ERROR_MESSAGE                 = "errorMessage";

    public static final String RESPONSE_RESULT_OK                          = "OK";
    public static final String RESPONSE_RESULT_ERROR                       = "ERROR";
}
