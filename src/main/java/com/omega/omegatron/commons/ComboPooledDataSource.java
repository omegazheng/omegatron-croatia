package com.omega.omegatron.commons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 */
public class ComboPooledDataSource extends TransactionAwareDataSourceProxy {
    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

    private String appName;
    private com.mchange.v2.c3p0.ComboPooledDataSource dataSource;

    private String getConfigFile(){
        String configFile =System.getenv("OMEGA"+appName.toUpperCase()+"_CONF_FILE");
//        if (configFile==null) return "/Users/anmolarora/conf/Omega"+appName+".conf";
        if (configFile==null) return "/home/omega/apps/conf/Omega"+appName+".conf";
        else return configFile;
    }

    @Autowired
    ComboPooledDataSource(com.mchange.v2.c3p0.ComboPooledDataSource dataSource)
    {
        this.dataSource =  dataSource;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
        //Do the initial now
        String configFile=getConfigFile();
        log.info(appName+" ConfigFile='"+configFile+"'");
        Properties prop = new Properties();

        try {
            InputStream input = new FileInputStream(configFile);
            prop.load(input);
            String jdbcUrl  = prop.getProperty("jdbcUrl");
            String user     = prop.getProperty("user");
            String password = prop.getProperty("password");
            log.info("Real jdbcUrl="+jdbcUrl+" user="+user+" password="+password);
            if (jdbcUrl == null || user == null || password == null) {
                log.error("missing setting in the "+ configFile +" file.");
                throw new RuntimeException("Configuration Error.");
            }
            dataSource.setJdbcUrl(jdbcUrl);
            dataSource.setUser(user);
            dataSource.setPassword(password);
            super.setTargetDataSource(dataSource);
        } catch (FileNotFoundException fnfe){
            log.error("Configuration file =" + configFile + " not found.");
            throw new RuntimeException("Configuration Error.");
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Fail to initialize dataSource");
        }

    }
}
