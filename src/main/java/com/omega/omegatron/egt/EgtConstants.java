package com.omega.omegatron.egt;

public class EgtConstants {

    public static final Integer RES_1000 = 1000;
    public static final Integer RES_1100 = 1100;
    public static final Integer RES_2000 = 2000;
    public static final Integer RES_3000 = 3000;
    public static final Integer RES_3100 = 3100;

    public static final String RES_OK = "OK";
    public static final String RES_DUPLICATE = "DUPLICATE";
    public static final String RES_TIME_OUT = "TIME_OUT";
    public static final String RES_INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";
    public static final String RES_INVALID_SESSION = "INVALID_SESSION";
    public static final String RES_EXPIRED = "EXPIRED";
    public static final String RES_INSUFFICIENT_FUNDS = "INSUFFICIENT_FUNDS";

    public static final String ERR_MISSING_PARAM = "Missing parameter";
    public static final String ERR_USERNAME = "Username not match";
    public static final String ERR_PASSWORD = "Password not match";
    public static final String ERR_BET_NOT_FOUND = "Bet not found";

    public static final String ROUND_BEGIN = "ROUND_BEGIN";
    public static final String ROUND_END = "ROUND_END";
    public static final String ROUND_CANCEL = "ROUND_CANCEL";
    public static final String ROUND_CONTINUE = "ROUND_CONTINUE";
    public static final String JACKPOT_END = "JACKPOT_END ";

    public static final String JACKPOT_WIN = "999";
}
