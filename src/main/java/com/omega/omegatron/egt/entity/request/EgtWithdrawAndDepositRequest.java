package com.omega.omegatron.egt.entity.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "WithdrawAndDepositRequest")
public class EgtWithdrawAndDepositRequest {

    @XmlElement(name = "UserName")
    private String username;

    @XmlElement(name = "Password")
    private String password;

    @XmlElement(name = "TransferId")
    private String tranferId;

    @XmlElement(name = "GameId")
    private String gameId;

    @XmlElement(name = "GameNumber")
    private String gameRoundId;

    @XmlElement(name = "PlayerId")
    private Integer partyId;

    @XmlElement(name = "PlatformType")
    private String platformType;

    @XmlElement(name = "SessionId")
    private String gameSessionId;

    @XmlElement(name = "Amount")
    private BigDecimal amount;

    @XmlElement(name = "WinAmount")
    private BigDecimal winAmount;

    @XmlElement(name = "Currency")
    private String currency;

    @XmlElement(name = "PostalCode")
    private String postalCode;

    @XmlElement(name = "Reason")
    private String reason;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPartyId() {
        return partyId;
    }

    public void setPartyId(Integer partyId) {
        this.partyId = partyId;
    }

    public String getTranferId() {
        return tranferId;
    }

    public void setTranferId(String tranferId) {
        this.tranferId = tranferId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getGameRoundId() {
        return gameRoundId;
    }

    public void setGameRoundId(String gameRoundId) {
        this.gameRoundId = gameRoundId;
    }

    public String getPlatformType() {
        return platformType;
    }

    public void setPlatformType(String platformType) {
        this.platformType = platformType;
    }

    public String getGameSessionId() {
        return gameSessionId;
    }

    public void setGameSessionId(String gameSessionId) {
        this.gameSessionId = gameSessionId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getWinAmount() {
        return winAmount;
    }

    public void setWinAmount(BigDecimal winAmount) {
        this.winAmount = winAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
