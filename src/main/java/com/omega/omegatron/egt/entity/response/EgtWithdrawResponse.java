package com.omega.omegatron.egt.entity.response;

import com.omega.omegatron.egt.EgtConstants;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "WithdrawResponse")
public class EgtWithdrawResponse {

    @XmlElement(name = "Balance")
    private BigDecimal balance;

    @XmlElement(name = "CasinoTransferId")
    private Long transactionId;

    @XmlElement(name = "ErrorCode")
    private Integer errorCode;

    @XmlElement(name = "ErrorMessage")
    private String errorMessage;

    public EgtWithdrawResponse() {
    }

    public EgtWithdrawResponse(BigDecimal balance, Long transactionId) {
        this.balance = balance;
        this.transactionId = transactionId;
        this.errorCode = EgtConstants.RES_1000;
        this.errorMessage = EgtConstants.RES_OK;
    }

    public EgtWithdrawResponse(BigDecimal balance, Long transactionId, Integer errorCode, String errorMessage) {
        this.balance = balance;
        this.transactionId = transactionId;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
