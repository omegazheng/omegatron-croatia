package com.omega.omegatron.egt.entity.response;

import com.omega.omegatron.egt.EgtConstants;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "GetPlayerBalanceResponse")
public class EgtBalanceResponse {

    @XmlElement(name = "Balance")
    private BigDecimal balance;

    @XmlElement(name = "ErrorCode")
    private Integer errorCode;

    @XmlElement(name = "ErrorMessage")
    private String errorMessage;

    public EgtBalanceResponse() {
    }

    public EgtBalanceResponse(BigDecimal balance) {
        this.balance = balance;
        this.errorCode = EgtConstants.RES_1000;
        this.errorMessage = EgtConstants.RES_OK;
    }

    public EgtBalanceResponse(BigDecimal balance, Integer errorCode, String errorMessage) {
        this.balance = balance;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
