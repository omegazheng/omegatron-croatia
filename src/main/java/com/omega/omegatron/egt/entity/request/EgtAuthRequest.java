package com.omega.omegatron.egt.entity.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AuthRequest")
public class EgtAuthRequest {

    @XmlElement(name = "UserName")
    private String username;

    @XmlElement(name = "Password")
    private String password;

    @XmlElement(name = "PlayerId")
    private Integer partyId;

    @XmlElement(name = "DefenceCode")
    private String sessionKey;

    @XmlElement(name = "SessionId")
    private String gameSessionId;

    public EgtAuthRequest() {
    }

    public EgtAuthRequest(String username, String password, Integer partyId, String sessionKey, String gameSessionId) {
        this.username = username;
        this.password = password;
        this.partyId = partyId;
        this.sessionKey = sessionKey;
        this.gameSessionId = gameSessionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPartyId() {
        return partyId;
    }

    public void setPartyId(Integer partyId) {
        this.partyId = partyId;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getGameSessionId() {
        return gameSessionId;
    }

    public void setGameSessionId(String gameSessionId) {
        this.gameSessionId = gameSessionId;
    }
}
