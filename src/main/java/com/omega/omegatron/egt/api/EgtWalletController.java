package com.omega.omegatron.egt.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.omega.commonsUtil.ObjectMapperUtil;
import com.omega.omegatron.BaseWalletController;
import com.omega.omegatron.constants.OmegatronConstants;
import com.omega.omegatron.egt.service.EgtWalletAdapter;
import com.omega.omegatron.util.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Controller
@RequestMapping("/egtWallet")
public class EgtWalletController {
    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

    @Autowired
    private EgtWalletAdapter egtWalletAdapter;

    @RequestMapping(value = {"*", ""}, method = RequestMethod.GET)
    @ResponseBody
    public String defaultEndPoint(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.info("Egt wallet End Point");
        BaseWalletController.setGeneralResponse(OmegatronConstants.PRODUCT_EGT, response);
        return null;
    }


    @RequestMapping("/Authenticate")
    public ModelAndView authenticate(HttpServletRequest request, HttpServletResponse response) {
        log.info("Egt wallet authenticate request");
        String xmlString = getXml(request, response);
        if (xmlString != null) {
            String res = egtWalletAdapter.authenticate(xmlString);
            sendResponse(response, res);
        }
        return null;
    }

    @RequestMapping("/GetPlayerBalance")
    public ModelAndView getBalance(HttpServletRequest request, HttpServletResponse response) {
        log.info("Egt wallet deposit request");
        String xmlString = getXml(request, response);
        if (xmlString != null) {
            String res = egtWalletAdapter.balance(xmlString);
            sendResponse(response, res);
        }
        return null;
    }

    @RequestMapping("/Withdraw")
    public ModelAndView withdraw(HttpServletRequest request, HttpServletResponse response) {
        log.info("Egt wallet withdraw request");
        String xmlString = getXml(request, response);
        if (xmlString != null) {
            String res = egtWalletAdapter.withdraw(xmlString);
            sendResponse(response, res);
        }
        return null;
    }

    @RequestMapping("/Deposit")
    public ModelAndView deposit(HttpServletRequest request, HttpServletResponse response) {
        log.info("Egt wallet deposit request");
        String xmlString = getXml(request, response);
        if (xmlString != null) {
            String res = egtWalletAdapter.deposit(xmlString);
            sendResponse(response, res);
        }
        return null;
    }

    @RequestMapping("/WithdrawAndDeposit")
    public ModelAndView withdrawAndDeposit(HttpServletRequest request, HttpServletResponse response) {
        log.info("Egt wallet withdrawAndDeposit request");
        String xmlString = getXml(request, response);
        if (xmlString != null) {
            String res = egtWalletAdapter.withdrawAndDeposit(xmlString);
            sendResponse(response, res);
        }
        return null;
    }

    private String getXml(HttpServletRequest request, HttpServletResponse response) {
        try {
            String xmlString = JsonUtil.readRequest(request);
            log.info("Request: " + xmlString);
            if (xmlString.length() == 0) {
                sendResponse(response, "EgtWallet is running. Please send valid XML request.");
                return null;
            }
            return xmlString;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void sendResponse(HttpServletResponse response, String res) {
        log.info("Response: " + res);
        try {
            response.setContentType("application/xml; charset=UTF-8");
            PrintWriter writer = response.getWriter();
            writer.write(res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setEgtWalletAdapter(EgtWalletAdapter egtWalletAdapter) {
        this.egtWalletAdapter = egtWalletAdapter;
    }
}
