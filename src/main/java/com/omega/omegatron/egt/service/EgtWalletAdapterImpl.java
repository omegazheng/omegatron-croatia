package com.omega.omegatron.egt.service;

import com.omega.commons.service.PlatformService;
import com.omega.commons.service.UserService;
import com.omega.commonsUtil.JAXBUtil;
import com.omega.omegatron.constants.OmegatronConstants;
import com.omega.omegatron.core.*;
import com.omega.omegatron.egt.EgtConstants;
import com.omega.omegatron.egt.entity.request.*;
import com.omega.omegatron.egt.entity.response.*;
import com.omega.omegatron.service.GameInstanceService;
import com.omega.omegatron.service.RegistryService;
import com.omega.omegatron.service.UserWebSession;
import com.omega.omegatron.service.UserWebSessionService;
import com.sun.istack.NotNull;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class EgtWalletAdapterImpl implements EgtWalletAdapter {
    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

    private RegistryService registryService;
    private TransactionService transactionService;
    private UserWebSessionService userWebSessionService;
    private GameInstanceService gameInstanceService;
    private PlatformService platformService;
    private UserService userService;

    public String authenticate(String xmlString) {
        EgtAuthRequest req = JAXBUtil.toObject(xmlString, EgtAuthRequest.class);
        EgtGeneralResponse res = validPartner(req.getPartyId(), req.getUsername(), req.getPassword());
        if (res != null) {
            return toXml(res);
        }
        log.info("EGT authenticate::partyId=" + req.getPartyId());
        UserWebSession userWebSession = userWebSessionService.getUserWebSession(req.getSessionKey());
        if (null == userWebSession || null == userWebSession.getPartyId()) {
            return toXml(new EgtGeneralResponse(null, EgtConstants.RES_2000, EgtConstants.RES_TIME_OUT));
        } else {
            if (!userWebSession.getPartyId().equals(req.getPartyId()))
                return toXml(new EgtGeneralResponse(null, EgtConstants.RES_3100, EgtConstants.RES_INVALID_SESSION));
        }

        BalanceResponse br = transactionService.getBalance(req.getPartyId(), null, OmegatronConstants.PRODUCT_EGT);
        if (br.getTranServiceError() != null) {
            return toXml(new EgtGeneralResponse(null, EgtConstants.RES_3000, br.getTranServiceError().name()));
        }
        return toXml(new EgtGeneralResponse(x100(br.getBalance())));
    }

    public String balance(String xmlString) {
        EgtBalanceRequest req = JAXBUtil.toObject(xmlString, EgtBalanceRequest.class);
        EgtGeneralResponse res = validPartner(req.getPartyId(), req.getUsername(), req.getPassword());
        if (res != null) {
            return toXml(res);
        }
        log.info("EGT get balance::partyId=" + req.getPartyId());
        BalanceResponse br = transactionService.getBalance(req.getPartyId(), req.getCurrency(), OmegatronConstants.PRODUCT_EGT, req.getGameId());
        if (br.getTranServiceError() != null) {
            if (br.getTranServiceError().equals(TranServiceError.SESSIONKEY_NOT_MATCH) || br.getTranServiceError().equals(TranServiceError.SESSION_NOT_FOUND)) {
                return toXml(new EgtBalanceResponse(null, EgtConstants.RES_2000, EgtConstants.RES_TIME_OUT));
            }
            return toXml(new EgtBalanceResponse(null, EgtConstants.RES_3000, br.getTranServiceError().name()));
        }
        return toXml(new EgtBalanceResponse(x100(br.getBalance())));
    }

    public String withdraw(String xmlString) {
        EgtWithdrawRequest req = JAXBUtil.toObject(xmlString, EgtWithdrawRequest.class);
        EgtGeneralResponse generalRes = validPartner(req.getPartyId(), req.getUsername(), req.getPassword());
        if (generalRes != null) {
            return toXml(generalRes);
        }
        String sessionKey = userWebSessionService.getSessionKey(req.getPartyId());
        if(StringUtils.isEmpty(sessionKey)) {
            log.error("withdraw invalid sessionKey. ");
            return toXml(new EgtWithdrawResponse(null, null, EgtConstants.RES_3000, EgtConstants.RES_INVALID_SESSION));
        }

        log.info("EGT withdraw::partyId=" + req.getPartyId());
        ProcessTransactionRequest ptRequest = preparePtRequest(OmegatronConstants.TRAN_TYPE_GAME_BET, req.getPartyId(), req.getAmount(), req.getCurrency(), req.getGameId(), req.getGameRoundId(), req.getTranferId(), req.getReason());
        ProcessTransactionResponse ptResponse = transactionService.processTransaction(ptRequest);
        if (ptResponse.getTranServiceError() != null) {
            if (ptResponse.getTranServiceError().equals(TranServiceError.SESSIONKEY_NOT_MATCH) || ptResponse.getTranServiceError().equals(TranServiceError.SESSION_NOT_FOUND)) {
                return toXml(new EgtWithdrawResponse(null, null, EgtConstants.RES_2000, EgtConstants.RES_TIME_OUT));
            } else if (ptResponse.getTranServiceError().equals(TranServiceError.INSUFFICIENT_FUNDS)) {
                return toXml(new EgtWithdrawResponse(null, null, EgtConstants.RES_3100, EgtConstants.RES_INSUFFICIENT_FUNDS));
            }
            return toXml(new EgtWithdrawResponse(null, null, EgtConstants.RES_3000, ptResponse.getTranServiceError().name()));
        }
        if (ptResponse.getAlreadyProcessed()) {
            return toXml(new EgtWithdrawResponse(x100(ptResponse.getBalance()), ptResponse.getTransactionId(), EgtConstants.RES_1100, EgtConstants.RES_DUPLICATE));
        }
        return toXml(new EgtWithdrawResponse(x100(ptResponse.getBalance()), ptResponse.getTransactionId()));
    }

    public String deposit(String xmlString) {
        EgtDepositRequest req = JAXBUtil.toObject(xmlString, EgtDepositRequest.class);
        EgtGeneralResponse generalRes = validPartner(req.getPartyId(), req.getUsername(), req.getPassword());
        if (generalRes != null) {
            return toXml(generalRes);
        }
        log.info("EGT deposit::partyId=" + req.getPartyId());
        //check bet
        ProcessTransactionResponse betResponse = transactionService.checkTransaction(req.getGameRoundId(), OmegatronConstants.PRODUCT_EGT, true);
        if (null == betResponse)
            return toXml(new EgtDepositResponse(null, null, EgtConstants.RES_3000, EgtConstants.ERR_BET_NOT_FOUND));
        if (betResponse.getTranServiceError() != null)
            return toXml(new EgtDepositResponse(null, null, EgtConstants.RES_3000, betResponse.getTranServiceError().name()));
        ProcessTransactionRequest ptRequest;
        if (req.getGameId().equals(EgtConstants.JACKPOT_WIN) && req.getReason().equals(EgtConstants.JACKPOT_END)) {
            ptRequest = preparePtRequest(OmegatronConstants.TRAN_TYPE_GAME_WIN, req.getPartyId(), req.getAmount(), req.getCurrency(), req.getGameId(), req.getGameRoundId(), req.getTranferId() + "_JACKPOT", req.getReason());
        } else if (EgtConstants.ROUND_CANCEL.compareTo(req.getReason()) == 0) {
            ptRequest = preparePtRequest(OmegatronConstants.TRAN_TYPE_REFUND, req.getPartyId(), req.getAmount(), req.getCurrency(), req.getGameId(), req.getGameRoundId(), req.getTranferId(), req.getReason());
        } else {
            ptRequest = preparePtRequest(OmegatronConstants.TRAN_TYPE_GAME_WIN, req.getPartyId(), req.getAmount(), req.getCurrency(), req.getGameId(), req.getGameRoundId(), req.getTranferId(), req.getReason());
        }
        ProcessTransactionResponse ptResponse = transactionService.processTransaction(ptRequest);
        if (ptResponse.getTranServiceError() != null) {
            if (ptResponse.getTranServiceError().equals(TranServiceError.SESSIONKEY_NOT_MATCH) || ptResponse.getTranServiceError().equals(TranServiceError.SESSION_NOT_FOUND)) {
                return toXml(new EgtDepositResponse(null, null, EgtConstants.RES_2000, EgtConstants.RES_TIME_OUT));
            }
            return toXml(new EgtDepositResponse(null, null, EgtConstants.RES_3000, ptResponse.getTranServiceError().name()));
        }
        if (ptResponse.getAlreadyProcessed()) {
            return toXml(new EgtDepositResponse(x100(ptResponse.getBalance()), ptResponse.getTransactionId(), EgtConstants.RES_1100, EgtConstants.RES_DUPLICATE));
        }
        if (EgtConstants.ROUND_END.compareTo(req.getReason()) == 0 || BigDecimal.ZERO.compareTo(req.getAmount()) == 0) {
            Integer platformId = platformService.getPlatformIdFromCode(OmegatronConstants.PRODUCT_EGT);
            gameInstanceService.endGame(req.getPartyId(), req.getGameRoundId(), platformId, req.getGameId());
        }
        Long tranId = ptResponse.getTransactionId() == 0 ? betResponse.getTransactionId() : ptResponse.getTransactionId();
        return toXml(new EgtDepositResponse(x100(ptResponse.getBalance()), tranId));
    }

    public String withdrawAndDeposit(String xmlString) {
        EgtWithdrawAndDepositRequest req = JAXBUtil.toObject(xmlString, EgtWithdrawAndDepositRequest.class);
        EgtGeneralResponse generalRes = validPartner(req.getPartyId(), req.getUsername(), req.getPassword());
        if (generalRes != null) {
            return toXml(generalRes);
        }

        // check user websession
        String sessionKey = userWebSessionService.getSessionKey(req.getPartyId());
        if(StringUtils.isEmpty(sessionKey)) {
            log.error("withdrawAndDeposit invalid sessionKey.");
            return toXml(new EgtWithdrawAndDepositResponse(null, null, EgtConstants.RES_3000, EgtConstants.RES_INVALID_SESSION));
        }

        log.info("EGT withdrawAndDeposit - processing withdraw::partyId=" + req.getPartyId());
        ProcessTransactionRequest ptWithdrawRequest = preparePtRequest(OmegatronConstants.TRAN_TYPE_GAME_BET, req.getPartyId(), req.getAmount(), req.getCurrency(), req.getGameId(), req.getGameRoundId(), req.getTranferId(), req.getReason());
        ProcessTransactionResponse ptWithdrawResponse = transactionService.processTransaction(ptWithdrawRequest);
        if (ptWithdrawResponse.getAlreadyProcessed()) {
            return toXml(new EgtWithdrawAndDepositResponse(x100(ptWithdrawResponse.getBalance()), ptWithdrawResponse.getTransactionId(), EgtConstants.RES_1100, EgtConstants.RES_DUPLICATE));
        }
        if (null == ptWithdrawResponse.getTranServiceError()) {
            log.info("EGT withdrawAndDeposit - processing deposit::partyId=" + req.getPartyId());
            ProcessTransactionRequest ptDepositRequest;
            if (req.getGameId().equals(EgtConstants.JACKPOT_WIN) && req.getReason().equals(EgtConstants.JACKPOT_END)) {
                ptDepositRequest = preparePtRequest(OmegatronConstants.TRAN_TYPE_GAME_WIN, req.getPartyId(), req.getAmount(), req.getCurrency(), req.getGameId(), req.getGameRoundId(), req.getTranferId() + "_JACKPOT", req.getReason());
            } else {
                ptDepositRequest = preparePtRequest(OmegatronConstants.TRAN_TYPE_GAME_WIN, req.getPartyId(), req.getWinAmount(), req.getCurrency(), req.getGameId(), req.getGameRoundId(), req.getTranferId() + "_WIN", req.getReason());
            }
            ProcessTransactionResponse ptDepositResponse = transactionService.processTransaction(ptDepositRequest);
            if (ptDepositResponse.getTranServiceError() != null) {
                return toXml(new EgtWithdrawAndDepositResponse(null, null, EgtConstants.RES_3000, ptDepositResponse.getTranServiceError().name()));
            }
            //following line should be removed when we do save zero win amount to DB
            long tranId = req.getWinAmount().compareTo(BigDecimal.ZERO) == 0 ? ptWithdrawResponse.getTransactionId() : ptDepositResponse.getTransactionId();
            return toXml(new EgtWithdrawAndDepositResponse(x100(ptDepositResponse.getBalance()), tranId));
        } else {
            if (ptWithdrawResponse.getTranServiceError().equals(TranServiceError.SESSIONKEY_NOT_MATCH) || ptWithdrawResponse.getTranServiceError().equals(TranServiceError.SESSION_NOT_FOUND)) {
                return toXml(new EgtWithdrawAndDepositResponse(null, null, EgtConstants.RES_2000, EgtConstants.RES_TIME_OUT));
            }
            return toXml(new EgtWithdrawAndDepositResponse(null, null, EgtConstants.RES_3000, ptWithdrawResponse.getTranServiceError().name()));
        }
    }

    private ProcessTransactionRequest preparePtRequest(String tranType, Integer partyId, BigDecimal amount, String currency, String gameId, String gameRoundId, String transferId, String reason) {
        ProcessTransactionRequest ptRequest = new ProcessTransactionRequest();
        ptRequest.setAmount(formatAmount(amount, tranType));
        ptRequest.setCurrency(currency);
        ptRequest.setGameTranId(gameRoundId);
        ptRequest.setGameId(gameId);
        ptRequest.setPartyId(partyId);
        ptRequest.setProviderTranId(transferId);
        ptRequest.setTranType(tranType);
        ptRequest.setReference(reason);
        ptRequest.setPlatformCode(OmegatronConstants.PRODUCT_EGT);
        return ptRequest;
    }

    private BigDecimal x100(@NotNull final BigDecimal amount) {
        return amount.multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_FLOOR);
    }

    private BigDecimal formatAmount(@NotNull BigDecimal amount, @NotNull final String tranType) {
        amount = amount.divide(new BigDecimal(100), 2, BigDecimal.ROUND_FLOOR);
        if (tranType.equalsIgnoreCase(OmegatronConstants.TRAN_TYPE_GAME_BET) && amount.compareTo(BigDecimal.ZERO) > 0) {
            return amount.negate();
        }
        return amount;
    }

    private EgtGeneralResponse validPartner(final Integer partyID, final String username, final String password) {
        Integer brandId = userService.getBrandIdByPartyId(partyID);

        if (null == username || null == password)
            return new EgtGeneralResponse(null, EgtConstants.RES_3000, EgtConstants.ERR_MISSING_PARAM);
        if (username.compareToIgnoreCase(registryService.getValue(brandId, OmegatronConstants.EGT_PARTNER_UID, null)) != 0)
            return new EgtGeneralResponse(null, EgtConstants.RES_3000, EgtConstants.ERR_USERNAME);
        if (password.compareToIgnoreCase(registryService.getValue(brandId, OmegatronConstants.EGT_PARTNER_KEY, null)) != 0)
            return new EgtGeneralResponse(null, EgtConstants.RES_3000, EgtConstants.ERR_PASSWORD);
        return null;
    }

    private String toXml(Object res) {
        String reply = "";
        try {
            reply = JAXBUtil.toFormatString(res);
        } catch (Exception e) {
            log.error("Exception:" + e.getMessage());
        }
        return reply;
    }

    public void setRegistryService(RegistryService registryService) {
        this.registryService = registryService;
    }

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public void setUserWebSessionService(UserWebSessionService userWebSessionService) {
        this.userWebSessionService = userWebSessionService;
    }

    public void setGameInstanceService(GameInstanceService gameInstanceService) {
        this.gameInstanceService = gameInstanceService;
    }

    public void setPlatformService(PlatformService platformService) {
        this.platformService = platformService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
