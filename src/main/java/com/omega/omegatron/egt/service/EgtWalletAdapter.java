package com.omega.omegatron.egt.service;

public interface EgtWalletAdapter {
    String authenticate(String xmlString);

    String withdraw(String xmlString);

    String deposit(String xmlString);

    String balance(String xmlString);

    String withdrawAndDeposit(String xmlString);
}
