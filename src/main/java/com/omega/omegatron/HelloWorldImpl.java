
package com.omega.omegatron;

import javax.jws.WebService;

@WebService(endpointInterface = "com.omega.omegatron.HelloWorld")
public class HelloWorldImpl implements HelloWorld {

    public String sayHi(String text) {
        return "Hello " + text;
    }
}

