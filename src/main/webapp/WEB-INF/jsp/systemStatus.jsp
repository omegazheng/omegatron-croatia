<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html>
<head>
    <title>OmegaTron System Status</title>
	<link rel="stylesheet" href="../css/style.css" />
</head>

<body>

<div class="panel">
    <h1>System Status</h1>
    <table class="dataTable">
        <tr>
            <td><b>Host Name</b></td>
            <td colspan=2 class="right">${hostName}</td>
        </tr>
        <tr>
            <td><b>Database Host</b></td>
            <td colspan=2 class="right">${dbHost}</td>
        </tr>
        <tr>
            <td><b>Database Name</b></td>
            <td colspan=2 class="right">${dbName}</td>
        </tr>
        <tr>
            <td><label>Used Memory</label></td>
            <td class="right">${usedMemory} B</td>
            <td class="right">${usedMemoryMegs} M</td>
        </tr>
        <tr>
            <td><label>Free Memory<br/></label>(Allocated Not in Use)</td>
            <td class="right">${freeMemory} B</td>
            <td class="right">${freeMemoryMegs} M</td>
        </tr>
        <tr>
            <td><label>Heap Size <br/></label>(JVM Allocated Memory)</td>
            <td class="right">${heapSize} B</td>
            <td class="right">${heapSizeMegs} M</td>
        </tr>
        <tr>
            <td><label>Max Heap Size</label></td>
            <td class="right">${maxHeapSize} B</td>
            <td class="right">${maxHeapSizeMegs} M</td>
        </tr>
        <tr>
            <td><label>Memory Until Max Heap</label><br/>
                (Amount of memory available<br/> before heap is maxed)</td>
            <td class="right">${maxHeapRemaining} B</td>
            <td class="right">${maxHeapRemainingMegs} M</td>
        </tr>
        <tr>
            <td><label>Uptime</label></td>
            <td class="right" colspan="2">
                ${uptime}
                <br>
                (${uptimeRaw} ms)
            </td>
        </tr>
        <tr>
            <td><label>Start Time</label></td>
            <td class="right" colspan="2">${startTime}</td>
        </tr>
        <tr>
            <td><label>Server Time</label></td>
            <td class="right" colspan="2">${serverTime}</td>
        </tr>

        <tr>
            <td><label>Cache Reloaded</label></td>
            <td class="right" colspan="2">${cacheReloaded}</td>
        </tr>

        <tr>
            <td><label>Software Version</label></td>
            <td class="right" colspan="2">${softwareVersion}</td>
        </tr>
    </table>
</div>

</body>
</html>